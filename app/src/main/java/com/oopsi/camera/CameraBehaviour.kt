package com.oopsi.camera

import android.graphics.Bitmap

interface CameraBehaviour : LifecycleObserver {
    fun flash(isFlashEnabled: Boolean)

    fun takePhoto(onPhotoTaken: OnPhotoTaken): Bitmap
}
