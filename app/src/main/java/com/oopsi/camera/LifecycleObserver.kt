package com.oopsi.camera

interface LifecycleObserver {
    fun onDestroy()

    fun onPause()

    fun onResume()
}
