package com.oopsi.camera;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.renderscript.RenderScript;
import android.support.annotation.NonNull;
import android.util.Size;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.FotoapparatBuilder;
import io.fotoapparat.configuration.UpdateConfiguration;
import io.fotoapparat.parameter.Resolution;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.view.CameraView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kotlin.Unit;

import static io.fotoapparat.selector.FlashSelectorsKt.off;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;

public class CameraService implements CameraBehaviour {
    private final Fotoapparat fotoapparat;
    private final Context context;
    private Disposable disposable;
    private Frame frame = null;
    private Bitmap bitmapPreview;

    public CameraService(CameraView cameraView) {
        context = cameraView.getContext();
        fotoapparat = new FotoapparatBuilder(context)
                .frameProcessor(frame -> {
                    clearFrame();
                    this.frame = frame;
                })
                .previewScaleType(ScaleType.CenterCrop)
                .into(cameraView)
                .build();
    }

    private void clearFrame() {
        if (this.frame != null) {
            this.frame = null;
        }
    }


    @Override
    public void onDestroy() {
        dispose();
        clearFrame();
    }

    @Override
    public void onPause() {
        fotoapparat.stop();
    }

    @Override
    public void onResume() {
        fotoapparat.start();
        recycle(bitmapPreview);
    }

    @Override
    public void flash(boolean isFlashEnabled) {
        UpdateConfiguration newConfiguration = UpdateConfiguration.builder()
                .flash(flashes -> isFlashEnabled ? torch().invoke(flashes) : off().invoke(flashes))
                .build();
        fotoapparat.updateConfiguration(newConfiguration);

    }

    @NonNull
    @Override
    public Bitmap takePhoto(@NonNull OnPhotoTaken onPhotoTaken) {
        Resolution size = frame.getSize();
        Size imageSize = new Size(size.width, size.height);
        Matrix matrix = new Matrix();
        matrix.postRotate(-frame.getRotation());
        // do it for blocking way
        recycle(bitmapPreview);
        bitmapPreview = Bitmap.createBitmap(
                RenderScriptHelper.convertYuvToRgbIntrinsic(RenderScript.create(context), frame.getImage(), imageSize),
                0,
                0,
                size.width,
                size.height,
                matrix,
                true);
        fotoapparat.takePicture()
                .toBitmap()
                .whenAvailable(bitmapPhoto -> {
                    if (bitmapPhoto != null) {
                        preprocessing(bitmapPhoto, onPhotoTaken);
                        clearFrame();
                    }
                    return Unit.INSTANCE;
                });
        return bitmapPreview;
    }

    private void recycle(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    private void preprocessing(@NonNull BitmapPhoto bitmapPhoto, OnPhotoTaken onPhotoTaken) {
        Matrix matrix = new Matrix();
        matrix.postRotate(-bitmapPhoto.rotationDegrees);
        dispose();
        disposable = Observable.just(bitmapPhoto.bitmap)
                .subscribeOn(Schedulers.io())
                .map(bitmap -> {
                    Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    //  recycle(bitmap);
                    return rotatedBitmap;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onPhotoTaken::onPhotoTaken);

    }

    private void dispose() {
        dispose(disposable);
    }

    private void dispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }

}
