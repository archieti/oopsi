package com.oopsi.camera

import android.graphics.Bitmap

interface OnPhotoTaken {

    fun onPhotoTaken(bitmap: Bitmap)
}
