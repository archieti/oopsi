package com.oopsi.navigation

import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by dev on 19.02.18.
 */
@Singleton
class NavigationLifecycle @Inject constructor(private val navigatorHolder: NavigatorHolder) : AndroidNavigationLifecycle {

    override fun onPause() {
        navigatorHolder.removeNavigator()
    }

    override fun onResume(navigator: Navigator) {
        navigatorHolder.setNavigator(navigator)
    }
}
