package com.oopsi.navigation

import ru.terrakok.cicerone.Navigator

/**
 * Created by dev on 19.02.18.
 */

interface AndroidNavigationLifecycle {
    fun onPause()

    fun onResume(navigator: Navigator)
}
