package com.oopsi.navigation

object Screens {
    const val ERROR = "ERROR"
    const val TIMEOUT = "TIMEOUT"

    const val STORY_DETAILS = "STORY_DETAILS"
    const val SEARCH_BRANDED_DEVICE = "SEARCH_BRANDED_DEVICE"
    const val SEARCH_DEVICE = "SEARCH_DEVICE"
    const val SEARCH_BRAND = "SEARCH_BRAND"
    const val MAYBE_YOU_LOOKING = "MAYBE_YOU_LOOKING"
}