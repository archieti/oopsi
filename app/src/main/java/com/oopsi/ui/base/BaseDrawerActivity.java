package com.oopsi.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.oopsi.R;
import com.oopsi.ui.about.AboutOnBoardingActivity;
import com.oopsi.ui.content.PrivacyPolicyActivity;
import com.oopsi.ui.content.TermsActivity;
import com.oopsi.ui.devices.history.DeviceHistoryActivity;
import com.oopsi.ui.help.HelpActivity;
import com.oopsi.ui.languages.LanguagesActivity;
import com.oopsi.ui.settings.SettingsActivity;

import io.reactivex.disposables.CompositeDisposable;

import static com.oopsi.ui.base.DrawerDispatcher.toggleDrawer;

public abstract class BaseDrawerActivity extends BaseActivity implements DrawerLayout.DrawerListener {

    private DrawerDispatcher drawerDispatcher;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawerDispatcher = new DrawerDispatcher(navigationView());
        drawerLayout().addDrawerListener(this);
        menu().setOnClickListener(v -> toggleDrawer(drawerLayout()));
        compositeDisposable.add(drawerDispatcher.getOnClick()
                .doOnNext(view -> toggleDrawer(drawerLayout()))
                .subscribe(view -> {
                    switch (view.getId()) {
                        case R.id.menuHistory:
                            router.navigateTo(DeviceHistoryActivity.Companion.key());
                            break;
                        case R.id.menuLanguage:
                            router.navigateTo(LanguagesActivity.Companion.key());
                            break;
                        case R.id.menuSettings:
                            router.navigateTo(SettingsActivity.Companion.key());
                            break;
                        case R.id.menuHelp:
                            router.navigateTo(HelpActivity.Companion.key());
                            break;
                        case R.id.menuPrivacy:
                            router.navigateTo(PrivacyPolicyActivity.Companion.key());
                            break;
                        case R.id.menuTerms:
                            router.navigateTo(TermsActivity.Companion.key());
                            break;
                        case R.id.menuAbout:
                            router.navigateTo(AboutOnBoardingActivity.Companion.key());
                            break;
                    }
                }));
    }

    protected abstract NavigationView navigationView();

    protected abstract DrawerLayout drawerLayout();

    protected abstract View content();

    protected abstract View menu();

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        drawerDispatcher.move(slideOffset, content());
    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
