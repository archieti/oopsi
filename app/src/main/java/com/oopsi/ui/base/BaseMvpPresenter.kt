package com.oopsi.ui.base

import android.support.annotation.CallSuper
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.artjoker.events.AppEvents
import com.artjoker.repositories.LocalStorage

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseMvpPresenter<T : MvpView> : MvpPresenter<T>(), AppEvents.Listener {

    private var compositeDisposable = CompositeDisposable()

    @Inject lateinit var events: AppEvents
    @Inject lateinit var router: Router
    @Inject lateinit var storage: LocalStorage

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        events.addListener(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        events.removeListener(this)
        compositeDisposable.dispose()
    }

    infix fun disposeOnDestroy(subscribe: Disposable) {
        compositeDisposable.add(subscribe)
    }

    protected abstract inner class PresenterObserver<T> : DisposableObserver<T>() {

        @CallSuper
        override fun onStart() {
            super.onStart()
            disposeOnDestroy(this)
        }
    }

    protected abstract inner class PresenterSingleObserver<T> : DisposableSingleObserver<T>() {

        @CallSuper
        override fun onStart() {
            super.onStart()
            disposeOnDestroy(this)
        }
    }

    protected abstract inner class PresenterCompletableObserver : DisposableCompletableObserver() {

        @CallSuper
        override fun onStart() {
            super.onStart()
            disposeOnDestroy(this)
        }
    }


}
