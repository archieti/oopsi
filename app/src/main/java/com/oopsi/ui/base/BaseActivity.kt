package com.oopsi.ui.base

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.artjoker.NoInternetException
import com.artjoker.TimeoutException
import com.artjoker.data.errors.DefaultErrorHandler
import com.artjoker.events.AppEvents
import com.artjoker.models.Filter
import com.oopsi.Application
import com.oopsi.R
import com.oopsi.navigation.NavigationLifecycle
import com.oopsi.navigation.Screens
import com.oopsi.ui.devices.device.DeviceActivity
import com.oopsi.ui.filters.FiltersActivity
import com.oopsi.ui.scanner.search.brand.BrandSearchActivity
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.ui.services.profile.ServiceDetailsActivity
import com.oopsi.ui.views.progress.ProgressDialog
import com.oopsi.utils.*
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import org.jetbrains.anko.AlertBuilder
import org.jetbrains.anko.configuration
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import javax.inject.Inject

@Suppress("DEPRECATION")
abstract class BaseActivity : MvpAppCompatActivity(), AppEvents.Listener {

    @Inject lateinit var navigationLifecycle: NavigationLifecycle
    @Inject lateinit var events: AppEvents
    @Inject lateinit var router: Router

    private lateinit var progress: ProgressDialog
    private lateinit var toast: Toast

    private var alert: AlertDialog? = null
    private val disposable = CompositeDisposable()

    protected abstract val layoutId: Int
    protected open val navigator: Navigator = BaseNavigator()
    protected open val handleTimeout = false

    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        adjustFontScale(Configuration(configuration))
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        events.addListener(this)
        (application as Application).addToStackCount(javaClass)
        setContentView(layoutId)
        toast = Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT)
        manage(DefaultErrorHandler.onErrorHandled()
                .subscribe { httpError ->
                    runOnUiThread {
                        when {
                            httpError is NoInternetException -> showNoInternet()
                            httpError is TimeoutException && !handleTimeout -> onTimeout(httpError)
                            else -> router.navigateTo(Screens.ERROR, httpError.message.orEmpty())
                        }
                    }
                })
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleSubstituter.substitute(newBase))
    }

    protected open fun onTimeout(t: Throwable) {}

    protected fun manage(subscribe: Disposable) = subscribe.addTo(disposable)

    protected fun AlertBuilder<AlertDialog>.replace(): AlertDialog {
        alert?.dismiss()
        alert = show()
        return alert!!
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        progressConfigurations()
    }

    private fun progressConfigurations() {
        progress = ProgressDialog(this)
    }

    open fun showProgress() {
        if (!isFinishing) {
            runOnUiThread { progress.show() }
        }
    }

    private fun showNoInternet() {
        toast.cancel()
        toast = Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT)
        toast.show()
    }

    open fun hideProgress() {
        progress.dismiss()
    }

    override fun onResume() {
        super.onResume()
        navigationLifecycle.onResume(navigator)
    }

    override fun onPause() {
        super.onPause()
        hideProgress()
        navigationLifecycle.onPause()
    }

    override fun onDestroy() {
        (application as Application).removeFromStackCount(javaClass)
        disposable.clear()
        super.onDestroy()
    }

    override fun onLocaleChanges() = recreate()

    protected fun getInstancesCount() = (application as Application).getCountOf(javaClass)

    protected fun hideKeyboard() {
        currentFocus?.let {
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    private fun adjustFontScale(configuration: Configuration) {
        if (configuration.fontScale != 1f) {
            configuration.fontScale = 1f

            val metrics = resources.displayMetrics
            windowManager.defaultDisplay.getMetrics(metrics)

            metrics.scaledDensity = configuration.fontScale * metrics.density
            baseContext.resources.updateConfiguration(configuration, metrics)
        }
    }


    open inner class BaseNavigator : SupportAppNavigator(this, R.id.fragment_container) {

        override fun applyCommand(command: Command) {
            when (command.screenKey()) {
                Screens.ERROR -> command.transitionData<Any>().let { data ->
                    when (data) {
                        is Int -> showError(data).replace().tintBtns()
                        is String -> showError(data).replace().tintBtns()
                        else -> throw IllegalAccessException()
                    }
                }

                else -> super.applyCommand(command)
            }
        }

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?): Intent? = when (screenKey) {
            DeviceActivity.key() -> intentFor<DeviceActivity>()
            FilteredServicesActivity.key() -> intentFor<FilteredServicesActivity>()
            FiltersActivity.key() -> intentFor<FiltersActivity>(
                    FiltersActivity.EXT_FILTER to data as? Filter
            )
            ServiceDetailsActivity.key() -> intentFor<ServiceDetailsActivity>()
            Screens.SEARCH_BRANDED_DEVICE -> intentFor<BrandSearchActivity>(
                    BrandSearchActivity.EXT_ACTION to BrandSearchActivity.Action.SearchDevice
            )
            Screens.SEARCH_BRAND -> intentFor<BrandSearchActivity>(
                    BrandSearchActivity.EXT_ACTION to BrandSearchActivity.Action.Return(data as Int)
            )
            else -> null
        }

        override fun createFragment(screenKey: String?, data: Any?) = null
    }
}
