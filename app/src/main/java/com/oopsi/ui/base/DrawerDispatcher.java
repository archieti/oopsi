package com.oopsi.ui.base;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.animation.TranslateAnimation;

import com.artjoker.rx.RxBus;
import com.oopsi.R;

import io.reactivex.Observable;

import static android.view.Gravity.START;

public class DrawerDispatcher {
    private float lastTranslate = 0.0f;
    private NavigationView navigationView;


    private RxBus<View> onClick = new RxBus<>();

    public DrawerDispatcher(NavigationView navigationView) {
        this.navigationView = navigationView;
        navigationView.findViewById(R.id.menuHistory).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuLanguage).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuSettings).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuHelp).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuPrivacy).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuTerms).setOnClickListener(view -> onClick.send(view));
        navigationView.findViewById(R.id.menuAbout).setOnClickListener(view -> onClick.send(view));
    }

    public static void toggleDrawer(DrawerLayout drawerHolder) {
        if (drawerHolder.isDrawerVisible(START)) {
            new Handler().postDelayed(() -> drawerHolder.closeDrawer(START), 250);
        } else {
            drawerHolder.openDrawer(START);
        }
    }

    public Observable<View> getOnClick() {
        return onClick.toObservable();
    }

    @SuppressLint("ObsoleteSdkInt")
    public void move(float slideOffset, View content) {
        float moveFactor = (navigationView.getWidth() * slideOffset);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            content.setTranslationX(moveFactor);
        } else {
            TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
            anim.setDuration(0);
            anim.setFillAfter(true);
            content.startAnimation(anim);
            lastTranslate = moveFactor;
        }
    }

}
