package com.oopsi.ui.base

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

import com.artjoker.rx.RxBus

import io.reactivex.Observable

abstract class AbstractRecyclerViewAdapter<T>(var data: MutableList<T>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var itemClickBus = RxBus<T>()

    val itemClickBusObs: Observable<T>
        get() = itemClickBus.toObservable()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = createHolder(parent, viewType)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = bindHolder(holder, position)

    override fun getItemCount(): Int = data.size

    protected abstract fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder

    protected abstract fun bindHolder(holder: RecyclerView.ViewHolder, position: Int)

}
