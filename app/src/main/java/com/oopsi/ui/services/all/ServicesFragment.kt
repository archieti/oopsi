package com.oopsi.ui.services.all

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Service
import com.oopsi.R
import com.oopsi.ui.base.BaseFragment
import com.oopsi.ui.services.MarginItemDecoration
import com.oopsi.ui.services.ServicesRecyclerAdapter
import com.oopsi.ui.services.profile.ServiceDetailsActivity
import kotlinx.android.synthetic.main.fragment_services.*
import org.jetbrains.anko.support.v4.dimen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ServicesFragment : BaseFragment(), ServicesView {

    override val layoutId: Int = R.layout.fragment_services

    @Inject
    @InjectPresenter
    lateinit var servicesPresenter: ServicesPresenter

    @Inject
    lateinit var router: Router

    @ProvidePresenter
    fun providePresenter() = servicesPresenter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        servicesPresenter.findServices()

        servicesRefreshLayout.setOnRefreshListener {
            servicesPresenter.findServices()
        }

        val verticalSpaceItemDecoration = MarginItemDecoration(dimen(R.dimen.margin_half), dimen(R.dimen.margin))
        val servicesRecyclerAdapter = ServicesRecyclerAdapter(mutableListOf())
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(verticalSpaceItemDecoration)
            adapter = servicesRecyclerAdapter
        }
        add(servicesRecyclerAdapter.positionClickBus.toObservable()
                .subscribe {
                    servicesPresenter.put(servicesRecyclerAdapter.data[it])
                    router.navigateTo(ServiceDetailsActivity.key())
                })
    }

    override fun onLoading() {
        servicesRefreshLayout.isRefreshing = true
    }

    override fun showNoData() {
        emptyState.visibility = View.VISIBLE

        servicesRefreshLayout.isRefreshing = false
    }

    override fun showData(data: List<Service>) {
        emptyState.visibility = View.GONE
        (recyclerView.adapter as ServicesRecyclerAdapter).data = data.toMutableList()
        recyclerView.adapter.notifyDataSetChanged()

        servicesRefreshLayout.isRefreshing = false
    }
}
