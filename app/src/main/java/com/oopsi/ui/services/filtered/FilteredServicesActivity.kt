package com.oopsi.ui.services.filtered


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Chip
import com.artjoker.models.Service
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.services.MarginItemDecoration
import com.oopsi.ui.services.ServicesRecyclerAdapter
import com.oopsi.ui.services.profile.ServiceDetailsActivity
import com.oopsi.utils.setVisible
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_filtered_services.*
import org.jetbrains.anko.dimen
import javax.inject.Inject


class FilteredServicesActivity : BaseActivity(), FilteredServicesView {

    override val layoutId: Int = R.layout.activity_filtered_services

    @Inject
    @InjectPresenter
    lateinit var presenter: FilteredServicesPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        brandName.text = presenter.getBrandTitle(getString(R.string.results_title))
        backFromDeviceServices.setOnClickListener { finish() }
        filters.setOnClickListener {
            presenter.onFiltersClicked()
        }
        presenter.findServices()
        presenter.applyChips()

        servicesRefreshLayout.setOnRefreshListener {
            presenter.findServices()
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(MarginItemDecoration(dimen(R.dimen.margin_half), dimen(R.dimen.margin)))

        val servicesRecyclerAdapter = ServicesRecyclerAdapter(ArrayList())
        recyclerView.adapter = servicesRecyclerAdapter
        servicesRecyclerAdapter.positionClickBus.toObservable()
                .subscribeBy {
                    presenter.put(servicesRecyclerAdapter.data[it])
                    router.navigateTo(ServiceDetailsActivity.key())
                }
                .also { manage(it) }

        chipsRecycler.layoutManager = ChipsLayoutManager.newBuilder(this)
                .setScrollingEnabled(true)
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .build()
        val chipsRecyclerAdapter = ChipsRecyclerAdapter(mutableListOf(), ChipTitles(this))
        chipsRecycler.adapter = chipsRecyclerAdapter
        chipsRecyclerAdapter.removeClickBus.toObservable()
                .map { chipsRecyclerAdapter.data[it] }
                .subscribeBy { presenter.onChipRemove(it) }
                .also { manage(it) }
    }

    override fun showProgress() {
        noServices.visibility = View.GONE
        servicesRefreshLayout.isRefreshing = true
    }

    override fun onServicesFound(list: List<Service>) {
        noServices.visibility = View.GONE
        (recyclerView.adapter as ServicesRecyclerAdapter).data = list.toMutableList()
        recyclerView.adapter.notifyDataSetChanged()
        servicesRefreshLayout.isRefreshing = false
    }

    override fun noServices() {
        noServices.visibility = View.VISIBLE
        servicesRefreshLayout.isRefreshing = false
        (recyclerView.adapter as ServicesRecyclerAdapter).data = mutableListOf()
        recyclerView.adapter.notifyDataSetChanged()
    }

    override fun onChipsChanged(chips: MutableList<Chip>) {
        chipsRecycler.setVisible(chips.isNotEmpty())
        val chipsRecyclerAdapter = chipsRecycler.adapter as ChipsRecyclerAdapter
        chipsRecyclerAdapter.data = chips
        chipsRecyclerAdapter.notifyDataSetChanged()
    }


    companion object {
        const val TAG = "DeviceServicesActivity"
        fun key(): String = FilteredServicesActivity::class.java.simpleName
    }
}
