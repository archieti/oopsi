package com.oopsi.ui.services.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Schedule
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_recycler_schedule.view.*
import java.text.DateFormatSymbols


class ScheduleAdapter(data: MutableList<Schedule>) : AbstractRecyclerViewAdapter<Schedule>(data) {

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycler_schedule, parent, false)
        return ViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(schedule: Schedule) {
            val day = DateFormatSymbols.getInstance().weekdays[schedule.dayOfWeek.dayIndex()]
            itemView.dayTitle.text = day.capitalize()
            itemView.openDayTime.text = schedule.workHours.openTime(itemView.context)
        }
    }
}
