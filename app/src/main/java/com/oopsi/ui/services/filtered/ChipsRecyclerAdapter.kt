package com.oopsi.ui.services.filtered

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Chip
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_recycler_chip.view.*


class ChipsRecyclerAdapter(data: MutableList<Chip>, val chipTitles: ChipTitles) : AbstractRecyclerViewAdapter<Chip>(data) {
    var removeClickBus = RxBus<Int>()

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_chip, parent, false)
        return ViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.remove.setOnClickListener { removeClickBus.send(adapterPosition) }
        }

        fun bind(chip: Chip) {
            itemView.title.text = chipTitles.text(chip)
        }
    }
}
