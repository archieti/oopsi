package com.oopsi.ui.services

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Service
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_recycler_service.view.*


class ServicesRecyclerAdapter(data: MutableList<Service>) : AbstractRecyclerViewAdapter<Service>(data) {
    var positionClickBus = RxBus<Int>()

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_service, parent, false)
        return ViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.container.setOnClickListener { positionClickBus.send(adapterPosition) }
        }

        fun bind(service: Service) {
            itemView.serviceTitle.text = service.name
            itemView.serviceAddress.text = service.address
            itemView.serviceDistance.text = service.distance
        }
    }
}
