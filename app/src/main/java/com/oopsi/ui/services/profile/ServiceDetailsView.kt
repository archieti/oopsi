package com.oopsi.ui.services.profile

import android.content.Intent
import com.arellomobile.mvp.MvpView
import com.artjoker.models.Schedule
import com.artjoker.models.Service

interface ServiceDetailsView : MvpView {

    fun onServiceRetrieved(selectedService: Service)

    fun openAndroidActivity(intent: Intent)

    fun schedule(today: Schedule, schedules: List<Schedule>)
}