package com.oopsi.ui.services.all

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Service

interface ServicesView : MvpView {
    fun showNoData()

    fun showData(data: List<Service>)
    fun onLoading()
}
