package com.oopsi.ui.services.profile

import android.content.Context
import android.content.Context.TELEPHONY_SERVICE
import android.content.Intent
import android.net.Uri
import android.telephony.TelephonyManager
import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.Service
import com.artjoker.repositories.ServicesRepository
import com.oopsi.R
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.utils.StringJoiner
import javax.inject.Inject


@InjectViewState
class ServiceDetailsPresenter @Inject constructor(
        private val servicesRepository: ServicesRepository
) : BaseMvpPresenter<ServiceDetailsView>() {

    fun getService() {
        val selectedService = servicesRepository.selectedService
        viewState.schedule(selectedService.todaySchedule(), selectedService.scheduleExceptToday())
        viewState.onServiceRetrieved(selectedService)
    }

    fun startDirections(selectedService: Service) {
        val uri = Uri.parse("geo:0,0?q=${selectedService.location.lat},${selectedService.location.lng}(${selectedService.name})")
        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
        mapIntent.`package` = "com.google.android.apps.maps"
        viewState.openAndroidActivity(mapIntent)
    }

    fun startDialActivity(selectedService: Service, context: Context) {
        if (isTelephonyEnabled(context)) {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${selectedService.contact?.phone}")
            viewState.openAndroidActivity(intent)
        }
    }

    fun startMailingActivity(selectedService: Service, context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.type = "text/plain"
        intent.data = Uri.parse("mailto:${selectedService.contact?.email}")
        intent.putExtra(Intent.EXTRA_EMAIL, selectedService.contact?.email)
        viewState.openAndroidActivity(Intent.createChooser(intent, context.getString(R.string.send_email_title)))
    }

    private fun isTelephonyEnabled(context: Context): Boolean {
        val telephonyManager = context.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        return telephonyManager.simState == TelephonyManager.SIM_STATE_READY
    }
}


internal fun List<String>.openTime(context: Context): String {

    if (isEmpty()) return context.getString(R.string.closed)

    val joiner = StringJoiner("\n")
    forEach { joiner.add(it) }

    return joiner.toString()
}
