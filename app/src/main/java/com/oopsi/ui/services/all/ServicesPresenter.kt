package com.oopsi.ui.services.all

import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.NearbyServicesUseCase
import com.artjoker.models.Service
import com.artjoker.repositories.ServicesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject

@InjectViewState
class ServicesPresenter @Inject constructor(
        private val nearbyServicesUseCase: NearbyServicesUseCase,
        private val servicesRepository: ServicesRepository
) : BaseMvpPresenter<ServicesView>() {

    fun findServices() = nearbyServicesUseCase.execute(
            object : PresenterSingleObserver<List<Service>>() {

                override fun onStart() {
                    super.onStart()
                    viewState.onLoading()
                }

                override fun onSuccess(t: List<Service>) {
                    viewState.showData(t)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    viewState.showNoData()
                }
            })


    fun put(service: Service) {
        servicesRepository.selectedService = service
    }
}
