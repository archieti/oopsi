package com.oopsi.ui.services.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Schedule
import com.artjoker.models.Service
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.activity_service_details.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class ServiceDetailsActivity : BaseActivity(), ServiceDetailsView {

    override val layoutId: Int = R.layout.activity_service_details

    @Inject
    @InjectPresenter
    lateinit var presenter: ServiceDetailsPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.getService()
    }


    override fun onServiceRetrieved(selectedService: Service) {
        collapsingToolbar.title = selectedService.name
        toolbar.setNavigationOnClickListener { finish() }

        val phone = selectedService.contact?.phone
        val emailText = selectedService.contact?.email

        email.text = emailText
        phoneNumber.text = phone

        val isPhonePresent = phone?.isNotEmpty() ?: false
        phoneLayout.setVisible(isPhonePresent)

        val isEmailPresent = emailText?.isNotEmpty() ?: false
        emailLayout.setVisible(isEmailPresent)
        authorizedLayout.setVisible(selectedService.isAuthorized)

        // divider.visibility = if (isEmailPresent && isPhonePresent) View.VISIBLE else View.INVISIBLE
        divider.setVisible(isEmailPresent && isPhonePresent)
        address.text = selectedService.address
        distance.text = getString(R.string.distance_to_service, selectedService.distance)

        directions.setOnClickListener { presenter.startDirections(selectedService) }
        phoneLayout.setOnClickListener { presenter.startDialActivity(selectedService, this) }
        emailLayout.setOnClickListener { presenter.startMailingActivity(selectedService, this) }
        todayLayout.setOnClickListener {
            expandableLayout.toggle()
            when {
                expandableLayout.isExpanded -> R.drawable.ic_collapse_icon
                else -> R.drawable.ic_expand_icon
            }.let(toggle::setImageResource)
        }
    }

    override fun openAndroidActivity(intent: Intent) {
        intent.resolveActivity(packageManager)?.let {
            startActivity(intent)
        }
    }

    override fun schedule(today: Schedule, schedules: List<Schedule>) {
        openDayTime.text = today.workHours.openTime(this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ScheduleAdapter(schedules.toMutableList())
    }

    override val navigator: Navigator = object : BaseNavigator() {

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    MainActivity.key() -> MainActivity.getIntent(context)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        fun key(): String = ServiceDetailsActivity::class.java.simpleName
    }
}