package com.oopsi.ui.services.filtered

import com.arellomobile.mvp.InjectViewState
import com.artjoker.events.AppEvents
import com.artjoker.interactors.ChipsUseCase
import com.artjoker.interactors.FilterServicesUseCase
import com.artjoker.models.*
import com.artjoker.repositories.ServicesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.filters.FiltersActivity
import javax.inject.Inject

@InjectViewState
class FilteredServicesPresenter @Inject constructor(
        private val servicesRepository: ServicesRepository,
        private val getChips: ChipsUseCase,
        private val filtering: FilterServicesUseCase
) : BaseMvpPresenter<FilteredServicesView>(), AppEvents.Listener {

    fun getBrand(): Brand? = servicesRepository.filter.brand?.value

    fun getBrandTitle(noBrandTitle: String): String = getBrand()
            ?.name.takeUnless { it.isNullOrEmpty() } ?: noBrandTitle

    fun findServices() = filtering.execute(
            servicesRepository.filter,
            object : PresenterSingleObserver<List<Service>>() {
                override fun onStart() {
                    super.onStart()
                    viewState.showProgress()
                }

                override fun onSuccess(list: List<Service>) {
                    if (list.isEmpty()) {
                        viewState.noServices()
                    } else {
                        viewState.onServicesFound(list)
                    }
                }

                override fun onError(e: Throwable) {
                    viewState.noServices()
                }
            })

    fun put(service: Service) {
        servicesRepository.selectedService = service
    }

    fun applyChips() {
        getChips.execute(
                object : PresenterSingleObserver<MutableList<Chip>>() {

                    override fun onSuccess(t: MutableList<Chip>) {
                        viewState.onChipsChanged(t)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                }
        )
    }

    override fun onFiltersChanged(filters: Filter) {
        applyChips()
        findServices()
    }

    fun onChipRemove(chip: Chip) {

        when (chip.filter) {
            is RadiusFilter -> servicesRepository.filter = servicesRepository.filter.copy(radius = null)
            is WorkingDayFilter -> servicesRepository.filter = servicesRepository.filter.copy(workingDay = null)
            is StartTimeFilter -> servicesRepository.filter = servicesRepository.filter.copy(startTime = null)
            is EndTimeFilter -> servicesRepository.filter = servicesRepository.filter.copy(endTime = null)
        }

        events.notifyListeners { it.onFiltersChanged(servicesRepository.filter) }
    }

    fun onFiltersClicked() {
        router.navigateTo(FiltersActivity.key(), servicesRepository.filter)
    }
}
