package com.oopsi.ui.services.filtered

import android.content.Context
import com.artjoker.models.*
import com.artjoker.utils.toTime
import com.oopsi.R
import java.util.*

class ChipTitles(context: Context) {
    private val formats = HashMap<Class<out FilterParam<*>>, String>()

    init {
        formats[RadiusFilter::class.java] = context.getString(R.string.distance_format)
        formats[WorkingDayFilter::class.java] = context.getString(R.string.day_format)
        formats[StartTimeFilter::class.java] = context.getString(R.string.start_format)
        formats[EndTimeFilter::class.java] = context.getString(R.string.to_format)
    }

    fun text(chip: Chip): String {
        val filter = chip.filter
        val format = formats[filter.javaClass]!!
        return when (filter) {
            is BrandFilter -> filter.value.name
            is RadiusFilter -> String.format(format, filter.value.toString())
            is WorkingDayFilter -> String.format(format, filter.value.toDay().name)
            is StartTimeFilter -> String.format(format, filter.value.toTime())
            is EndTimeFilter -> String.format(format, filter.value.toTime())
        }

    }
}
