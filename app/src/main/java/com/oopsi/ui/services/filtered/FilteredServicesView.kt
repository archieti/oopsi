package com.oopsi.ui.services.filtered

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Chip
import com.artjoker.models.Service

interface FilteredServicesView : MvpView {
    fun onServicesFound(list: List<Service>)
    fun noServices()
    fun onChipsChanged(chips: MutableList<Chip>)
    fun showProgress()
}
