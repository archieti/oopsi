package com.oopsi.ui.splash

import android.os.Bundle
import android.os.Handler
import com.oopsi.R
import com.oopsi.store.Preferences
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.ui.onboarding.OnBoardingActivity
import org.jetbrains.anko.intentFor

class SplashActivity : BaseActivity() {

    override val layoutId: Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startNextScreenAfter(2000)
    }

    private fun startNextScreenAfter(delay: Long) {
        Handler().postDelayed({
            openNextScreen()
            finish()
        }, delay)
    }

    private fun openNextScreen() {
        when {
            isFirstLaunch() -> intentFor<OnBoardingActivity>()
            else -> MainActivity.getIntent(this)
        }.let(::startActivity)
    }

    private fun isFirstLaunch(): Boolean {
        val preferences = Preferences(this, FIRST_LAUNCH_KEY)
        val boolean = preferences.getBoolean(FIRST_LAUNCH_KEY, true)
        if (boolean) {
            preferences.putBoolean(FIRST_LAUNCH_KEY, false)
        }
        return boolean
    }


    companion object {
        const val FIRST_LAUNCH_KEY = "FIRST_LAUNCH_KEY"
    }
}
