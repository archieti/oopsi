package com.oopsi.ui.scanner.search.brand

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.artjoker.models.Brand
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import org.jetbrains.anko.find

class BrandSearchRecyclerViewAdapter(data: MutableList<Brand>) : AbstractRecyclerViewAdapter<Brand>(data) {

    lateinit var context: Context

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_recycler_brand, parent, false)
        return EntityViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myOptions = RequestOptions()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
        Glide.with(context)
                .load(data[position].imageUrl)
                .apply(myOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into((holder as EntityViewHolder).image)
    }


    inner class EntityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView: CardView = itemView.find(R.id.rootView)
        val image: ImageView = itemView.find(R.id.image)

        init {
            rootView.setOnClickListener { itemClickBus.send(data[adapterPosition]) }
        }
    }
}
