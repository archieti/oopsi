package com.oopsi.ui.scanner.search.brand

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Brand

interface BrandSearchView : MvpView {
    fun showBrands(brands: List<Brand>)
}
