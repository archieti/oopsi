package com.oopsi.ui.scanner.preview;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import android.widget.ImageView.ScaleType;

import com.arellomobile.mvp.InjectViewState;
import com.artjoker.core.models.Image;
import com.artjoker.core.models.OcrDevice;
import com.artjoker.data.models.ImageBitmap;
import com.artjoker.interactors.devices.DeviceRecognitionUseCase;
import com.artjoker.repositories.DeviceRepository;
import com.artjoker.repositories.ScanningRepository;
import com.oopsi.navigation.Screens;
import com.oopsi.ui.base.BaseMvpPresenter;
import com.oopsi.ui.devices.device.DeviceActivity;

import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ScannerPreviewPresenter extends BaseMvpPresenter<ScannerPreviewView> {

    private ScanningRepository scanningRepository;
    private DeviceRepository deviceRepository;
    private DeviceRecognitionUseCase recognitionUseCase;

    @Inject
    public ScannerPreviewPresenter(
            ScanningRepository scanningRepository,
            DeviceRepository deviceRepository,
            DeviceRecognitionUseCase recognitionUseCase) {
        this.scanningRepository = scanningRepository;
        this.recognitionUseCase = recognitionUseCase;
        this.deviceRepository = deviceRepository;
    }

    public void recognize() {
        Image image = scanningRepository.getImage();
        recognitionUseCase.execute(image, new PresenterSingleObserver<OcrDevice>() {
            @Override
            public void onSuccess(OcrDevice device) {
                getViewState().onDeviceRecognized(device);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                getViewState().onDeviceRecognitionFailed();
            }
        });
    }

    @Nullable
    public void getBitmap() {
        ImageBitmap iBitmap = getBitmapFromRepository();
        if (iBitmap == null) {
            getViewState().onNotExistBitmap();
        } else {
            getViewState().onNewBitmap(iBitmap, getScaleType(iBitmap.getBitmap()));
        }
    }

    private ScaleType getScaleType(Bitmap bitmap) {
        return bitmap.getWidth() > bitmap.getHeight() ? ScaleType.FIT_CENTER : ScaleType.CENTER_CROP;
    }

    private ImageBitmap getBitmapFromRepository() {
        ImageBitmap image = (ImageBitmap) scanningRepository.getImage();
        if (image == null) return null;
        return image;
    }

    public void rotate(ImageBitmap iBitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(-90);
        disposeOnDestroy(Observable.just(iBitmap.getBitmap())
                .subscribeOn(Schedulers.io())
                .filter(bmp -> !bmp.isRecycled())
                .map(bmp -> Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rotated -> {
                    ImageBitmap imageBitmap = new ImageBitmap(rotated, iBitmap.getUrl());
                    scanningRepository.put(imageBitmap);
                    Log.e(ScannerPreviewActivity.TAG, "rotate - " + rotated.isRecycled());
                    getViewState().onNewBitmap(imageBitmap, getScaleType(rotated));
                }, Throwable::printStackTrace));

    }

    public void freeResources() {
        scanningRepository.clear();
    }

    public void onAction(boolean success, @Nullable OcrDevice device) {
        if (success) {
            router.navigateTo(DeviceActivity.Companion.key());
            if (device != null) {
                deviceRepository.putToHistory(device.getDevice());
            }
        } else {
            router.navigateTo(Screens.SEARCH_BRANDED_DEVICE);
        }
    }
}
