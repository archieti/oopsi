package com.oopsi.ui.scanner.camera

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.oopsi.R
import com.oopsi.camera.CameraBehaviour
import com.oopsi.camera.CameraService
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.scanner.preview.ScannerPreviewActivity
import com.oopsi.ui.tips.TipsActivity
import com.oopsi.ui.views.picker.model.Config
import com.oopsi.ui.views.picker.model.Image
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_scanner_camera.*
import org.jetbrains.anko.imageResource
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject


class ScannerCameraActivity : BaseActivity(), ScannerCameraView {

    @Inject
    @InjectPresenter
    lateinit var mScannerCameraPresenter: ScannerCameraPresenter

    @ProvidePresenter()
    fun providePresenter() = mScannerCameraPresenter

    override val layoutId: Int = R.layout.activity_scanner_camera

    private lateinit var cameraService: CameraBehaviour

    private lateinit var rxPermissions: RxPermissions
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rxPermissions = RxPermissions(this)
        cameraService = CameraService(camera)
        mScannerCameraPresenter.attachCamera(cameraService)
        flash.setOnClickListener { flashState(mScannerCameraPresenter.toggleFlash()) }
        close.setOnClickListener { finish() }
        tips.setOnClickListener { router.navigateTo(TipsActivity.key()) }
        manual.setOnClickListener { router.navigateTo(Screens.SEARCH_BRANDED_DEVICE) }
        gallery.setOnClickListener { mScannerCameraPresenter.openGallery(this) }
        takePhoto.setOnClickListener {
            if (rxPermissions.isGranted(Manifest.permission.CAMERA) && rxPermissions.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showProgress()
                takePhoto.isEnabled = false
                mScannerCameraPresenter.takePhoto()
            }

        }
    }

    private fun flashState(toggleFlash: Boolean) {
        flash.imageResource = when {
            toggleFlash -> R.drawable.ic_flash_on
            else -> R.drawable.ic_flash_off
        }

        val fs = R.string.on.takeIf { toggleFlash } ?: R.string.off
        flashState.setText(fs)
    }

    @SuppressLint("CheckResult")
    override fun onResume() {
        super.onResume()
        takePhoto.isEnabled = true
        when {
            !rxPermissions.isGranted(Manifest.permission.CAMERA)
                    || !rxPermissions.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribe { cameraService.onResume() }
            }
            else -> cameraService.onResume()
        }
        framePreview.setImageBitmap(null)
    }

    override fun onPause() {
        cameraService.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraService.onDestroy()
    }

    override fun showPreview(bitmap: Bitmap) {
        if (!bitmap.isRecycled) {
            framePreview.setImageBitmap(bitmap)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            val images = data.getParcelableArrayListExtra<Image>(Config.EXTRA_IMAGES)
            mScannerCameraPresenter.onResult(images[0].path)
        }
    }

    override val navigator: Navigator = object : BaseNavigator() {

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    ScannerPreviewActivity.key() -> ScannerPreviewActivity.getIntent(this@ScannerCameraActivity)
                    TipsActivity.key() -> TipsActivity.getIntent(this@ScannerCameraActivity)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }

    override fun onPhotoTaken() {

        Handler(Looper.getMainLooper()).post {
            hideProgress()
            router.navigateTo(ScannerPreviewActivity.key())
        }

    }

    companion object {
        fun getIntent(context: Context) = Intent(context, ScannerCameraActivity::class.java)
        fun key(): String = ScannerCameraActivity::class.java.simpleName
    }
}
