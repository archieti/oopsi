package com.oopsi.ui.scanner.camera;

import android.graphics.Bitmap;

import com.arellomobile.mvp.MvpView;

public interface ScannerCameraView extends MvpView {

    void onPhotoTaken();

    void showPreview(Bitmap bitmap);
}
