package com.oopsi.ui.scanner.search

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Device
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_device.view.*


open class DevicesAdapter constructor(data: MutableList<Device>) : AbstractRecyclerViewAdapter<Device>(data) {

    lateinit var context: Context

    open val layoutRes = R.layout.item_device

    open fun viewHolder(view: View): EntityViewHolder = EntityViewHolder(view)

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(layoutRes, parent, false)
        return viewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EntityViewHolder).bind(data[position])
    }


    open inner class EntityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.container.setOnClickListener { itemClickBus.send(data[adapterPosition]) }
        }

        fun bind(device: Device) {
            itemView.brandName.text = device.brand.name
            itemView.modelName.text = device.model
            val myOptions = RequestOptions()
                    .centerCrop()
            val thumb: String = device.thumbs.firstOrNull() ?: ""
            Glide.with(context)
                    .load(thumb)
                    .apply(myOptions)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(itemView.image)
        }
    }
}