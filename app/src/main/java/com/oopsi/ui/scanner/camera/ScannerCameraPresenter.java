package com.oopsi.ui.scanner.camera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.artjoker.core.models.Image;
import com.artjoker.data.models.ImageBitmap;
import com.artjoker.repositories.ScanningRepository;
import com.artjoker.repositories.ScanningRepositoryKt;
import com.oopsi.camera.CameraBehaviour;
import com.oopsi.ui.base.BaseMvpPresenter;
import com.oopsi.ui.views.picker.ui.imagepicker.ImagePicker;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class ScannerCameraPresenter extends BaseMvpPresenter<ScannerCameraView> {

    private CameraBehaviour cameraService;
    private boolean isFlashEnabled = false;
    private Disposable subscribe;
    private ScanningRepository scanningRepository;

    @Inject
    public ScannerCameraPresenter(ScanningRepository scanningRepository) {
        this.scanningRepository = scanningRepository;
    }

    public void attachCamera(@NotNull CameraBehaviour cameraService) {
        this.cameraService = cameraService;
    }

    public boolean toggleFlash() {
        isFlashEnabled = !isFlashEnabled;
        cameraService.flash(isFlashEnabled);
        return isFlashEnabled;
    }

    @SuppressLint("CheckResult")
    public void takePhoto() {
        getViewState().showPreview(cameraService.takePhoto(bitmap -> {
            ImageBitmap image = new ImageBitmap(bitmap, "");
            String url = ScanningRepositoryKt.saveToGallery(scanningRepository, image);
            ImageBitmap imageWithUrl = new ImageBitmap(bitmap, url);
            scanningRepository.put(imageWithUrl);
            getViewState().onPhotoTaken();
        }));
    }

    public void onResult(String path) {
        Image image = scanningRepository.getFromGallery(path);
        assert image != null;
        scanningRepository.put(image);
        getViewState().onPhotoTaken();
    }

    public void openGallery(Activity activity) {
        ImagePicker.with(activity)
                .setShowCamera(false)
                .setToolbarColor("#3b5998")
                .setKeepScreenOn(true)
                .setMultipleMode(false)
                .start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("---", "clear camera");
        scanningRepository.clear();
    }
}
