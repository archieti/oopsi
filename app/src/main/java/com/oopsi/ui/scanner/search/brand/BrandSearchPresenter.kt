package com.oopsi.ui.scanner.search.brand

import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.BrandsUseCase
import com.artjoker.models.Brand
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class BrandSearchPresenter @Inject constructor(private val brandsUseCase: BrandsUseCase) : BaseMvpPresenter<BrandSearchView>() {

    fun searchBrands(searchQuery: String) = brandsUseCase.execute(
            searchQuery,
            object : PresenterSingleObserver<List<Brand>>() {
                override fun onSuccess(brands: List<Brand>) {
                    viewState.showBrands(brands)
                }

                override fun onError(e: Throwable) {
                    viewState.showBrands(emptyList())
                    e.printStackTrace()
                }
            })
}
