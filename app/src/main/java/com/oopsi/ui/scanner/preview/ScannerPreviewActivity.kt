package com.oopsi.ui.scanner.preview

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.core.models.OcrDevice
import com.artjoker.data.models.ImageBitmap
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.scanner.maybe.MaybeDevicesActivity
import com.oopsi.ui.views.result.Fail
import com.oopsi.ui.views.result.Result
import com.oopsi.ui.views.result.Success
import kotlinx.android.synthetic.main.activity_scanner_preview.*
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class ScannerPreviewActivity : BaseActivity(), ScannerPreviewView {

    override val layoutId: Int = R.layout.activity_scanner_preview

    @Inject
    @InjectPresenter
    lateinit var mScannerPreviewPresenter: ScannerPreviewPresenter

    @ProvidePresenter()
    fun providePresenter() = mScannerPreviewPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerPreviewPresenter.getBitmap()
        next.setOnClickListener {
            showProgress()
            mScannerPreviewPresenter.recognize()
        }
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun showDialog(isSuccess: Boolean, ocrDevice: OcrDevice? = null) {
        val builder = getDialog(isSuccess)
                .builder()
                .message(if (isSuccess) R.string.success_recognition_result else R.string.fail_recognition_result)
                .actionText(if (isSuccess) R.string.confirm else R.string.result_dialog_enter_manually)
                .title(if (isSuccess) R.string.success_title else R.string.sorry)
                .action(getAction(isSuccess, ocrDevice))

        if (isSuccess) {
            builder.alternativeActionText(R.string.search_more)
                    .alternativeAction(DialogInterface.OnClickListener { dialog, _ ->
                        router.navigateTo(Screens.MAYBE_YOU_LOOKING, ocrDevice!!.ocrText)
                        dialog.dismiss()
                    })
                    .resultDeviceModel(ocrDevice!!.device.model)
                    .resultDeviceBrand(ocrDevice.device.brand.name)
                    .resultImageUrl(ocrDevice.device.backgroundImage)
        }

        builder.create().show()
    }

    private fun getAction(success: Boolean, device: OcrDevice?): DialogInterface.OnClickListener =
            DialogInterface.OnClickListener { dialog, _ ->
                mScannerPreviewPresenter.onAction(success, device)
                dialog.dismiss()
            }


    private fun getDialog(isSuccess: Boolean): Result = when (isSuccess) {
        true -> Success(this)
        else -> Fail(this)
    }

    override fun onDeviceRecognitionFailed() {
        hideProgress()
        showDialog(false)
    }

    override fun onDeviceRecognized(device: OcrDevice) {
        hideProgress()
        showDialog(true, device)
    }

    override fun onNewBitmap(iBitmap: ImageBitmap, scale: ImageView.ScaleType) {
        rotate.isEnabled = true
        preview.scaleType = scale
        if (!iBitmap.bitmap.isRecycled) {
            preview.setImageBitmap(iBitmap.bitmap)
            rotate.setOnClickListener(null)
            rotate.setOnClickListener {
                rotate.isEnabled = false
                mScannerPreviewPresenter.rotate(iBitmap)
            }
        }
    }

    override fun onNotExistBitmap() {
        finish()
    }

    override fun onDestroy() {
        if (getInstancesCount() == 1) {
            mScannerPreviewPresenter.freeResources()
        }
        super.onDestroy()
    }


    override val navigator: Navigator = object : BaseNavigator() {

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    Screens.MAYBE_YOU_LOOKING -> intentFor<MaybeDevicesActivity>(MaybeDevicesActivity.EXT_OCR_STRING to data)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        const val TAG: String = "Preview"

        fun getIntent(context: Context) = Intent(context, ScannerPreviewActivity::class.java)

        fun key(): String = ScannerPreviewActivity::class.java.simpleName
    }
}
