package com.oopsi.ui.scanner.search

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.core.models.SearchDeviceData
import com.artjoker.interactors.devices.DeviceSearchInteractor
import com.artjoker.models.Brand
import com.artjoker.models.BrandFilter
import com.artjoker.models.Device
import com.artjoker.models.Filter
import com.artjoker.repositories.DeviceRepository
import com.artjoker.repositories.ServicesRepository
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.devices.device.DeviceActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.utils.SimpleTextWatcher
import com.oopsi.utils.setVisible
import io.reactivex.observers.DisposableObserver
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_search_device.*
import org.jetbrains.anko.intentFor
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchDeviceActivity : BaseActivity(), SearchDeviceView {

    override val layoutId = R.layout.activity_search_device

    @Inject
    @InjectPresenter
    lateinit var presenter: SearchDevicePresenter
    @Inject
    lateinit var servicesRepository: ServicesRepository

    private val devicesAdapter by lazy { DevicesAdapter(mutableListOf()) }
    private var brand: Brand? = null
        get() = field ?: intent.extras?.getSerializable(EXT_BRAND) as? Brand

    @ProvidePresenter()
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        router.setResultListener(BRAND_RESULT_CODE) {
            brand = it as Brand
            brandButton.text = brand!!.name
            if (modelEditText.text.toString().isNotEmpty()) {
                doSearch()
            }
            servicesRepository.filter = Filter(brand = BrandFilter(brand!!))
        }
        brandButton.setOnClickListener {
            router.navigateTo(Screens.SEARCH_BRAND, BRAND_RESULT_CODE)
        }

        with(toolbar) {
            inflateMenu(R.menu.menu_close)
            setNavigationOnClickListener { router.exit() }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.close -> {
                        val intent = intentFor<MainActivity>().apply {
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        }
                        startActivity(intent)
                        finish()
                        true
                    }
                    else -> false
                }
            }
        }

        devicesAdapter.itemClickBusObs
                .subscribe(presenter::onDevice)
                .let(::manage)
        devices.adapter = devicesAdapter

        modelEditText.addTextChangedListener(object : SimpleTextWatcher() {

            override fun afterTextChanged(s: Editable?) {
                if (brandButton.text != getString(R.string.choose_brand)) {
                    doSearch()
                }
            }
        })

        searchServices.setOnClickListener {
            presenter.onSearchServices(modelEditText.text.toString(), brand)
        }


        brand?.let {
            brandButton.text = it.name
        }
    }

    override fun loading(loading: Boolean) {
        progressContainer.setVisible(loading)
        devices.setVisible(!loading)
        if (loading) {
            noDevicesFoundVisibility(false)
        }
    }

    override fun onLoaded(newDevices: List<Device>) {
        noDevicesFoundVisibility(newDevices.isEmpty(), true)
        devices.setVisible(!newDevices.isEmpty())
        devicesAdapter.data = newDevices.toMutableList()
        devicesAdapter.notifyDataSetChanged()
    }

    private fun noDevicesFoundVisibility(visibility: Boolean, bySearch: Boolean = false) {
        noDevicesFound.setVisible(visibility)
        when {
            bySearch -> R.string.device_not_found_but_service
            else -> R.string.enter_brand_model
        }.let(noDevicesFoundDesc::setText)

        searchServices.setVisible(visibility && bySearch)
    }

    private fun doSearch() {
        presenter.search(modelEditText.text.toString(), brandButton.text.toString())
    }


    companion object {
        private const val BRAND_RESULT_CODE = 421
        const val EXT_BRAND = "EXT_BRAND"
    }
}


interface SearchDeviceView : MvpView {
    fun loading(loading: Boolean)
    fun onLoaded(newDevices: List<Device>)
}


@InjectViewState
class SearchDevicePresenter @Inject constructor() : BaseMvpPresenter<SearchDeviceView>() {

    @Inject lateinit var deviceSearch: DeviceSearchInteractor
    @Inject lateinit var deviceRepository: DeviceRepository
    @Inject lateinit var servicesRepository: ServicesRepository

    private var publishSubject: PublishSubject<SearchDeviceData>? = null
    private var searchDisposable: DisposableObserver<List<Device>>? = null

    override fun onDestroy() {
        super.onDestroy()
        flush()
    }

    fun search(model: String, brand: String) {

        if (model.isEmpty()) {

            viewState.loading(false)
            viewState.onLoaded(emptyList())

            flush()

            return
        }

        viewState.loading(true)

        if (publishSubject == null) {
            publishSubject = PublishSubject.create()
            searchDisposable = publishSubject!!.debounce(400, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .switchMap(deviceSearch::execute)
                    .subscribeWith(SearchDeviceObservable())
        }
        publishSubject!!.onNext(SearchDeviceData(model, brand))
    }

    fun onDevice(it: Device) {
        deviceRepository.device = it
        deviceRepository.putToHistory(it)
        router.navigateTo(DeviceActivity.key())
    }

    private fun flush() {
        searchDisposable?.dispose()
        searchDisposable = null
        publishSubject = null
    }

    fun onSearchServices(model: String, brand: Brand?) {
        brand?.let {
            val device = Device(UUID.randomUUID().toString(), model, it)
            deviceRepository.putToHistory(device)
            servicesRepository.filter = Filter(brand = BrandFilter(brand))
        }
        router.navigateTo(FilteredServicesActivity.key())
    }


    inner class SearchDeviceObservable : DisposableObserver<List<Device>>() {

        override fun onNext(t: List<Device>) {
            viewState.loading(false)
            viewState.onLoaded(t)
        }

        override fun onComplete() {
            viewState.loading(false)
        }

        override fun onError(e: Throwable) {
            viewState.loading(false)
        }
    }
}
