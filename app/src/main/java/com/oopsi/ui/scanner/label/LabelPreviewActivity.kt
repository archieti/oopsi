package com.oopsi.ui.scanner.label

import android.graphics.Bitmap
import android.os.Bundle
import android.widget.ImageView
import android.widget.ImageView.ScaleType.CENTER_CROP
import android.widget.ImageView.ScaleType.FIT_CENTER
import com.artjoker.data.models.ImageBitmap
import com.artjoker.data.repositories.ScannerRepository
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_label_preview.*
import javax.inject.Inject


class LabelPreviewActivity : BaseActivity() {

    @Inject
    lateinit var scannerRepository: ScannerRepository

    override val layoutId: Int = R.layout.activity_label_preview

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val url = intent.extras!!.getString(EXT_URL)!!
        val image = scannerRepository.getFromGallery(url) as? ImageBitmap

        when {
            image != null -> onNewBitmap(image.bitmap, getScaleType(image.bitmap))
            else -> router.exitWithMessage(getString(R.string.imagepicker_error_images_not_exist))
        }

        toolbar.setNavigationOnClickListener { finish() }
    }


    private fun onNewBitmap(bitmap: Bitmap, scale: ImageView.ScaleType) {
        preview.scaleType = scale
        if (!bitmap.isRecycled) {
            preview.setImageBitmap(bitmap)
        }
    }


    private fun getScaleType(bitmap: Bitmap) =
            when {
                bitmap.width > bitmap.height -> FIT_CENTER
                else -> CENTER_CROP
            }


    companion object {
        const val EXT_URL = "EXT_URL"

        fun key(): String = LabelPreviewActivity::class.java.simpleName
    }
}
