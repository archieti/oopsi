package com.oopsi.ui.scanner.search.brand

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Brand
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.scanner.search.SearchDeviceActivity
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.activity_brand_search.*
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.Navigator
import java.io.Serializable
import javax.inject.Inject


class BrandSearchActivity : BaseActivity(), BrandSearchView {

    override val layoutId: Int = R.layout.activity_brand_search
    override val navigator: Navigator = NavigatorImpl()

    @Inject
    @InjectPresenter
    lateinit var mBrandSearchPresenter: BrandSearchPresenter

    @ProvidePresenter
    fun provideSearchPresenter() = mBrandSearchPresenter

    private lateinit var adapterBrand: BrandSearchRecyclerViewAdapter
    private lateinit var layoutManager: GridLayoutManager

    private val action by lazy { intent.extras!!.getSerializable(EXT_ACTION) as Action }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initListeners()
        mBrandSearchPresenter.searchBrands("")
        showProgress()
    }

    private fun initViews() {
        adapterBrand = BrandSearchRecyclerViewAdapter(mutableListOf())
        layoutManager = GridLayoutManager(this, 2)
        entityRecyclerView.adapter = adapterBrand
        entityRecyclerView.layoutManager = layoutManager
    }

    private fun initListeners() {
        adapterBrand.itemClickBusObs
                .subscribe { brand ->
                    when (action) {
                        is Action.Return -> router.exitWithResult((action as Action.Return).brandResultCode, brand)
                        is Action.SearchDevice -> router.navigateTo(Screens.SEARCH_DEVICE, brand)
                    }
                }
                .let(::manage)

        toolbar.setNavigationOnClickListener { router.exit() }

        modelEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                showProgress()
                showBrands(emptyList())
                mBrandSearchPresenter.searchBrands(s?.toString().orEmpty())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }
        })
    }

    override fun showProgress() = progress.setVisible(true)

    override fun hideProgress() = progress.setVisible(false)

    override fun showBrands(brands: List<Brand>) {
        hideProgress()
        adapterBrand.data = brands.toMutableList()
        adapterBrand.notifyDataSetChanged()
    }


    private inner class NavigatorImpl : BaseNavigator() {
        override fun createActivityIntent(context: Context, screenKey: String, data: Any?): Intent? =
                when (screenKey) {
                    Screens.SEARCH_DEVICE -> intentFor<SearchDeviceActivity>(SearchDeviceActivity.EXT_BRAND to data)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        const val EXT_ACTION = "EXT_ACTION"
    }


    sealed class Action : Serializable {

        class Return(val brandResultCode: Int) : Action(), Serializable

        object SearchDevice : Action(), Serializable
    }

}
