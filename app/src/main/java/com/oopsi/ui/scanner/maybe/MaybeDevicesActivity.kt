package com.oopsi.ui.scanner.maybe

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.interactors.devices.MaybeDevicesInteractor
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.devices.device.DeviceActivity
import com.oopsi.ui.scanner.search.DevicesAdapter
import com.oopsi.utils.nonCancelable
import com.oopsi.utils.screenKey
import com.oopsi.utils.setVisible
import com.oopsi.utils.tintBtns
import kotlinx.android.synthetic.main.activity_maybe_devices.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import ru.terrakok.cicerone.commands.Command
import javax.inject.Inject


class MaybeDevicesActivity : BaseActivity(), MaybeDevicesView {

    override val layoutId = R.layout.activity_maybe_devices
    override val navigator: BaseNavigator = NavigatorImpl()

    @Inject
    @InjectPresenter
    lateinit var presenter: MaybeDevicesPresenter

    @ProvidePresenter
    fun provideMaybeDevicesPresenter() = presenter

    private val devicesAdapter by lazy { DevicesAdapter(mutableListOf()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(toolbar) {
            inflateMenu(R.menu.menu_manually)
            setNavigationOnClickListener { router.exit() }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.change -> {
                        router.navigateTo(Screens.SEARCH_BRANDED_DEVICE)
                        true
                    }
                    else -> false
                }
            }
        }

        devicesAdapter.itemClickBusObs
                .subscribe(presenter::onDevice)
                .also { manage(it) }
        maybeDevicesRecycler.adapter = devicesAdapter

        val ocrString = intent?.extras?.getString(EXT_OCR_STRING)
                ?: throw RuntimeException("OCRString")
        presenter.init(ocrString)
    }

    override fun loading(loading: Boolean) {
        progressContainer.setVisible(loading)
        maybeDevicesRecycler.setVisible(!loading)
    }

    override fun onTooLongRequest() {
        progressDesc.setText(R.string.too_long_searching)
    }

    override fun onLoaded(newDevices: List<Device>) {

        noDevicesFound.setVisible(newDevices.isEmpty())

        devicesAdapter.data = newDevices.toMutableList()
        devicesAdapter.notifyDataSetChanged()
    }

    override fun onTimeout(t: Throwable) {
        router.navigateTo(Screens.TIMEOUT)
    }


    companion object {
        const val EXT_OCR_STRING = "EXT_OCR_STRING"
    }


    private inner class NavigatorImpl : BaseNavigator() {

        override fun applyCommand(command: Command) {
            when (command.screenKey()) {
                Screens.TIMEOUT -> alert(
                        getString(R.string.msg_timeout),
                        getString(R.string.title_timeout)
                ) {

                    positiveButton(R.string.result_dialog_enter_manually) {
                        router.replaceScreen(Screens.SEARCH_BRANDED_DEVICE)
                    }
                    cancelButton {
                        router.exit()
                    }
                }.replace().nonCancelable().tintBtns()

                else -> super.applyCommand(command)
            }
        }
    }
}


interface MaybeDevicesView : MvpView {
    fun loading(loading: Boolean)
    fun onLoaded(newDevices: List<Device>)
    fun onTooLongRequest()
}


@InjectViewState
class MaybeDevicesPresenter @Inject constructor(
        val deviceRepository: DeviceRepository,
        val maybeDevicesInteractor: MaybeDevicesInteractor
) : BaseMvpPresenter<MaybeDevicesView>() {

    fun onDevice(it: Device) {
        deviceRepository.device = it
        deviceRepository.putToHistory(it)
        router.navigateTo(DeviceActivity.key())
    }

    fun init(ocrString: String) {
        maybeDevicesInteractor.execute(ocrString, object : PresenterObserver<MaybeDevicesInteractor.Result>() {

            override fun onStart() {
                super.onStart()
                viewState.loading(true)
            }

            override fun onNext(t: MaybeDevicesInteractor.Result) {
                when (t) {
                    is MaybeDevicesInteractor.Result.Success -> viewState.onLoaded(t.devices)
                    is MaybeDevicesInteractor.Result.TooLongRequest -> viewState.onTooLongRequest()
                }
            }

            override fun onComplete() {
                viewState.loading(false)
            }

            override fun onError(e: Throwable) {
                viewState.loading(false)
            }
        })
    }
}
