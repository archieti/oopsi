package com.oopsi.ui.scanner.preview;

import android.widget.ImageView;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.artjoker.core.models.OcrDevice;
import com.artjoker.data.models.ImageBitmap;

public interface ScannerPreviewView extends MvpView {

    @StateStrategyType(SingleStateStrategy.class)
    void onNewBitmap(ImageBitmap iBitmap, ImageView.ScaleType scaleType);

    void onNotExistBitmap();

    void onDeviceRecognitionFailed();

    void onDeviceRecognized(OcrDevice device);
}
