package com.oopsi.ui.views

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    var pagingEnabled = false

    override fun onTouchEvent(event: MotionEvent) = this.pagingEnabled && super.onTouchEvent(event)
    override fun onInterceptTouchEvent(event: MotionEvent) = this.pagingEnabled && super.onInterceptTouchEvent(event)
}
