package com.oopsi.ui.views.picker.ui.camera;


import com.oopsi.ui.views.picker.model.Image;
import com.oopsi.ui.views.picker.ui.common.MvpView;

import java.util.List;

/**
 * Created by hoanglam on 8/22/17.
 */

public interface CameraView extends MvpView {

    void finishPickImages(List<Image> images);
}
