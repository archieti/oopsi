package com.oopsi.ui.views.picker.listener;

/**
 * Created by hoanglam on 8/17/17.
 */

public interface OnBackAction {
    void onBackToFolder();

    void onFinishImagePicker();
}
