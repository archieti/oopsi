package com.oopsi.ui.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Size;
import android.view.MotionEvent;
import android.view.View;

import com.oopsi.R;
import com.oopsi.ui.tips.InfinityPager;


public class IndicatorsView extends View implements ViewPager.OnPageChangeListener {

    private Drawable mSelectedDrawable;
    private Drawable mUnSelectedDrawable;

    private Bitmap mUnSelectedBitmap;
    private Bitmap mSelectedBitmap;

    private Rect mRect;
    private Rect mTempRect;

    // Default size
    private Size mIndicatorSize = new Size(26, 13);
//    private int mIndicatorSelectedSize = 13;

    // Default padding between indicators
    private int mPaddingBetweenIndicators = 7;

    private int mNumOfIndicators = 3;
    private int mSelectedIndicator = 0;

    private int mLeftBound;
    private int mTopBound;
    private int mTotalWidthWeNeed;
    private boolean mIndicatorsClickChangePage;
    private ViewPager mViewPager;

    private OnIndicatorClickListener mOnIndicatorClickListener;
    private boolean mSmoothTransitionEnabled;
    private boolean mDiscreteIndicator;
    private float mCurrentPositionOffset;
    private float mCurrentPosition;
    private Rect mSelectedRect;
    private Paint clearPaint;
    private boolean mInfinityPager;

    public IndicatorsView(Context context) {
        this(context, null);
    }

    public IndicatorsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // Helper
    public static Bitmap drawableToBitmap(Drawable drawable, Size size) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }


        Bitmap bitmap = Bitmap.createBitmap(size.getWidth(), size.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        setDefaults(context);
        getDataFromAttributes(context, attrs);
        convertDrawablesToBitmaps();

        mTempRect = new Rect();

        clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        clearPaint.setColor(getResources().getColor(android.R.color.transparent));
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        //clearPaint.setColor(Color.BLACK);
        //clearPaint.setStyle(Paint.Style.FILL);
    }

    private void setDefaults(Context context) {
        float density = getResources().getDisplayMetrics().density;

        mIndicatorSize = new Size((int) (mIndicatorSize.getWidth() * density), (int) (mIndicatorSize.getHeight() * density));

        mPaddingBetweenIndicators = (int) (mPaddingBetweenIndicators * density);

        mSelectedDrawable = ContextCompat.getDrawable(context, R.drawable.indicator_selected);
        mUnSelectedDrawable = ContextCompat.getDrawable(context, R.drawable.indicator_unselected);
    }

    private void getDataFromAttributes(Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.IndicatorsView, 0, 0);

            // Get indicators if exist
            Drawable selectedDrawable = a.getDrawable(R.styleable.IndicatorsView_selectedDrawable);
            if (selectedDrawable != null) {
                mSelectedDrawable = selectedDrawable;
            }
            Drawable unSelectedDrawable = a.getDrawable(R.styleable.IndicatorsView_unSelectedDrawable);
            if (unSelectedDrawable != null) {
                mUnSelectedDrawable = unSelectedDrawable;
            }

            // Get indicator size
            int width = (int) a.getDimension(R.styleable.IndicatorsView_indicatorWidth, mIndicatorSize.getWidth());
            int height = (int) a.getDimension(R.styleable.IndicatorsView_indicatorHeight, mIndicatorSize.getHeight());
            mIndicatorSize = new Size(width, height);
            // Get padding between indicators
            mPaddingBetweenIndicators = (int) a.getDimension(R.styleable.IndicatorsView_paddingBetweenIndicators, mPaddingBetweenIndicators);

            // Get number of indicators
            mNumOfIndicators = a.getInteger(R.styleable.IndicatorsView_numberOfIndicators, mNumOfIndicators);

            // Get selected indicator
            mSelectedIndicator = a.getInteger(R.styleable.IndicatorsView_selectedIndicator, mSelectedIndicator);

            a.recycle();
        }
    }

    private void convertDrawablesToBitmaps() {
        mRect = new Rect(0, 0, mIndicatorSize.getWidth(), mIndicatorSize.getHeight());
        mUnSelectedBitmap = drawableToBitmap(mUnSelectedDrawable, mIndicatorSize);

        mSelectedRect = new Rect(0, 0, mIndicatorSize.getWidth(), mIndicatorSize.getHeight());
        mSelectedBitmap = drawableToBitmap(mSelectedDrawable, mIndicatorSize);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);


        int desiredWidth = mIndicatorSize.getWidth() * mNumOfIndicators
                + mPaddingBetweenIndicators * (mNumOfIndicators - 1)
                + mIndicatorSize.getWidth() - mIndicatorSize.getWidth();
        int desiredHeight = Math.max(mIndicatorSize.getHeight(), mIndicatorSize.getHeight());

        desiredWidth += getPaddingLeft() + getPaddingRight();
        desiredHeight += getPaddingTop() + getPaddingBottom();

        int width = desiredWidth;
        int height = desiredHeight;


        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;  // Must be this size (match_parent or exactly dayIndex)
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize); // (wrap_content)
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize; // Must be this size (match_parent or exactly dayIndex)
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);   // (wrap_content)
        }

        changeIndicatorSizeIfNecessary(width - getPaddingLeft() - getPaddingRight(), height - getPaddingTop() - getPaddingBottom());

        setMeasuredDimension(width, height);
    }

    private void changeIndicatorSizeIfNecessary(int width, int height) {
        boolean isIndicatorSizeChanged = false;

        int diffWithSelectedSize = 0;
        int allIndicatorsWidth = mIndicatorSize.getWidth() * mNumOfIndicators + diffWithSelectedSize;
        int paddingBetweenIndicators = (mNumOfIndicators - 1) * mPaddingBetweenIndicators;

        // if width is not wide enough
        if (allIndicatorsWidth + paddingBetweenIndicators > width) {
            width -= paddingBetweenIndicators - diffWithSelectedSize;
            mIndicatorSize = new Size(width / mNumOfIndicators, mIndicatorSize.getHeight());
            isIndicatorSizeChanged = true;
        }

        // if height is not high enough
        if (mIndicatorSize.getHeight() > height) {
            mIndicatorSize = new Size(mIndicatorSize.getWidth(), height);
            isIndicatorSizeChanged = true;
        }

        if (isIndicatorSizeChanged) {
            convertDrawablesToBitmaps();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int hCenter = (getWidth() - getPaddingLeft() - getPaddingRight()) / 2;
        final int vCenter = (getHeight() - getPaddingTop() - getPaddingBottom()) / 2;

        int diffWithSelectedSize = 0;
        int allIndicatorsWidth = mIndicatorSize.getWidth() * mNumOfIndicators + diffWithSelectedSize;
        int paddingBetweenIndicators = (mNumOfIndicators - 1) * mPaddingBetweenIndicators;

        mTotalWidthWeNeed = allIndicatorsWidth + paddingBetweenIndicators;
        mLeftBound = hCenter - mTotalWidthWeNeed / 2 + getPaddingLeft();
        //mLeftBound = hCenter - mTotalWidthWeNeed / 2 + Math.round(getPaddingLeft() / mDensity);
        mTopBound = vCenter - mIndicatorSize.getHeight() / 2 + getPaddingTop();
        int topSelectedBound = vCenter - mIndicatorSize.getHeight() / 2 + getPaddingTop();
        //mTopBound = vCenter - mIndicatorSize / 2 + Math.round(getPaddingTop() / mDensity);

        mRect.offsetTo(mLeftBound, mTopBound);
        mSelectedRect.offsetTo(mLeftBound, topSelectedBound);
        for (int i = 0; i < mNumOfIndicators; i++) {
            int offset = 0;

            //Log.e("ZAQ", "mSelectedIndicator: " + mSelectedIndicator + " mCurrentPosition: " + mCurrentPosition);
            if (i == mSelectedIndicator) {
                offset = 0;
            }

            if (i != mSelectedIndicator || mSmoothTransitionEnabled) {
                canvas.drawBitmap(mUnSelectedBitmap, null, mRect, null);
            } else {
                canvas.drawBitmap(mSelectedBitmap, null, mSelectedRect, null);
            }

            if (i == mCurrentPosition && mSmoothTransitionEnabled) {
                mTempRect.set(mSelectedRect);
            }

            mRect.offset(mIndicatorSize.getWidth() + offset + mPaddingBetweenIndicators, 0);
            mSelectedRect.offset(mIndicatorSize.getWidth() + offset + mPaddingBetweenIndicators, 0);

        }

        if (mSmoothTransitionEnabled) {
            int offset = Math.round((mIndicatorSize.getWidth() + mPaddingBetweenIndicators) * mCurrentPositionOffset);
            mTempRect.offset(offset, 0);
            canvas.drawBitmap(mSelectedBitmap, null, mTempRect, null);
        }
        if (mDiscreteIndicator) {
            for (int i = 1; i < mNumOfIndicators; i++) {
                int paddingStart = mLeftBound + mIndicatorSize.getWidth() * i + mPaddingBetweenIndicators * (i - 1);
                canvas.drawRect(paddingStart, mTopBound, paddingStart + mPaddingBetweenIndicators, mIndicatorSize.getHeight(), clearPaint);
            }
        }
        if (mInfinityPager) {
            int paddingStart = mLeftBound + mIndicatorSize.getWidth() * mNumOfIndicators + mPaddingBetweenIndicators * (mNumOfIndicators - 1);
            canvas.drawRect(paddingStart, mTopBound, paddingStart + mIndicatorSize.getWidth() * 2, mIndicatorSize.getHeight(), clearPaint);
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mIndicatorsClickChangePage && mOnIndicatorClickListener != null) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    int x = (int) event.getX();
                    int y = (int) event.getY();
                    int indicatorNumberClicked = getNumberOfCLickedIndicator(x, y);
                    if (indicatorNumberClicked > -1) {
                        onIndicatorClick(indicatorNumberClicked);
                    }
                    break;
                case MotionEvent.ACTION_CANCEL:
                    break;
            }
            return true;
        }
        return false;
    }

    private int getNumberOfCLickedIndicator(int x, int y) {
        if (y < mTopBound - mIndicatorSize.getHeight()
                || y > (mTopBound + mIndicatorSize.getHeight())
                || x < mLeftBound - mPaddingBetweenIndicators / 2
                || x > (mLeftBound + mTotalWidthWeNeed + mPaddingBetweenIndicators / 2)) {
            return -1;
        }

        int x2 = x - mLeftBound;
        final int firstIndicatorWidth = mIndicatorSize.getWidth() + mPaddingBetweenIndicators / 2;

        if (x2 < firstIndicatorWidth) {
            return 0;
        }

        int count = 1;
        x2 -= firstIndicatorWidth;
        final int indicatorWidth = mIndicatorSize.getWidth() + mPaddingBetweenIndicators;

        while (x2 > indicatorWidth) {
            count++;
            x2 -= indicatorWidth;
        }

        return count;
    }

    private void onIndicatorClick(int indicatorNumberClicked) {
        if (mIndicatorsClickChangePage && mViewPager != null) {
            mViewPager.setCurrentItem(indicatorNumberClicked);
        }

        if (mOnIndicatorClickListener != null) {
            mOnIndicatorClickListener.onClick(indicatorNumberClicked);
        }
    }

    // Control
    public void setSelectedIndicator(int selectedIndicator) {
        mSelectedIndicator = selectedIndicator;
        invalidate();
    }

    public void setViewPager(ViewPager viewPager) {
        mViewPager = viewPager;
        if (mViewPager.getAdapter() instanceof InfinityPager) {
            mNumOfIndicators = ((InfinityPager) mViewPager.getAdapter()).getRealCount();
            mInfinityPager = true;
        } else {
            mNumOfIndicators = mViewPager.getAdapter().getCount();
        }
        mSelectedIndicator = mViewPager.getCurrentItem();
        mViewPager.addOnPageChangeListener(this);

        invalidate();
    }

    public void setSmoothTransition(boolean newValue) {
        mSmoothTransitionEnabled = newValue;
    }

    public void setDiscreteIndicator(boolean mDiscreteIndicator) {
        this.mDiscreteIndicator = mDiscreteIndicator;
    }

    public void setIndicatorsClickChangePage(boolean newValue) {
        mIndicatorsClickChangePage = newValue;
    }

    public void setIndicatorsClickListener(OnIndicatorClickListener listener) {
        mOnIndicatorClickListener = listener;
    }

    // ViewPager
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mSmoothTransitionEnabled) {
            mCurrentPositionOffset = positionOffset;
            if (mInfinityPager) {
                mCurrentPosition = position % ((InfinityPager) mViewPager.getAdapter()).getRealCount();
            } else {
                mCurrentPosition = position;
            }
            invalidate();
        }
    }

    @Override
    public void onPageSelected(int position) {
        setSelectedIndicator(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void setSelectedDrawable(Drawable drawable) {
        mSelectedDrawable = drawable;
        convertDrawablesToBitmaps();
        invalidate();
    }

    public void setUnSelectedDrawable(Drawable drawable) {
        mUnSelectedDrawable = drawable;
        convertDrawablesToBitmaps();
        invalidate();
    }

    // Click listener interface
    public interface OnIndicatorClickListener {
        void onClick(int indicatorNumber);
    }

}
