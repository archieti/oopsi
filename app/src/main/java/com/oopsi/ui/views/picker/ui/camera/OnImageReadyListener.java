package com.oopsi.ui.views.picker.ui.camera;


import com.oopsi.ui.views.picker.model.Image;

import java.util.List;

public interface OnImageReadyListener {
    void onImageReady(List<Image> images);
}
