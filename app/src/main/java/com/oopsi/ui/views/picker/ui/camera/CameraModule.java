package com.oopsi.ui.views.picker.ui.camera;

import android.content.Context;
import android.content.Intent;

import com.oopsi.ui.views.picker.model.Config;


/**
 * Created by hoanglam on 8/18/17.
 */

public interface CameraModule {
    Intent getCameraIntent(Context context, Config config);

    void getImage(Context context, Intent intent, OnImageReadyListener imageReadyListener);
}
