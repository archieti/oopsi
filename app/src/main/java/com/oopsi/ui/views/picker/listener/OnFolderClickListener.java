package com.oopsi.ui.views.picker.listener;


import com.oopsi.ui.views.picker.model.Folder;

/**
 * Created by boss1088 on 8/23/16.
 */
public interface OnFolderClickListener {
    void onFolderClick(Folder folder);
}
