package com.oopsi.ui.views.picker.listener;


import com.oopsi.ui.views.picker.model.Folder;
import com.oopsi.ui.views.picker.model.Image;

import java.util.List;

/**
 * Created by hoanglam on 8/17/17.
 */

public interface OnImageLoaderListener {
    void onImageLoaded(List<Image> images, List<Folder> folders);

    void onFailed(Throwable throwable);
}
