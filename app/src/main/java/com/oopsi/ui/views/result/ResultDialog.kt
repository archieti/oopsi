package com.oopsi.ui.views.result

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDialog
import android.view.View
import android.view.Window
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import kotlinx.android.synthetic.main.dialog_result.*

@Suppress("MemberVisibilityCanBePrivate")
class ResultDialog(context: Context, theme: Int) : AppCompatDialog(context, theme) {
    lateinit var builder: Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_result)
        setActionColor(builder.color)
        setIcon(builder.drawable)
        title(builder.title)
        message(builder.message)
        actionText(builder.actionText)
        alternativeActionText(builder.alternativeActionText)
        setResultImageUrl(builder.resultImageUrl)
        setResultDeviceModel(builder.resultDeviceModel)
        setResultDeviceBrand(builder.resultDeviceBrand)
        builder.callback?.let { action(action, it) }
        builder.alternativeCallback?.let { action(alternativeAction, it) }
    }

    fun setActionColor(@ColorRes errorColor: Int) {
        action.setTextColor(ContextCompat.getColor(context, errorColor))
    }

    fun setIcon(icon: Int) {
        iconView.setImageResource(icon)
    }

    fun setResultImageUrl(url: String?) {
        if (url != null) {
            deviceImage.visibility = View.VISIBLE
            Glide.with(deviceImage)
                    .load(url)
                    .apply(RequestOptions().centerCrop())
                    .into(deviceImage)
        }
    }

    fun setResultDeviceModel(model: String?) {
        if (model != null) {
            deviceModel.visibility = View.VISIBLE
            deviceModel.text = model
        }
    }

    fun setResultDeviceBrand(brand: String?) {
        if (brand != null) {
            deviceBrand.visibility = View.VISIBLE
            deviceBrand.text = brand
        }
    }

    fun title(@StringRes text: Int) {
        setTextView(text, title)
    }

    fun message(@StringRes text: Int) {
        message.setText(text)
    }

    fun actionText(@StringRes text: Int) {
        action.setText(text)
    }

    fun alternativeActionText(@StringRes text: Int) {
        setTextView(text, alternativeAction)
    }

    fun action(action: TextView, listener: DialogInterface.OnClickListener) {
        action.setOnClickListener {
            listener.onClick(this, 0)
        }
    }


    private fun setTextView(text: Int, textView: TextView) {
        when (text) {
            0 -> View.GONE
            else -> {
                textView.setText(text)
                View.VISIBLE
            }
        }.let { textView.visibility = it }
    }
}


class Builder(context: Context) {

    var drawable: Int = 0
    var resultImageUrl: String? = null
    var resultDeviceModel: String? = null
    var resultDeviceBrand: String? = null
    var color: Int = 0
    var title: Int = 0
    var message: Int = 0
    var actionText: Int = 0
    var alternativeActionText: Int = 0
    var callback: DialogInterface.OnClickListener? = null
    var alternativeCallback: DialogInterface.OnClickListener? = null
    private val resultDialog: ResultDialog = ResultDialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen)

    fun icon(@DrawableRes drawable: Int): Builder {
        this.drawable = drawable
        return this
    }

    fun resultImageUrl(resultImageUrl: String): Builder {
        this.resultImageUrl = resultImageUrl
        return this
    }

    fun resultDeviceModel(resultDeviceModel: String): Builder {
        this.resultDeviceModel = resultDeviceModel
        return this
    }

    fun resultDeviceBrand(resultDeviceBrand: String): Builder {
        this.resultDeviceBrand = resultDeviceBrand
        return this
    }

    fun color(@ColorRes color: Int): Builder {
        this.color = color
        return this
    }

    fun title(@StringRes text: Int): Builder {
        title = text
        return this
    }

    fun message(@StringRes text: Int): Builder {
        message = text
        return this
    }

    fun actionText(@StringRes text: Int): Builder {
        actionText = text
        return this
    }

    fun alternativeActionText(@StringRes text: Int): Builder {
        alternativeActionText = text
        return this
    }

    fun action(callback: DialogInterface.OnClickListener): Builder {
        this.callback = callback
        return this
    }

    fun alternativeAction(callback: DialogInterface.OnClickListener): Builder {
        this.alternativeCallback = callback
        return this
    }

    fun create(): ResultDialog {
        resultDialog.builder = this
        return resultDialog
    }
}


class Success constructor(context: Context) : Result(context) {

    override fun builder() = builder
            .color(android.R.color.white)
            .icon(R.drawable.ic_success_small)
}


class Fail(context: Context) : Result(context) {

    override fun builder() = builder
            .color(android.R.color.white)
            .icon(R.drawable.ic_error_small)
}


sealed class Result(context: Context) {

    protected val builder = Builder(context)

    abstract fun builder(): Builder
}
