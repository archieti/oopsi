package com.oopsi.ui.views.progress

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatDialog
import android.view.Window

import com.oopsi.R

class ProgressDialog : AppCompatDialog {
    constructor(context: Context) : super(context) {
        setup()
    }

    constructor(context: Context, theme: Int) : super(context, theme) {
        setup()
    }

    private fun setup() {
        window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_progress)
    }
}
