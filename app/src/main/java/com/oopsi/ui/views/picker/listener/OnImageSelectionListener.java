package com.oopsi.ui.views.picker.listener;


import com.oopsi.ui.views.picker.model.Image;

import java.util.List;

/**
 * Created by hoanglam on 8/18/17.
 */

public interface OnImageSelectionListener {
    void onSelectionUpdate(List<Image> images);
}
