package com.oopsi.ui.onboarding.slide

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oopsi.R
import kotlinx.android.synthetic.main.fragment_slide.*

class SlideFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_slide, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        image.setImageResource(arguments!!.getInt(IMAGE_RES_TAG))
        thesis.text = arguments!!.getString(THESIS_TAG)
        description.text = arguments!!.getString(DESCRIPTION_TAG)
    }

    companion object {
        val TAG = "SlideFragment"
        val IMAGE_RES_TAG = "IMAGE_RES_TAG"
        val THESIS_TAG = "THESIS_TAG"
        val DESCRIPTION_TAG = "DESCRIPTION_TAG"

        fun newInstance(imageRes: Int, thesis: String, description: String): SlideFragment {
            val fragment = SlideFragment()
            val args = Bundle()
            args.putInt(IMAGE_RES_TAG, imageRes)
            args.putString(THESIS_TAG, thesis)
            args.putString(DESCRIPTION_TAG, description)
            fragment.arguments = args
            return fragment
        }
    }
}
