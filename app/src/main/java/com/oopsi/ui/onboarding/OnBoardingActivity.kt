package com.oopsi.ui.onboarding

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.ui.onboarding.slide.SlideFragment
import kotlinx.android.synthetic.main.activity_on_boarding.*


class OnBoardingActivity : BaseActivity() {

    override val layoutId = R.layout.activity_on_boarding

    lateinit var adapter: ViewPagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        backButton.setOnClickListener {
            if (slidesViewPager.currentItem != 0)
                slidesViewPager.currentItem = slidesViewPager.currentItem - 1
        }
        nextButton.setOnClickListener {
            if (slidesViewPager.currentItem != adapter.count - 1) {
                slidesViewPager.currentItem = slidesViewPager.currentItem + 1
            } else {
                startActivity(MainActivity.getIntent(this@OnBoardingActivity))
                finish()
            }
        }
        skipButton.setOnClickListener {
            startActivity(MainActivity.getIntent(this@OnBoardingActivity))
            finish()
        }
        slidesViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                val FIRST_POSITION = 0
                val LAST_POSITION = adapter.count - 1
                when (position) {
                    FIRST_POSITION -> {
                        backButton.visibility = View.GONE
                        nextButton.setText(R.string.button_continue)
                        skipButton.visibility = View.VISIBLE
                    }
                    LAST_POSITION -> {
                        backButton.visibility = View.VISIBLE
                        nextButton.setText(R.string.onboarding_get_started)
                        skipButton.visibility = View.GONE
                    }
                    else -> {
                        backButton.visibility = View.VISIBLE
                        nextButton.setText(R.string.button_continue)
                        skipButton.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun initViews() {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(SlideFragment.newInstance(R.drawable.first_slide,
                getString(R.string.onboarding_1_title),
                getString(R.string.onboarding_1_body)))
        adapter.addFragment(SlideFragment.newInstance(R.drawable.second_slide,
                getString(R.string.onboarding_2_title),
                getString(R.string.onboarding_2_body)))
        adapter.addFragment(SlideFragment.newInstance(R.drawable.third_slide,
                getString(R.string.onboarding_3_title),
                getString(R.string.onboarding_3_body)))
        slidesViewPager.adapter = adapter
        indicator.apply {
            setViewPager(slidesViewPager)
            setSmoothTransition(true)
            setDiscreteIndicator(true)
            setIndicatorsClickChangePage(false)
        }
    }
}
