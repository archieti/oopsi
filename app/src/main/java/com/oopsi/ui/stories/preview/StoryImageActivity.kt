package com.oopsi.ui.stories.preview


import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_story_image_preview.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

class StoryImageActivity : BaseActivity(), StoryImageView {


    @Inject
    @InjectPresenter
    lateinit var presenter: StoryImagePresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_story_image_preview

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar.setNavigationOnClickListener { finish() }
        progress.visibility = View.VISIBLE
        val url = intent.getStringExtra(IMAGE_URL_KEY)
        Glide.with(this)
                .load(url)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                })
                .into(image)
    }


    companion object {

        fun getIntent(context: Context, url: String) = context.intentFor<StoryImageActivity>(Pair(IMAGE_URL_KEY, url))
        fun key(): String = StoryImageActivity::class.java.simpleName

        const val IMAGE_URL_KEY = "image_url"
    }
}