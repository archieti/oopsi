package com.oopsi.ui.stories

import com.arellomobile.mvp.InjectViewState
import com.artjoker.events.AppEvents
import com.artjoker.interactors.StoriesUseCase
import com.artjoker.models.Story
import com.artjoker.repositories.StoriesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@InjectViewState
class StoriesPresenter @Inject constructor(
        private val storiesUseCase: StoriesUseCase,
        private val storiesRepository: StoriesRepository
) : BaseMvpPresenter<StoriesView>(), AppEvents.Listener {

    private var stories: List<Story> = listOf()

    fun retrieveStories() = storiesUseCase.execute(
            object : DisposableSingleObserver<List<Story>>() {
                override fun onSuccess(stories: List<Story>) {
                    viewState.showStories(stories)
                    this@StoriesPresenter.stories = stories
                }

                override fun onError(e: Throwable) {
                    viewState.showStories(listOf())
                    e.printStackTrace()
                }
            })


    fun setStory(it: Story) {
        storiesRepository.selectedStory = it
    }

    override fun onCommentsCountChanged(story: Story, commentsCount: Int) =
            stories.indexOf(story)
                    .let { viewState.onCommentsCountChangedAt(it, commentsCount) }


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        events.addListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        events.removeListener(this)
    }
}
