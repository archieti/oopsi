package com.oopsi.ui.stories.details

import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.CommentsUseCase
import com.artjoker.interactors.StoryDetailsUseCase
import com.artjoker.models.Comment
import com.artjoker.models.Story
import com.artjoker.repositories.StoriesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.comments.reply.ReplyCommentActivity
import javax.inject.Inject

@InjectViewState
class StoryDetailsPresenter @Inject constructor(
        private val storyDetailsUseCase: StoryDetailsUseCase,
        private val commentsUseCase: CommentsUseCase,
        private val storiesRepository: StoriesRepository
) : BaseMvpPresenter<StoryDetailsView>() {

    fun retrieveStoryDetails() = storyDetailsUseCase.execute(
            object : PresenterSingleObserver<Story>() {

                override fun onSuccess(story: Story) {
                    viewState.showStoryDetails(story)
                    retrieveComments(story)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    viewState.showError()
                }
            })


    private fun retrieveComments(story: Story) = commentsUseCase.execute(
            object : PresenterSingleObserver<List<Comment>>() {
                override fun onSuccess(comments: List<Comment>) {
                    viewState.showComments(comments)
                    events.notifyListeners { it.onCommentsCountChanged(story, comments.size) }
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    viewState.showError()
                }

            })

    override fun onNewComment(story: Story, comments: List<Comment>) = comments.let {
        viewState.showComments(it)
        // viewState.scrollToBottom()
        events.notifyListeners { it.onCommentsCountChanged(story, comments.size) }
    }

    fun reply(comment: Comment) {
        storiesRepository.reply = comment
        router.navigateTo(ReplyCommentActivity.key())
    }
}
