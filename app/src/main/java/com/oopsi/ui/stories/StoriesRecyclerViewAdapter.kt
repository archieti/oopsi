package com.oopsi.ui.stories

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.artjoker.models.Story
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import org.jetbrains.anko.find

class StoriesRecyclerViewAdapter(data: MutableList<Story>) : AbstractRecyclerViewAdapter<Story>(data) {
    lateinit var context: Context

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_recycler_story, parent, false)
        return EntityViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myOptions = RequestOptions()
                .centerCrop()
        Glide.with(context)
                .load(data[position].backgroundImageUrl)
                .apply(myOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into((holder as EntityViewHolder).image)
        holder.title.text = data[position].title
        holder.comments.text = context.resources.getQuantityString(R.plurals.comments_plurals, data[position].commentsCount,
                data[position].commentsCount)
    }

    inner class EntityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rootView: CardView = itemView.find(R.id.rootView)
        var image: ImageView = itemView.find(R.id.image)
        var title: TextView = itemView.find(R.id.title)
        var comments: TextView = itemView.find(R.id.comments)

        init {
            rootView.setOnClickListener { itemClickBus.send(data[adapterPosition]) }
        }
    }
}