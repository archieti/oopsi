package com.oopsi.ui.stories.details

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Comment
import com.artjoker.models.Story

interface StoryDetailsView : MvpView {
    fun showStoryDetails(story: Story)
    fun showComments(comments: List<Comment>)
    fun showError()
    fun scrollToBottom()
}
