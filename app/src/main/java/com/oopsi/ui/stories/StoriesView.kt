package com.oopsi.ui.stories

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Story

interface StoriesView : MvpView {
    fun showStories(stories: List<Story>)
    fun onCommentsCountChangedAt(position: Int, commentsCount: Int)
}
