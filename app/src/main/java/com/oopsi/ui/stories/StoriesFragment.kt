package com.oopsi.ui.stories

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Story
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.base.BaseFragment
import com.oopsi.ui.services.MarginItemDecoration
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_stories.*
import org.jetbrains.anko.support.v4.dimen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class StoriesFragment : BaseFragment(), StoriesView {
    override val layoutId: Int = R.layout.fragment_stories

    @Inject
    @InjectPresenter
    lateinit var mStoriesPresenter: StoriesPresenter

    @Inject lateinit var router: Router

    @ProvidePresenter
    fun provideStoriesPresenter() = mStoriesPresenter

    private lateinit var adapter: StoriesRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
        mStoriesPresenter.retrieveStories()
        progress.setVisible(true)
    }

    private fun initListeners() {
        adapter.itemClickBusObs
                .subscribe {
                    mStoriesPresenter.setStory(it)
                    router.navigateTo(Screens.STORY_DETAILS)
                }
    }

    private fun initViews() {
        adapter = StoriesRecyclerViewAdapter(mutableListOf())
        layoutManager = LinearLayoutManager(activity)
        storiesRecyclerView.addItemDecoration(MarginItemDecoration(dimen(R.dimen.margin_half), dimen(R.dimen.margin)))
        storiesRecyclerView.adapter = adapter
        storiesRecyclerView.layoutManager = layoutManager
    }

    override fun showStories(stories: List<Story>) {
        progress.setVisible(false)
        storiesRecyclerView.setVisible(stories.isNotEmpty())
        adapter.data = stories.toMutableList()
        adapter.notifyDataSetChanged()
        emptyState.setVisible(stories.isEmpty())
    }

    override fun onCommentsCountChangedAt(position: Int, commentsCount: Int) {
        adapter.data[position].commentsCount = commentsCount
        adapter.notifyItemChanged(position)
    }
}
