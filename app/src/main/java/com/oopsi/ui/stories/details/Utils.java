package com.oopsi.ui.stories.details;

import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;

public class Utils {

    @SuppressWarnings("deprecation")
    public static Spannable fromHtml(String html, UrlImageParser imageParser) {
        if (html == null) return null;
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY, imageParser, null);
        } else {
            result = Html.fromHtml(html, imageParser, null);
        }
        return (Spannable) result;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (html == null) return null;
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
