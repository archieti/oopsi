package com.oopsi.ui.stories.preview

import com.arellomobile.mvp.InjectViewState
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class StoryImagePresenter @Inject constructor() : BaseMvpPresenter<StoryImageView>()

