package com.oopsi.ui.stories.details

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Spannable
import android.text.method.LinkMovementMethod
import android.text.style.ImageSpan
import android.text.style.URLSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Comment
import com.artjoker.models.Story
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.comments.CommentsRecyclerViewAdapter
import com.oopsi.ui.comments.add.AddCommentActivity
import com.oopsi.ui.comments.reply.ReplyCommentActivity
import com.oopsi.ui.stories.preview.StoryImageActivity
import kotlinx.android.synthetic.main.activity_story_details.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject


class StoryDetailsActivity : BaseActivity(), StoryDetailsView, Callback {

    @Inject
    @InjectPresenter
    lateinit var mStoryDetailsPresenter: StoryDetailsPresenter

    private lateinit var adapter: CommentsRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager

    @ProvidePresenter
    fun provideStoryDetailsPresenter() = mStoryDetailsPresenter

    override val layoutId: Int = R.layout.activity_story_details

    override val navigator: Navigator = object : BaseNavigator() {
        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) = when (screenKey) {
            AddCommentActivity.key() -> AddCommentActivity.getIntent(context)
            ReplyCommentActivity.key() -> ReplyCommentActivity.getIntent(context)
            StoryImageActivity.key() -> StoryImageActivity.getIntent(context, data as String)
            else -> super.createActivityIntent(context, screenKey, data)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
        mStoryDetailsPresenter.retrieveStoryDetails()
    }


    private fun initListeners() {
        toolbar.setNavigationOnClickListener { finish() }
        addComment.setOnClickListener { router.navigateTo(AddCommentActivity.key()) }
    }


    override fun showStoryDetails(story: Story) {

        bindStory(story.storyHtmlText)

        adapter = CommentsRecyclerViewAdapter()
        layoutManager = LinearLayoutManager(this)
        commentsRecyclerView.adapter = adapter
        commentsRecyclerView.layoutManager = layoutManager
        commentsRecyclerView.isNestedScrollingEnabled = false
        adapter.showCommentsProgress()
        collapsing_toolbar.title = story.title
        val myOptions = RequestOptions()
                .centerCrop()
        Glide.with(this)
                .load(story.backgroundImageUrl)
                .apply(myOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(brandBackgroundImage)

        adapter.replyEvent
                .toObservable()
                .subscribe { mStoryDetailsPresenter.reply(it) }
                .also { manage(it) }

        adapter.mentionEvent
                .toObservable()
                .filter { it != -1 }
                .subscribe { commentsRecyclerView.smoothScrollToPosition(it) }
                .also { manage(it) }
    }

    override fun showComments(comments: List<Comment>) {
        adapter.hideCommentsProgress()
        adapter.data = comments.asReversed()
        adapter.notifyDataSetChanged()
    }

    override fun scrollToBottom() = Unit//commentsRecyclerView.smoothScrollToPosition(adapter.itemCount)


    override fun showError() {
        adapter.hideCommentsProgress()
        Toast.makeText(this, R.string.default_error_message, Toast.LENGTH_SHORT).show()
    }

    override fun onImageClick(imageUrl: String) {
        Log.e("onImageClick", imageUrl)
        router.navigateTo(StoryImageActivity.key(), imageUrl)
    }

    private fun bindStory(storyHtmlText: String) {
        val urlImageParser = UrlImageParser(this, articleBodyTextView)
        val fromHtml = Utils.fromHtml(storyHtmlText, urlImageParser)
        applyClick(fromHtml, this)
        articleBodyTextView.text = fromHtml
        articleBodyTextView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun applyClick(spannable: Spannable, callback: Callback) {
        val spans = spannable.getSpans(0, spannable.length, ImageSpan::class.java)
        spans.forEach {
            val flags = spannable.getSpanFlags(it)
            val start = spannable.getSpanStart(it)
            val end = spannable.getSpanEnd(it)

            spannable.setSpan(object : URLSpan(it.source) {
                override fun onClick(v: View) {
                    callback.onImageClick(url)
                }
            }, start, end, flags)
        }
    }
}


interface Callback {
    fun onImageClick(imageUrl: String)
}
