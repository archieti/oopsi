package com.oopsi.ui.comments.add

import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.AddCommentUseCase
import com.artjoker.interactors.CommentsUseCase
import com.artjoker.models.Comment
import com.artjoker.repositories.StoriesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class AddCommentPresenter @Inject constructor(
        private val addComment: AddCommentUseCase,
        private val storiesRepository: StoriesRepository,
        private val getComments: CommentsUseCase
) : BaseMvpPresenter<AddCommentView>() {

    fun addComment(name: String, text: String) = addComment.execute(
            Comment(
                    authorName = name,
                    text = text,
                    storyId = storiesRepository.selectedStory.id
            ),
            object : PresenterCompletableObserver() {
                override fun onStart() {
                    super.onStart()
                    viewState.showProgress()
                }

                override fun onComplete() = updateComments()

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    viewState.hideProgress()
                    viewState.showError()
                }
            }
    )

    private fun updateComments() {
        getComments.execute(object : PresenterSingleObserver<List<Comment>>() {
            override fun onSuccess(comments: List<Comment>) {
                viewState.hideProgress()
                events.notifyListeners { it.onNewComment(storiesRepository.selectedStory, comments) }
                router.exit()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                viewState.hideProgress()
                viewState.showError()
            }
        })
    }
}
