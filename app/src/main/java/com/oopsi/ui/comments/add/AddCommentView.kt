package com.oopsi.ui.comments.add

import com.arellomobile.mvp.MvpView

interface AddCommentView : MvpView {
    fun showError()
    fun hideProgress()
    fun showProgress()

}
