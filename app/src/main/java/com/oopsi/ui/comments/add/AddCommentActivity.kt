package com.oopsi.ui.comments.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding2.widget.RxTextView
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.activity_add_comment.*
import javax.inject.Inject

class AddCommentActivity : BaseActivity(), AddCommentView {


    @Inject
    @InjectPresenter
    lateinit var presenter: AddCommentPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_add_comment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        name.setOnFocusChangeListener { _, hasFocus ->
            nameLayout.hint = getString(R.string.top_name_hint).takeIf { hasFocus } ?: getString(R.string.input_comment_name_hint)
        }
        comment.setOnFocusChangeListener { _, hasFocus ->
            commentLayout.hint = getString(R.string.top_comment_hint).takeIf { hasFocus } ?: getString(R.string.input_comment_comment_hint)
        }
        Observable.combineLatest<String, String, Boolean>(
                RxTextView.afterTextChangeEvents(name).map { event -> event.editable()?.toString() },
                RxTextView.afterTextChangeEvents(comment).map { event -> event.editable()?.toString() },
                BiFunction { name, comment -> name.trim().isNotEmpty() && comment.trim().isNotEmpty() })
                .subscribe { addComment.isEnabled = it }

        close.setOnClickListener { router.exit() }

        addComment.setOnClickListener { presenter.addComment(name.text.toString(), comment.text.toString()) }
    }

    override fun showError() = Toast.makeText(
            this,
            getString(R.string.fail_comment),
            Toast.LENGTH_SHORT)
            .show()


    companion object {

        fun getIntent(context: Context) = Intent(context, AddCommentActivity::class.java)

        fun key(): String = AddCommentActivity::class.java.simpleName
    }
}