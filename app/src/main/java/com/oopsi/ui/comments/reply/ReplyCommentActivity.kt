package com.oopsi.ui.comments.reply

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Comment
import com.jakewharton.rxbinding2.widget.RxTextView
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.utils.*
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.activity_reply_comment.*
import java.util.*
import javax.inject.Inject

class ReplyCommentActivity : BaseActivity(), ReplyCommentView {


    @Inject
    @InjectPresenter
    lateinit var presenter: ReplyCommentPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_reply_comment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        name.setOnFocusChangeListener { _, hasFocus ->
            nameLayout.hint = getString(R.string.top_name_hint).takeIf { hasFocus } ?: getString(R.string.input_comment_name_hint)
        }

        Observable.combineLatest<String, String, Boolean>(
                RxTextView.afterTextChangeEvents(name).map { event -> event.editable()?.toString() },
                RxTextView.afterTextChangeEvents(comment).map { event -> event.editable()?.toString() },
                BiFunction { name, comment -> name.trim().isNotEmpty() && comment.trim().isNotEmpty() })
                .subscribe { reply.isEnabled = it }

        close.setOnClickListener { router.exit() }

        reply.setOnClickListener { presenter.reply(name.text.toString(), comment.text.toString()) }

    }

    override fun showError() = Toast.makeText(
            this,
            getString(R.string.fail_comment),
            Toast.LENGTH_SHORT)
            .show()


    @SuppressLint("SetTextI18n")
    override fun fillParentCommentUi(parent: Comment) {
        author.text = parent.authorName
        commentToReply.text = parent.text
        date.text = DateUtils.getReadableDate(parent.timestamp, this)
        authorAvatar.circleBackground(Random().color())
        nameSymbol.text = parent.authorName[0].toString().toUpperCase()

        comment.setOnFocusChangeListener { _, hasFocus ->
            commentLayout.hint = getString(R.string.top_comment_hint).takeIf { hasFocus }
            val nickName = parent.authorName.replace("\\s".toRegex(), "_")
            val toMentionSpan = "@$nickName".applySpanToMention(this)
            comment.setText("$toMentionSpan ")


        }
        comment.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                comment.removeTextChangedListener(this)
                val text = s?.applySpanToMentions(this@ReplyCommentActivity)
                comment.setSelection(text?.length ?: 0)
                comment.addTextChangedListener(this)
            }
        })
    }


    companion object {

        fun getIntent(context: Context) = Intent(context, ReplyCommentActivity::class.java)

        fun key(): String = ReplyCommentActivity::class.java.simpleName
    }
}