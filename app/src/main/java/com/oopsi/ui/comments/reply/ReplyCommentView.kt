package com.oopsi.ui.comments.reply

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Comment

interface ReplyCommentView : MvpView {
    fun showError()
    fun hideProgress()
    fun showProgress()
    fun fillParentCommentUi(parent: Comment)

}
