package com.oopsi.ui.comments.reply

import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.CommentsUseCase
import com.artjoker.interactors.ReplyCommentUseCase
import com.artjoker.models.Comment
import com.artjoker.repositories.StoriesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject

@InjectViewState
class ReplyCommentPresenter @Inject constructor(
        private val reply: ReplyCommentUseCase,
        private val storiesRepository: StoriesRepository,
        private val getComments: CommentsUseCase
) : BaseMvpPresenter<ReplyCommentView>() {

    override fun attachView(view: ReplyCommentView?) {
        super.attachView(view)
        viewState.fillParentCommentUi(storiesRepository.reply)
    }

    fun reply(name: String, text: String) {
        reply.execute(
                ReplyCommentUseCase.Params(
                        Comment(
                                authorName = name,
                                text = text,
                                storyId = storiesRepository.selectedStory.id
                        ),
                        storiesRepository.reply
                ),
                object : PresenterCompletableObserver() {
                    override fun onStart() {
                        super.onStart()
                        viewState.showProgress()
                    }

                    override fun onComplete() = updateComments()

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        viewState.hideProgress()
                        viewState.showError()
                    }
                }
        )
    }

    private fun updateComments() = getComments.execute(object : PresenterSingleObserver<List<Comment>>() {
        override fun onSuccess(comments: List<Comment>) {
            viewState.hideProgress()
            events.notifyListeners { it.onNewComment(storiesRepository.selectedStory, comments) }
            router.exit()
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
            viewState.hideProgress()
            viewState.showError()
        }
    })
}
