package com.oopsi.ui.comments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Comment
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.comments.CommentsRecyclerViewAdapter.ViewType.COMMENT
import com.oopsi.ui.comments.CommentsRecyclerViewAdapter.ViewType.COUNT
import com.oopsi.utils.*
import kotlinx.android.synthetic.main.item_recycler_comment.view.*
import kotlinx.android.synthetic.main.item_recycler_comments_count.view.*
import java.util.*

class CommentsRecyclerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var context: Context
    val replyEvent = RxBus<Comment>()
    val mentionEvent = RxBus<Int>()
    val itemClickBus = RxBus<Comment>()
    var data = listOf<Comment>()
    var progressState = false
    private var holderCount: CountViewHolder? = null

    override fun getItemCount(): Int = data.size + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (viewType) {
            COUNT.ordinal -> CountViewHolder(LayoutInflater.from(context).inflate(R.layout.item_recycler_comments_count, parent, false))
            else -> CommentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_recycler_comment, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            COMMENT.ordinal -> {
                val commentViewHolder = holder as CommentViewHolder
                commentViewHolder.bind(data[position - 1])
            }
            COUNT.ordinal -> {
                holderCount = holder as CountViewHolder
                holderCount?.bind()
            }
        }
    }

    override fun getItemViewType(position: Int) = when (position) {
        0 -> COUNT.ordinal
        else -> COMMENT.ordinal
    }

    fun hideCommentsProgress() = holderCount?.setProgressVisible(false).also {
        progressState = false
    }


    fun showCommentsProgress() = holderCount?.setProgressVisible(true).also {
        progressState = true
    }

    inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.rootView.setOnClickListener { itemClickBus.send(data[adapterPosition - 1]) }
            itemView.reply.setOnClickListener { replyEvent.send(data[adapterPosition - 1]) }
            itemView.comment.setOnClickListener {
                data[adapterPosition - 1].let {
                    if (!it.parentId.isNullOrEmpty()) {
                        mentionEvent.send(it.findParentPosition(data) + 1)
                    }
                }
            }
        }

        fun bind(comment: Comment) {
            itemView.author.text = comment.authorName
            itemView.comment.text = comment.text.applySpanToMentions(context)
            itemView.date.text = DateUtils.getReadableDate(comment.timestamp, itemView.context)
            itemView.authorAvatar.circleBackground(Random().color())
            itemView.nameSymbol.text = comment.authorName[0].toString().toUpperCase()
        }
    }


    inner class CountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            itemView.commentsCountView.text = String.format(context.getString(R.string.comment_template), data.size)
            setProgressVisible(progressState)
        }

        fun setProgressVisible(isVisible: Boolean) = itemView.progress.setVisible(isVisible)
    }

    enum class ViewType {
        COMMENT,
        COUNT
    }
}