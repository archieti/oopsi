package com.oopsi.ui.languages

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Language
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.item_recycler_langs.view.*


class LanguagesAdapter(var selectedLang: Language, data: MutableList<Language>) : AbstractRecyclerViewAdapter<Language>(data) {

    val selection = RxBus<Language>()


    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_langs, parent, false)
        return ViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                selectedLang = data[adapterPosition]
                selection.send(selectedLang)
                notifyDataSetChanged()
            }
        }

        fun bind(lang: Language) {
            itemView.langTitle.text = lang.title
            if (selectedLang.locale.displayName.isNullOrEmpty() && adapterPosition == 0) {
                itemView.selection.setVisible(true)
            } else
                itemView.selection.setVisible(lang.locale.displayName == selectedLang.locale.displayName)
        }
    }
}