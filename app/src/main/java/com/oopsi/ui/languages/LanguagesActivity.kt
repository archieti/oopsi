package com.oopsi.ui.languages


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Language
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_languages.*
import javax.inject.Inject

class LanguagesActivity : BaseActivity(), LanguagesView {


    @Inject
    @InjectPresenter
    lateinit var presenter: LanguagesPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_languages

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { router.exit() }
        presenter.initLanguages(resources.getStringArray(R.array.supported_languages))
    }

    override fun fillSupportedLocales(current: Language, mutableListOf: MutableList<Language>) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        val languagesAdapter = LanguagesAdapter(current, mutableListOf)
        recyclerView.adapter = languagesAdapter
        languagesAdapter.selection.toObservable()
                .subscribe { presenter.onLanguageSelected(it) }
                .let { manage(it) }
    }


    companion object {
        fun getIntent(context: Context) = Intent(context, LanguagesActivity::class.java)
        fun key(): String = LanguagesActivity::class.java.simpleName
    }
}