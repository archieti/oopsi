package com.oopsi.ui.languages

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Language

interface LanguagesView : MvpView {
    fun fillSupportedLocales(current: Language, mutableListOf: MutableList<Language>)
}
