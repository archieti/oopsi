package com.oopsi.ui.languages

import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.Language
import com.artjoker.repositories.LangStorage
import com.oopsi.ui.base.BaseMvpPresenter
import java.util.*
import javax.inject.Inject


@InjectViewState
class LanguagesPresenter @Inject constructor(private val langStorage: LangStorage) : BaseMvpPresenter<LanguagesView>() {

    fun initLanguages(languages: Array<String>) {
        val languagesList = mutableListOf(
                Language(languages[0], Locale("en", "GB")),
                Language(languages[1], Locale("fr", "FR")),
                Language(languages[2], Locale("lv", "LV")),
                Language(languages[3], Locale("ru", "RU"))
        )

        viewState.fillSupportedLocales(langStorage.language, languagesList)
    }

    fun onLanguageSelected(language: Language) {
        langStorage.language = language
        events.notifyListeners { it.onLocaleChanges() }
    }
}
