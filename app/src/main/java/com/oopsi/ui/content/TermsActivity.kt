package com.oopsi.ui.content


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_privacy_policy.*

class TermsActivity : BaseActivity() {

    override val layoutId: Int = R.layout.activity_terms

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { finish() }
        text.movementMethod = ScrollingMovementMethod()
    }


    companion object {
        fun getIntent(context: Context) = Intent(context, TermsActivity::class.java)
        fun key(): String = TermsActivity::class.java.simpleName
    }
}
