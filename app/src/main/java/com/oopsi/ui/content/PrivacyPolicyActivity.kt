package com.oopsi.ui.content


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_privacy_policy.*


class PrivacyPolicyActivity : BaseActivity() {

    override val layoutId: Int = R.layout.activity_privacy_policy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { finish() }
        text.movementMethod = ScrollingMovementMethod()
    }


    companion object {
        fun getIntent(context: Context) = Intent(context, PrivacyPolicyActivity::class.java)
        fun key(): String = PrivacyPolicyActivity::class.java.simpleName
    }
}
