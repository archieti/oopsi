package com.oopsi.ui.settings


import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.*
import javax.inject.Inject

class SettingsActivity : BaseActivity(), SettingsView {

    @Inject
    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { router.exit() }
    }

    override fun showDataAllowed(isAllowed: Boolean) {
        switchNetworkType.setOnCheckedChangeListener(null)
        switchNetworkType.isChecked = isAllowed
        switchNetworkType.setOnCheckedChangeListener { _, isChecked -> presenter.setDataAllowed(isChecked) }
    }


    companion object {

        fun getIntent(context: Context) = Intent(context, SettingsActivity::class.java)

        fun key(): String = SettingsActivity::class.java.simpleName
    }
}