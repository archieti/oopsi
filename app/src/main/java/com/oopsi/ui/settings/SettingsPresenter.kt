package com.oopsi.ui.settings

import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.Settings.NetworkState.ALL
import com.artjoker.models.Settings.NetworkState.WIFI_ONLY
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class SettingsPresenter @Inject constructor() : BaseMvpPresenter<SettingsView>() {

    override fun attachView(view: SettingsView?) {
        super.attachView(view)
        storage.settings.networkState
                .let { viewState.showDataAllowed(it == ALL) }
    }

    fun setDataAllowed(checked: Boolean) {
        val networkState = ALL.takeIf { checked } ?: WIFI_ONLY
        storage.settings = storage.settings.copy(networkState = networkState)
    }
}
