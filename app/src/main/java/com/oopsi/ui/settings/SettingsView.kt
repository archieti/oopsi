package com.oopsi.ui.settings

import com.arellomobile.mvp.MvpView

interface SettingsView : MvpView {
    fun showDataAllowed(isAllowed: Boolean)
}
