package com.oopsi.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.View
import com.artjoker.models.Brand
import com.artjoker.models.BrandFilter
import com.artjoker.models.Filter
import com.artjoker.repositories.LocationRepository
import com.artjoker.repositories.ServicesRepository
import com.oopsi.R
import com.oopsi.navigation.Screens
import com.oopsi.ui.about.AboutOnBoardingActivity
import com.oopsi.ui.base.BaseDrawerActivity
import com.oopsi.ui.content.PrivacyPolicyActivity
import com.oopsi.ui.content.TermsActivity
import com.oopsi.ui.devices.favourites.DevicesFragment
import com.oopsi.ui.devices.history.DeviceHistoryActivity
import com.oopsi.ui.filters.FiltersActivity
import com.oopsi.ui.help.HelpActivity
import com.oopsi.ui.languages.LanguagesActivity
import com.oopsi.ui.scanner.camera.ScannerCameraActivity
import com.oopsi.ui.services.all.ServicesFragment
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.ui.settings.SettingsActivity
import com.oopsi.ui.stories.StoriesFragment
import com.oopsi.ui.stories.details.StoryDetailsActivity
import com.oopsi.utils.LocationHelper
import com.oopsi.utils.setVisible
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_main_activity_content.*
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class MainActivity : BaseDrawerActivity() { //TODO convert to MVP pattern


    override fun menu(): View = menu
    override fun content(): View = contentView
    override fun drawerLayout(): DrawerLayout = drawerLayout
    override fun navigationView(): NavigationView = navigationView
    override val layoutId: Int = R.layout.activity_main

    @Inject
    lateinit var locationRepository: LocationRepository

    @Inject
    lateinit var servicesRepository: ServicesRepository

    var selectedBrand: Brand? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val adapter = ViewPagerAdapter(supportFragmentManager).apply {
            addFragment(StoriesFragment(), getString(R.string.stories_tab))
            addFragment(ServicesFragment(), getString(R.string.services_tab))
            addFragment(DevicesFragment(), getString(R.string.devices_tab))
        }
        pager.adapter = adapter
        tabs.setupWithViewPager(pager)
        onPageScrolled()
        RxPermissions(this)
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .filter { it }
                .subscribe { locationHelper.tryRequestLocation() }

        scannerButton.setOnClickListener { router.navigateTo(ScannerCameraActivity.key()) }
        search.setOnClickListener { router.navigateTo(Screens.SEARCH_BRAND, BRAND_RESULT_CODE) }
        filters.setOnClickListener { router.navigateTo(FiltersActivity.key()) }
    }

    override fun onResume() {
        super.onResume()

        router.setResultListener(BRAND_RESULT_CODE) { resultData ->
            showServices(resultData as Brand)
        }
        selectedBrand?.let {
            router.navigateTo(FilteredServicesActivity.key())
            selectedBrand = null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationHelper.handleActivityResult(requestCode, resultCode)
    }

    override fun onDestroy() {
        locationHelper.cancelLocationUpdates()
        super.onDestroy()
    }

    private fun showServices(brand: Brand) {
        servicesRepository.filter = Filter(brand = BrandFilter(brand)) // global state
        selectedBrand = brand // local state
    }

    private fun onPageScrolled() {

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                search.setVisible(position == 1)
                filters.setVisible(position == 1)
            }
        })
    }


    private val locationHelper by lazy {
        LocationHelper(this, object : LocationHelper.Listener {
            override fun onLocationRequested() {
            }

            override fun onLocationDetected(location: Location) {
                locationRepository.putLocation(com.artjoker.models.Location(location.latitude, location.longitude))
            }

            override fun onLocatingFail() {
                Log.e("location", "location failed")
            }
        })
    }

    override val navigator: Navigator = object : BaseNavigator() {
        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    ScannerCameraActivity.key() -> ScannerCameraActivity.getIntent(this@MainActivity)
                    Screens.STORY_DETAILS -> intentFor<StoryDetailsActivity>()
                    DeviceHistoryActivity.key() -> intentFor<DeviceHistoryActivity>()
                    PrivacyPolicyActivity.key() -> PrivacyPolicyActivity.getIntent(this@MainActivity)
                    TermsActivity.key() -> TermsActivity.getIntent(this@MainActivity)
                    SettingsActivity.key() -> SettingsActivity.getIntent(context)
                    LanguagesActivity.key() -> LanguagesActivity.getIntent(context)
                    AboutOnBoardingActivity.key() -> AboutOnBoardingActivity.getIntent(context)
                    HelpActivity.key() -> HelpActivity.getIntent(context)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        private const val BRAND_RESULT_CODE = 124
        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
        fun key(): String = MainActivity::class.java.simpleName
    }
}
