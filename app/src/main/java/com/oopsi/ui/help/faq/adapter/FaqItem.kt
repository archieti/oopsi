package com.oopsi.ui.help.faq.adapter

import android.os.Parcel
import android.os.Parcelable
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class FaqItem constructor(question: Question, answer: Answer) : ExpandableGroup<Answer>(question.string, listOf(answer))

data class Question(val string: String) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) = parcel.writeString(string)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Question> {
        override fun createFromParcel(parcel: Parcel) = Question(parcel)

        override fun newArray(size: Int): Array<Question?> = arrayOfNulls(size)

    }
}

data class Answer(val string: String) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) = parcel.writeString(string)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Answer> {
        override fun createFromParcel(parcel: Parcel): Answer = Answer(parcel)

        override fun newArray(size: Int): Array<Answer?> = arrayOfNulls(size)
    }

}