package com.oopsi.ui.help.faq

import com.arellomobile.mvp.MvpView
import com.oopsi.ui.help.faq.adapter.FaqItem

interface FaqView : MvpView {
    fun faqData(title: String, questions: MutableList<FaqItem>)
}
