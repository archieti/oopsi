package com.oopsi.ui.help.faq.adapter

import android.view.View
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kotlinx.android.synthetic.main.item_recycler_answer.view.*

class AnswerViewHolder(itemView: View) : ChildViewHolder(itemView) {
    fun bind(answer: Answer?) {
        itemView.answer.text = answer?.string
    }
}