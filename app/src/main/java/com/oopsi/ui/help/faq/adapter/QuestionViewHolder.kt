package com.oopsi.ui.help.faq.adapter

import android.view.View
import com.oopsi.R
import com.oopsi.utils.setVisible
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.item_recycler_question.view.*

class QuestionViewHolder(itemView: View) : GroupViewHolder(itemView) {
    fun bind(faqItem: FaqItem) {
        itemView.question.text = faqItem.title
    }

    override fun expand() {
        super.expand()
        itemView.line.setVisible(false)
        itemView.actionIcon.setImageResource(R.drawable.ic_collapse_icon)
    }

    override fun collapse() {
        super.collapse()
        itemView.line.setVisible(true)
        itemView.actionIcon.setImageResource(R.drawable.ic_expand_icon)
    }
}