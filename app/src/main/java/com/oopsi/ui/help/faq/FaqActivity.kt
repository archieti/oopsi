package com.oopsi.ui.help.faq


import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.help.faq.adapter.FaqAdapter
import com.oopsi.ui.help.faq.adapter.FaqItem
import kotlinx.android.synthetic.main.activity_faq.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

class FaqActivity : BaseActivity(), FaqView {

    @Inject
    @InjectPresenter
    lateinit var presenter: FaqPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_faq

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { router.exit() }
        presenter.generateData(intent.getIntExtra(type(), 0), this)
    }

    override fun faqData(title: String, questions: MutableList<FaqItem>) {
        titleHeader.text = title
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = FaqAdapter(questions)
    }


    companion object {
        fun getIntent(context: Context, type: Int) = context.intentFor<FaqActivity>(Pair(type(), type))
        fun key(): String = FaqActivity::class.java.simpleName
        fun type() = "type"
    }
}