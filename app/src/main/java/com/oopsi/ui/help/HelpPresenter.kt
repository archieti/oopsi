package com.oopsi.ui.help

import com.arellomobile.mvp.InjectViewState
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class HelpPresenter @Inject constructor() : BaseMvpPresenter<HelpView>()
