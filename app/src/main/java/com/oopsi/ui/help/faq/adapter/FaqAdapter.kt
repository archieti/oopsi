package com.oopsi.ui.help.faq.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.oopsi.R
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class FaqAdapter(items: List<FaqItem>) : ExpandableRecyclerViewAdapter<QuestionViewHolder, AnswerViewHolder>(items) {

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int) =
            QuestionViewHolder(LayoutInflater.from(parent?.context)
                    .inflate(R.layout.item_recycler_question, parent, false))

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int) =
            AnswerViewHolder(LayoutInflater.from(parent?.context)
                    .inflate(R.layout.item_recycler_answer, parent, false))


    override fun onBindGroupViewHolder(holder: QuestionViewHolder, flatPosition: Int, group: ExpandableGroup<*>?) =
            holder.bind(group as FaqItem)

    override fun onBindChildViewHolder(holder: AnswerViewHolder, flatPosition: Int, group: ExpandableGroup<*>?, childIndex: Int) =
            holder.bind((group as FaqItem).items[childIndex])
}
