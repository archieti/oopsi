package com.oopsi.ui.help.faq

import android.content.Context
import android.support.annotation.ArrayRes
import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.SAVED_DEVICES
import com.artjoker.models.SCANNER
import com.artjoker.models.SERVICES
import com.artjoker.models.STORIES
import com.oopsi.R
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.help.faq.adapter.Answer
import com.oopsi.ui.help.faq.adapter.FaqItem
import com.oopsi.ui.help.faq.adapter.Question
import com.oopsi.utils.getStringArray
import javax.inject.Inject


@InjectViewState
class FaqPresenter @Inject constructor() : BaseMvpPresenter<FaqView>() {

    fun generateData(type: Int, context: Context) = when (type) {

        STORIES -> viewState.faqData(context.getString(R.string.help_stories),
                context.mapFaqCategory(R.array.stories_questions, R.array.stories_answers))

        SCANNER -> viewState.faqData(context.getString(R.string.help_scanner),
                context.mapFaqCategory(R.array.scanner_questions, R.array.scanner_answers))

        SERVICES -> viewState.faqData(context.getString(R.string.help_services),
                context.mapFaqCategory(R.array.services_questions, R.array.services_answers))

        SAVED_DEVICES -> viewState.faqData(context.getString(R.string.help_saved_devices),
                context.mapFaqCategory(R.array.saved_devices_questions, R.array.saved_devices_answers))

        else -> Unit
    }

    private fun MutableList<FaqItem>.fill(questions: Array<out String>, answersArray: Array<String>) {
        questions.forEachIndexed { index, question ->
            val faqItem = FaqItem(Question(question), Answer(answersArray[index]))
            add(faqItem)
        }
    }

    private fun Context.mapFaqCategory(@ArrayRes questionsId: Int, @ArrayRes answersId: Int) =
            mutableListOf<FaqItem>().apply {
                fill(getStringArray(questionsId), getStringArray(answersId))
            }
}
