package com.oopsi.ui.help


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.SAVED_DEVICES
import com.artjoker.models.SCANNER
import com.artjoker.models.SERVICES
import com.artjoker.models.STORIES
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.help.faq.FaqActivity
import kotlinx.android.synthetic.main.activity_help.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class HelpActivity : BaseActivity(), HelpView {


    @Inject
    @InjectPresenter
    lateinit var presenter: HelpPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    override val layoutId: Int = R.layout.activity_help

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { router.exit() }
        stories.setOnClickListener { router.navigateTo(FaqActivity.key(), STORIES) }
        scanner.setOnClickListener { router.navigateTo(FaqActivity.key(), SCANNER) }
        services.setOnClickListener { router.navigateTo(FaqActivity.key(), SERVICES) }
        savedDevices.setOnClickListener { router.navigateTo(FaqActivity.key(), SAVED_DEVICES) }
        help.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.type = "text/plain"
            intent.data = Uri.parse("mailto:${getString(R.string.help_mail_address)}")
            intent.putExtra(Intent.EXTRA_EMAIL, getString(R.string.help_mail_address))
            intent.resolveActivity(packageManager)?.let {
                startActivity(intent)
            }
        }
        telegram.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/oopsiapp"))
            intent.resolveActivity(packageManager)?.let {
                startActivity(intent)
            }
        }
    }


    override val navigator: Navigator = object : BaseNavigator() {
        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    FaqActivity.key() -> FaqActivity.getIntent(this@HelpActivity, data as Int)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        fun getIntent(context: Context) = Intent(context, HelpActivity::class.java)
        fun key(): String = HelpActivity::class.java.simpleName
    }
}