package com.oopsi.ui.devices.gallery

import com.arellomobile.mvp.InjectViewState
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject

@InjectViewState
class GalleryPresenter @Inject constructor() : BaseMvpPresenter<GalleryView>() {

    fun showGallery(images: ArrayList<String>?, deviceTitle: String, position: Int) {
        images?.apply { viewState.showGallery(images, deviceTitle, position) }
    }
}
