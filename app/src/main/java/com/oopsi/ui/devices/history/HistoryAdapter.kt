package com.oopsi.ui.devices.history

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_history.view.*


class HistoryAdapter constructor(data: MutableList<HistoryDevice>) : AbstractRecyclerViewAdapter<HistoryDevice>(data) {

    private val onClickBus = RxBus<ClickHolder>()
    val onClickBusObs = onClickBus.toObservable()

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_history, parent, false)
        return EntityViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EntityViewHolder)
                .bind(data[position])
    }

    fun resetFavourite(device: HistoryDevice) {
        val find = data.find { it.device.id == device.device.id }
        val indexOf = data.indexOf(find)
        if (indexOf != -1) {
            data[indexOf] = device
            notifyItemChanged(indexOf)
        }
    }

    fun remove(device: HistoryDevice) {
        val indexOf = data.indexOf(device)
        if (indexOf != -1) {
            data.remove(device)
            notifyItemRemoved(indexOf)
        }
    }

    inner class EntityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(historyDevice: HistoryDevice) {
            with(itemView) {

                swipeLayout.reset()

                brand.text = historyDevice.device.brand.name
                model.text = historyDevice.device.model

                favoriteImage.setImageResource(R.drawable.ic_favorite.takeIf { historyDevice.favourite }
                        ?: R.drawable.ic_favorite_empty)

                device.setOnClickListener { sendOnClick(HistoryClickType.DEVICE) }
                favoriteContainer.setOnClickListener { sendOnClick(HistoryClickType.TOGGLE_FAVORITE) }
                removeContainer.setOnClickListener { sendOnClick(HistoryClickType.REMOVE_FROM_HISTORY) }
            }
        }

        private fun sendOnClick(typeHistory: HistoryClickType) {
            onClickBus.send(ClickHolder(data[adapterPosition], typeHistory))
        }
    }

    class ClickHolder(val device: HistoryDevice, val typeHistory: HistoryClickType)
}

enum class HistoryClickType {
    DEVICE, TOGGLE_FAVORITE, REMOVE_FROM_HISTORY
}
