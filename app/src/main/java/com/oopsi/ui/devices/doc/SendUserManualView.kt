package com.oopsi.ui.devices.doc

import com.arellomobile.mvp.MvpView

interface SendUserManualView : MvpView {
    fun updateViewForSending(validEmail: Boolean)
    fun onSuccess()
    fun onError()
}
