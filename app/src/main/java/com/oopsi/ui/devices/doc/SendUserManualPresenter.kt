package com.oopsi.ui.devices.doc

import android.text.TextUtils
import android.util.Patterns
import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.SendDocumentationUseCase
import com.artjoker.repositories.DeviceRepository
import com.oopsi.ui.base.BaseMvpPresenter
import javax.inject.Inject


@InjectViewState
class SendUserManualPresenter @Inject constructor(
        var sendToEmail: SendDocumentationUseCase,
        var deviceRepository: DeviceRepository
) : BaseMvpPresenter<SendUserManualView>() {

    lateinit var email: String

    fun updateEmail(email: String) {
        val isValidEmail = !TextUtils.isEmpty(email) && validEmail(email)
        viewState.updateViewForSending(isValidEmail)
        this@SendUserManualPresenter.email = email
    }

    private fun validEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun sendUserManual(email: String) = sendToEmail.execute(
            SendDocumentationUseCase.Params(deviceRepository.device, email),
            object : PresenterSingleObserver<Boolean>() {
                override fun onSuccess(t: Boolean) {
                    viewState.onSuccess()
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    viewState.onError()
                }
            })
}
