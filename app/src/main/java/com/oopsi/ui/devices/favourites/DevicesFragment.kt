package com.oopsi.ui.devices.favourites

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Device
import com.oopsi.R
import com.oopsi.ui.base.BaseFragment
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_devices.*
import javax.inject.Inject

class DevicesFragment : BaseFragment(), FavouritesView {

    override val layoutId: Int = R.layout.fragment_devices

    @Inject
    @InjectPresenter
    lateinit var presenter: FavouriteDevicesPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val favouriteDevicesAdapter = FavouriteDevicesAdapter(mutableListOf())
        recyclerView.adapter = favouriteDevicesAdapter
        favouriteDevicesAdapter.itemClickBusObs
                .subscribe { presenter.showDetails(it) }
                .also { add(it) }
        favouriteDevicesAdapter.removeAction
                .toObservable()
                .subscribe {
                    val device = favouriteDevicesAdapter.data[it]
                    presenter.remove(device)
                    favouriteDevicesAdapter.data.removeAt(it)
                    favouriteDevicesAdapter.notifyItemRemoved(it)
                }
                .also { add(it) }
    }

    override fun onDeviceChanged(it: List<Device>) {
        noDevices.setVisible(it.isEmpty())
        val favouriteDevicesAdapter = recyclerView.adapter as FavouriteDevicesAdapter
        favouriteDevicesAdapter.data = it.toMutableList()
        favouriteDevicesAdapter.notifyDataSetChanged()
    }
}
