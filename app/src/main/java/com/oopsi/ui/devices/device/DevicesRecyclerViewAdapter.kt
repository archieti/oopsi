package com.oopsi.ui.devices.device

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.artjoker.rx.RxBus
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import org.jetbrains.anko.find

class DevicesRecyclerViewAdapter(data: MutableList<String>) : AbstractRecyclerViewAdapter<String>(data) {

    lateinit var context: Context
    var positionClickBus = RxBus<Int>()

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_recycler_device_image, parent, false)
        return DeviceViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myOptions = RequestOptions()
                .centerCrop()
        Glide.with(context)
                .load(data[position])
                .apply(myOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into((holder as DeviceViewHolder).deviceImage)
    }

    inner class DeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val cardView: CardView = itemView.find(R.id.item_view)
        val deviceImage: ImageView = itemView.find(R.id.deviceImage)

        init {
            cardView.setOnClickListener { positionClickBus.send(adapterPosition) }
        }
    }
}