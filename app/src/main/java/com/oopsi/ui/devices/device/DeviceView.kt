package com.oopsi.ui.devices.device

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Device

interface DeviceView : MvpView {
    fun showDevice(device: Device, favourite: Boolean)
    fun showGallery(position: Int, images: List<String>?, brand: String)
    fun showSendUserManual(userManualUrl: String)
    fun noInternet()
    fun showPreview(isPreview: Boolean)
}
