package com.oopsi.ui.devices.favourites

import android.view.View
import com.artjoker.models.Device
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.scanner.search.DevicesAdapter
import kotlinx.android.synthetic.main.item_device_favourite.view.*


class FavouriteDevicesAdapter(data: MutableList<Device>) : DevicesAdapter(data) {

    val removeAction = RxBus<Int>()

    override val layoutRes = R.layout.item_device_favourite

    override fun viewHolder(view: View): EntityViewHolder = FavouriteEntityViewHolder(view)

    private inner class FavouriteEntityViewHolder(itemView: View) : EntityViewHolder(itemView) {

        init {
            itemView.remove.setOnClickListener { removeAction.send(adapterPosition) }
        }
    }
}
