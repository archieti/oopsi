package com.oopsi.ui.devices.gallery

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_gallery.*
import javax.inject.Inject


class GalleryActivity : BaseActivity(), GalleryView {

    override val layoutId: Int = R.layout.activity_gallery

    @Inject
    @InjectPresenter
    lateinit var mGalleryPresenter: GalleryPresenter

    @ProvidePresenter
    fun providemGalleryPresenter() = mGalleryPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
        mGalleryPresenter.showGallery(intent.getStringArrayListExtra(IMAGES_TAG),
                intent.getStringExtra(DEVICE_TAG),
                intent.getIntExtra(POSITION_TAG, 0))
    }

    private fun initListeners() {
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun showGallery(images: ArrayList<String>,
                             deviceTitle: String,
                             position: Int) {
        val imageAdapter = ImageAdapter(supportFragmentManager, images)
        imagesViewPager.adapter = imageAdapter
        imagesViewPager.pageMargin = resources.getDimension(R.dimen.pager_margin).toInt()
        indicator.setViewPager(imagesViewPager)
        imagesViewPager.setCurrentItem(position, true)
        toolbar.title = deviceTitle
    }


    companion object {
        const val TAG = "GalleryActivity"
        const val IMAGES_TAG = "IMAGES_TAG"
        const val DEVICE_TAG = "DEVICE_TAG"
        const val POSITION_TAG = "POSITION_TAG"

        fun getIntent(context: Context, images: ArrayList<String>, deviceTitle: String, position: Int): Intent =
                Intent(context, GalleryActivity::class.java).apply {
                    putStringArrayListExtra(IMAGES_TAG, images)
                    putExtra(DEVICE_TAG, deviceTitle)
                    putExtra(POSITION_TAG, position)
                }
    }
}
