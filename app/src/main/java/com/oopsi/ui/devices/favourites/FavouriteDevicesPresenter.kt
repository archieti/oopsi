package com.oopsi.ui.devices.favourites

import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.devices.device.DeviceActivity
import javax.inject.Inject

@InjectViewState
class FavouriteDevicesPresenter @Inject constructor(
        private val deviceRepository: DeviceRepository
) : BaseMvpPresenter<FavouritesView>() {

    override fun attachView(view: FavouritesView?) {
        super.attachView(view)
        val favouriteDevices = deviceRepository.getFavouriteDevices()
        viewState.onDeviceChanged(favouriteDevices)
    }

    fun showDetails(it: Device) {
        deviceRepository.device = it
        router.navigateTo(DeviceActivity.key())
    }

    fun remove(device: Device) = deviceRepository.toggleFavourite(device)
}
