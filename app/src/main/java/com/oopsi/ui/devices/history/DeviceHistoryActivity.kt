package com.oopsi.ui.devices.history


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Device
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.activity_device_history.*
import javax.inject.Inject


class DeviceHistoryActivity : BaseActivity(), DeviceHistoryView {

    @Inject
    @InjectPresenter
    lateinit var presenter: DeviceHistoryPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    private lateinit var historyAdapter: HistoryAdapter

    override val layoutId: Int = R.layout.activity_device_history

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back.setOnClickListener { finish() }
        recyclerView.layoutManager = LinearLayoutManager(this)
        historyAdapter = HistoryAdapter(ArrayList())
        recyclerView.adapter = historyAdapter
    }

    override fun showProgress() {
        emptyState.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }

    override fun retrieveHistory(history: MutableList<HistoryDevice>) {
        history.reverse()
        with(historyAdapter) {
            data = history
            notifyDataSetChanged()
            onClickBusObs
                    .subscribe {
                        presenter.onDevice(it.device, it.typeHistory)
                    }
                    .also { manage(it) }
        }
        syncEmptyMsg()
    }

    override fun onFavouritesChanged(device: Device, fav: Boolean) {
        resetFavourite(HistoryDevice(device, fav))
    }

    override fun resetFavourite(device: HistoryDevice) {
        historyAdapter.resetFavourite(device)
    }

    override fun remove(device: HistoryDevice) {
        historyAdapter.remove(device)
        syncEmptyMsg()
    }

    private fun syncEmptyMsg() {
        emptyState.setVisible(historyAdapter.data.isEmpty())
    }


    companion object {
        fun key(): String = DeviceHistoryActivity::class.java.simpleName
    }
}
