package com.oopsi.ui.devices.doc

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.views.result.Fail
import com.oopsi.ui.views.result.Result
import com.oopsi.ui.views.result.Success
import kotlinx.android.synthetic.main.activity_user_manual.*
import javax.inject.Inject


class SendUserManualActivity : BaseActivity(), SendUserManualView {

    override val layoutId: Int = R.layout.activity_user_manual

    @Inject
    @InjectPresenter
    lateinit var mSendUserManualPresenter: SendUserManualPresenter

    @ProvidePresenter
    fun providePresenter() = mSendUserManualPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
    }

    private fun initListeners() {
        emailEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                mSendUserManualPresenter.updateEmail(p0.toString())
            }
        })
        backManualButton.setOnClickListener { finish() }
        sendTextView.setOnClickListener { mSendUserManualPresenter.sendUserManual(emailEditText.text.toString()) }
    }

    override fun updateViewForSending(validEmail: Boolean) {
        sendTextView.isEnabled = validEmail
    }

    override fun onSuccess() {
        showDialog(true)
    }

    override fun onError() {
        showDialog(false)
    }

    private fun showDialog(isSuccess: Boolean) {
        getDialog(isSuccess)
                .builder()
                .message(if (isSuccess) R.string.manual_success_dialog_message else R.string.fail_manual_doc_result)
                .actionText(R.string.continue_title)
                .title(if (isSuccess) R.string.success_title else R.string.sorry)
                .action(getAction(isSuccess))
                .create()
                .show()
    }

    private fun getDialog(isSuccess: Boolean): Result = when (isSuccess) {
        true -> Success(this)
        else -> Fail(this)
    }

    private fun getAction(success: Boolean): DialogInterface.OnClickListener =
            if (success) {
                DialogInterface.OnClickListener { dialog, _ ->
                    finish()
                    dialog.dismiss()
                }
            } else {
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }
            }

    companion object {
        const val TAG = "UserManualActivity"

        fun getIntent(context: Context) = Intent(context, SendUserManualActivity::class.java)
    }
}
