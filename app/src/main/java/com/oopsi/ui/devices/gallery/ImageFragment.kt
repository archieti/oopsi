package com.oopsi.ui.devices.gallery

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.oopsi.R
import com.oopsi.ui.views.zoom.ZoomageView
import kotlinx.android.synthetic.main.fragment_image.*
import org.jetbrains.anko.find

/**
 * Created by Nikita Skubak on 25.03.2018.
 */

class ImageFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imageView: ZoomageView = view.find(R.id.image)
        progress.visibility = View.VISIBLE
        Glide.with(this)
                .load(arguments?.getString(IMAGE_URL_TAG))
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                })
                .into(imageView)
    }

    companion object {
        const val IMAGE_URL_TAG = "IMAGE_URL_TAG"

        fun newInstance(imageUrl: String): ImageFragment {
            val args = Bundle()
            args.putString(IMAGE_URL_TAG, imageUrl)
            val fragment = ImageFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
