package com.oopsi.ui.devices.device

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.artjoker.interactors.DownloadDeviceDocumentationUseCase
import com.artjoker.interactors.devices.DeviceDetailsUseCase
import com.artjoker.models.BrandFilter
import com.artjoker.models.Device
import com.artjoker.models.Filter
import com.artjoker.repositories.DeviceRepository
import com.artjoker.repositories.ServicesRepository
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.utils.Network
import javax.inject.Inject

@InjectViewState
class DevicePresenter @Inject constructor(
        private val getDeviceDetails: DeviceDetailsUseCase,
        private val getDocumentation: DownloadDeviceDocumentationUseCase
) : BaseMvpPresenter<DeviceView>() {

    lateinit var device: Device
    @Inject lateinit var servicesRepository: ServicesRepository
    @Inject lateinit var deviceRepository: DeviceRepository

    fun showDeviceDetails() = getDeviceDetails.execute(object : PresenterSingleObserver<Device>() {
        override fun onSuccess(device: Device) {
            this@DevicePresenter.device = device
            servicesRepository.filter = Filter(brand = BrandFilter(device.brand))
            viewState.showDevice(device, deviceRepository.isFavourite(device))
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }
    })


    fun openGallery(position: Int) {
        viewState.showGallery(position, device.images, device.brand.name)
    }

    fun openUserManualSending() {
        device.userManualUrl.apply { viewState.showSendUserManual(this) }
    }

    fun downloadDocumentation(context: Context) {
        when {
            Network.isConnected(context) -> getDocumentation.execute(device)
            else -> viewState.noInternet()
        }
    }

    fun setFavourite(isFavourite: Boolean) = deviceRepository.toggleFavourite(device)
}
