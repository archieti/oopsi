package com.oopsi.ui.devices.favourites

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Device

interface FavouritesView : MvpView {
    fun onDeviceChanged(it: List<Device>)

}
