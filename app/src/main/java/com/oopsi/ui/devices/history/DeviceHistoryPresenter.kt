package com.oopsi.ui.devices.history

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.devices.device.DeviceActivity
import javax.inject.Inject


data class HistoryDevice(val device: Device, val favourite: Boolean)


interface DeviceHistoryView : MvpView {
    fun retrieveHistory(history: MutableList<HistoryDevice>)
    fun resetFavourite(device: HistoryDevice)
    fun remove(device: HistoryDevice)
}


@InjectViewState
class DeviceHistoryPresenter @Inject constructor(
        private val deviceRepository: DeviceRepository
) : BaseMvpPresenter<DeviceHistoryView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        val history = deviceRepository.getHistory()
        val favourites = deviceRepository.getFavouriteDevices()
        val historyDevices = mutableListOf<HistoryDevice>()

        history.forEach { historyDev ->
            val favourite = favourites.find { it.id == historyDev.id } != null
            historyDevices.add(HistoryDevice(historyDev, favourite))
        }

        viewState.retrieveHistory(historyDevices)
    }

    fun onDevice(historyDevice: HistoryDevice, clickType: HistoryClickType) {
        when (clickType) {
            HistoryClickType.DEVICE -> {
                deviceRepository.device = historyDevice.device
                router.navigateTo(DeviceActivity.key())
            }
            HistoryClickType.TOGGLE_FAVORITE -> {
                val toggleFavourite = deviceRepository.toggleFavourite(historyDevice.device)
                val device = historyDevice.copy(favourite = toggleFavourite)
                viewState.resetFavourite(device)
            }
            HistoryClickType.REMOVE_FROM_HISTORY -> {
                deviceRepository.removeFromHistory(historyDevice.device)
                viewState.remove(historyDevice)
            }
        }
    }
}