package com.oopsi.ui.devices.gallery

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by Nikita Skubak on 25.03.2018.
 */

class ImageAdapter(supportFragmentManager: FragmentManager, private var imageUrls: List<String>)
    : FragmentPagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment {
        return ImageFragment.newInstance(imageUrls[position])
    }

    override fun getCount(): Int {
        return imageUrls.size
    }
}
