package com.oopsi.ui.devices.gallery

import com.arellomobile.mvp.MvpView
import java.util.*

interface GalleryView : MvpView {
    fun showGallery(images: ArrayList<String>,
                    deviceTitle: String,
                    position: Int)
}
