package com.oopsi.ui.devices.device


import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout.HORIZONTAL
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.data.repositories.ScannerRepository
import com.artjoker.models.Device
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.devices.doc.SendUserManualActivity
import com.oopsi.ui.devices.gallery.GalleryActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.ui.scanner.label.LabelPreviewActivity
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.utils.setVisible
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_device.*
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject


class DeviceActivity : BaseActivity(), DeviceView {

    @Inject
    @InjectPresenter
    lateinit var devicePresenter: DevicePresenter

    @Inject
    lateinit var scannerRepository: ScannerRepository

    @ProvidePresenter
    fun provideDevicePresenter() = devicePresenter

    lateinit var onFavoriteChangeAction: (CompoundButton, Boolean) -> Unit

    private lateinit var adapter: DevicesRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager

    override val layoutId: Int = R.layout.activity_device

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initListeners()
        devicePresenter.showDeviceDetails()
    }

    private fun initViews() {

        onFavoriteChangeAction = { _, isChecked ->
            devicePresenter.setFavourite(isChecked)
            val text = getString(R.string.in_favorites_title).takeIf { isChecked }
                    ?: getString(R.string.add_to_favorites_title)
            favouriteState.text = text
        }
        favouriteState.setOnCheckedChangeListener(onFavoriteChangeAction)
        adapter = DevicesRecyclerViewAdapter(mutableListOf())
        layoutManager = LinearLayoutManager(this, HORIZONTAL, false)
        deviceGalleryRecyclerView.adapter = adapter
        deviceGalleryRecyclerView.layoutManager = layoutManager
        deviceGalleryRecyclerView.isNestedScrollingEnabled = false
        close.setOnClickListener {
            val intent = MainActivity.getIntent(this).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            }
            startActivity(intent)
            finish()
        }
    }

    override fun showPreview(isPreview: Boolean) = showLabelContainer.setVisible(isPreview)


    private fun initListeners() {
        toolbar.setNavigationOnClickListener { finish() }
        adapter.positionClickBus
                .toObservable()
                .subscribe(devicePresenter::openGallery)
                .let(::manage)
        sendUserManualText.setOnClickListener { devicePresenter.openUserManualSending() }
        downloadUserManualText.setOnClickListener {
            RxPermissions(this)
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE)
                    .filter { permission -> permission }
                    .subscribe { devicePresenter.downloadDocumentation(this) }
        }

        searchServiceButton.setOnClickListener { router.navigateTo(FilteredServicesActivity.key()) }
    }

    override fun showDevice(device: Device, favourite: Boolean) {

        val image = scannerRepository.getFromGallery(device.labelUrl)
        showPreview(image != null)

        showLabel.setOnClickListener { router.navigateTo(LabelPreviewActivity.key(), device.labelUrl) }

        collapsing_toolbar.title = device.brand.name
        loadBrandBackgroundImage(device.backgroundImage)
        favouriteState.setOnCheckedChangeListener(null)
        favouriteState.isChecked = favourite
        val text = getString(R.string.in_favorites_title).takeIf { favourite }
                ?: getString(R.string.add_to_favorites_title)
        favouriteState.text = text
        favouriteState.setOnCheckedChangeListener(onFavoriteChangeAction)
        modelTextView.text = device.model
        typeTextView.text = device.type.name
        versionTextView.text = if (TextUtils.isEmpty(device.version)) "-" else device.version

        deviceGalleryLabel.visibility = View.GONE.takeIf { device.thumbs.isEmpty() } ?: View.VISIBLE
        adapter.data = device.thumbs.toMutableList()
        adapter.notifyDataSetChanged()

        manual.setVisible(device.userManualUrl.isNotEmpty())
        manualUnavailable.setVisible(device.userManualUrl.isEmpty())
    }

    override fun showGallery(position: Int, images: List<String>?, brand: String) {
        startActivity(GalleryActivity.getIntent(this, ArrayList(images), brand, position))
    }

    override fun showSendUserManual(userManualUrl: String) {
        startActivity(SendUserManualActivity.getIntent(this))
    }

    override fun noInternet() {
        Toast.makeText(this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show()
    }

    private fun loadBrandBackgroundImage(url: String?) {
        val myOptions = RequestOptions()
                .centerCrop()

        Glide.with(this)
                .load(url)
                .apply(myOptions)
                .transition(withCrossFade())
                .into(brandBackgroundImage)
    }


    override val navigator: Navigator = object : BaseNavigator() {
        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    LabelPreviewActivity.key() -> intentFor<LabelPreviewActivity>(LabelPreviewActivity.EXT_URL to data as String)
                    else -> super.createActivityIntent(context, screenKey, data)
                }
    }


    companion object {
        fun key(): String = DeviceActivity::class.java.simpleName
    }
}
