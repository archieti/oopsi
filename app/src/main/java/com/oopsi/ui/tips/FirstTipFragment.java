package com.oopsi.ui.tips;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oopsi.R;

public class FirstTipFragment extends Fragment {

    public static FirstTipFragment newInstance() {
        Bundle args = new Bundle();
        FirstTipFragment fragment = new FirstTipFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_tip, container, false);
    }
}
