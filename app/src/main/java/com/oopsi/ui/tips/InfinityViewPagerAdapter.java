package com.oopsi.ui.tips;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class InfinityViewPagerAdapter extends FragmentPagerAdapter implements InfinityPager {
    public static final int LOOP_COUNT = 1000;

    public InfinityViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        int index = position % getRealCount();
        switch (index) {
            case 0:
                return FirstTipFragment.newInstance();
            case 1:
                return SecondTipFragment.newInstance();
            case 2:
                return ThirdTipFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getRealCount() {
        return 3;
    }

}