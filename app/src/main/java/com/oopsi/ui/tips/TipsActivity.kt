package com.oopsi.ui.tips

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_tips.*

class TipsActivity : BaseActivity() {

    override val layoutId: Int = R.layout.activity_tips

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val adapter = InfinityViewPagerAdapter(supportFragmentManager)
        pager.adapter = adapter
        close.setOnClickListener { finish() }
        indicator.setViewPager(pager)
        indicator.setSmoothTransition(true)
        indicator.setDiscreteIndicator(true)
        indicator.setIndicatorsClickChangePage(false)
    }


    override fun onBackPressed() {
        //do nothing
    }

    companion object {
        fun getIntent(context: Context) = Intent(context, TipsActivity::class.java)
        fun key(): String = TipsActivity::class.java.simpleName
    }
}
