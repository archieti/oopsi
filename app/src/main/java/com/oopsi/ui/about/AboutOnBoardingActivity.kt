package com.oopsi.ui.about

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.ui.onboarding.ViewPagerAdapter
import com.oopsi.ui.onboarding.slide.SlideFragment
import kotlinx.android.synthetic.main.activity_about_the_app.*


class AboutOnBoardingActivity : BaseActivity() {

    override val layoutId = R.layout.activity_about_the_app

    lateinit var adapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        backButton.setOnClickListener {
            finish()
        }
    }

    private fun initViews() {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(SlideFragment.newInstance(R.drawable.first_slide,
                getString(R.string.onboarding_1_title),
                getString(R.string.onboarding_1_body)))
        adapter.addFragment(SlideFragment.newInstance(R.drawable.second_slide,
                getString(R.string.onboarding_2_title),
                getString(R.string.onboarding_2_body)))
        adapter.addFragment(SlideFragment.newInstance(R.drawable.third_slide,
                getString(R.string.onboarding_3_title),
                getString(R.string.onboarding_3_body)))
        slidesViewPager.adapter = adapter
        indicator.setViewPager(slidesViewPager)
        indicator.setSmoothTransition(true)
        indicator.setDiscreteIndicator(true)
        indicator.setIndicatorsClickChangePage(false)
    }


    companion object {
        fun key(): String = AboutOnBoardingActivity::class.java.simpleName
        fun getIntent(context: Context) = Intent(context, AboutOnBoardingActivity::class.java)
    }
}
