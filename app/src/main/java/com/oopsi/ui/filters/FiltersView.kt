package com.oopsi.ui.filters

import com.arellomobile.mvp.MvpView
import com.artjoker.models.Day

interface FiltersView : MvpView {
    fun onDistanceChanged(distance: Int, fromUser: Boolean)
    fun onDaysReady(day: List<Day>)
    fun onDaySelected(day: Day)
    fun onStartTimeChanged(format: String?)
    fun onEndTimeChanged(format: String?)
    fun isCanFilter(isCanFilter: Boolean)
    fun showProgress()
    fun hideProgress()
}
