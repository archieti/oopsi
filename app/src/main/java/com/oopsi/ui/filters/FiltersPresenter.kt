package com.oopsi.ui.filters

import android.annotation.SuppressLint
import com.arellomobile.mvp.InjectViewState
import com.artjoker.models.*
import com.artjoker.models.RadiusFilter.Companion.MAX_DISTANCE
import com.artjoker.repositories.ServicesRepository
import com.artjoker.utils.DayOfWeek
import com.oopsi.ui.base.BaseMvpPresenter
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.utils.fromPercents
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


@InjectViewState
class FiltersPresenter @Inject constructor(
        private val servicesRepository: ServicesRepository
) : BaseMvpPresenter<FiltersView>() {

    @SuppressLint("SimpleDateFormat")
    private val timeFormat = SimpleDateFormat("HH:mm")
    private var filter = Filter()

    override fun onFiltersChanged(filters: Filter) {
        reset(filters)
    }

    fun reset(filters: Filter) {
        filter = filters

        syncDist(filter.radius?.value ?: 0, false)
        selectDay(filter.workingDay?.value?.toDay() ?: Day(DayOfWeek.ALL))
        startTime(filter.startTime?.value)
        endTime(filter.endTime?.value)
    }

    fun calculateDistance(progress: Int, fromUser: Boolean) {
        val radius = (MAX_DISTANCE * progress.fromPercents()).toInt()
        syncDist(radius, fromUser)
    }

    private fun syncDist(radius: Int, fromUser: Boolean) {
        filter = filter.copy(radius = RadiusFilter(radius))
        viewState.onDistanceChanged(radius, fromUser)
        viewState.isCanFilter(filter.anyActiveFilter)
    }

    fun fillDays(defaultValue: String) {
        val day = DayOfWeek.values()
                .map { it.toDay(defaultValue) }
                .sortedBy { it.dayOfWeek.dayIndex() }
        viewState.onDaysReady(day)
    }

    fun selectDay(day: Day) {
        viewState.onDaySelected(day)
        filter = filter.copy(workingDay = WorkingDayFilter(day.dayOfWeek))
        viewState.isCanFilter(filter.anyActiveFilter)
    }

    fun startTime(date: Date?) {
        val pairFilter = pairFilter(date, ::StartTimeFilter)

        filter = filter.copy(startTime = pairFilter.second)
        viewState.onStartTimeChanged(pairFilter.first)
        viewState.isCanFilter(filter.anyActiveFilter)
    }

    fun endTime(date: Date?) {
        val pairFilter = pairFilter(date, ::EndTimeFilter)

        filter = filter.copy(endTime = pairFilter.second)
        viewState.onEndTimeChanged(pairFilter.first)
        viewState.isCanFilter(filter.anyActiveFilter)
    }

    fun filter(withBrand: Brand?) {
        val brand = when (withBrand) {
            null -> null
            else -> BrandFilter(withBrand)
        }
        filter = filter.copy(brand = brand)
        servicesRepository.filter = filter
        events.notifyListeners { it.onFiltersChanged(filter) }
        router.navigateTo(FilteredServicesActivity.key())
    }

    private inline fun <reified T> pairFilter(date: Date?, creator: (Date) -> T): Pair<String?, T?> {
        var filter: T? = null
        var time: String? = null
        if (date != null) {
            filter = creator(date)
            time = timeFormat.format(date)
        }
        return time to filter
    }
}
