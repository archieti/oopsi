package com.oopsi.ui.filters

import android.app.TimePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.SeekBar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.artjoker.models.Day
import com.artjoker.models.Filter
import com.artjoker.models.RadiusFilter.Companion.MAX_DISTANCE
import com.artjoker.utils.fromTime
import com.oopsi.R
import com.oopsi.ui.base.BaseActivity
import com.oopsi.utils.SimpleTextWatcher
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.activity_filters.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*
import javax.inject.Inject


class FiltersActivity : BaseActivity(), FiltersView {

    override val layoutId: Int = R.layout.activity_filters

    @Inject
    @InjectPresenter
    lateinit var presenter: FiltersPresenter

    @ProvidePresenter()
    fun providePresenter() = presenter

    private val extFilter get() = intent?.extras?.getSerializable(EXT_FILTER) as? Filter

    private val distanceFormatter = object : SimpleTextWatcher() {
        override fun afterTextChanged(s: Editable?) {
            val toString = s?.toString()
            when {
                toString.isNullOrEmpty() -> Unit
                toString!!.toInt() > MAX_DISTANCE -> onDistanceChanged(MAX_DISTANCE, false)
                else -> onDistanceChanged(toString.toInt(), false)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        close.setOnClickListener { router.exit() }
        showResults.setOnClickListener {
            presenter.filter(extFilter?.brand?.value)
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = DaysAdapter(mutableListOf())
        distanceFlow()
        daysFlow()
        timeFlow()

        extFilter?.let(presenter::reset)
    }

    private fun timeFlow() {

        startContainer.setOnClickListener {
            showTimePicker(10, 0, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                presenter.startTime(Calendar.getInstance().fromTime(hourOfDay, minute))
            })
        }

        endContainer.setOnClickListener {
            showTimePicker(18, 0, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                presenter.endTime(Calendar.getInstance().fromTime(hourOfDay, minute))
            })
        }

        startTime.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                clearStart.setVisible(!s.isNullOrEmpty())
            }
        })

        endTime.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                clearEnd.setVisible(!s.isNullOrEmpty())
            }
        })

        clearStart.setOnClickListener { presenter.startTime(null) }
        clearEnd.setOnClickListener { presenter.endTime(null) }
    }

    private fun showTimePicker(hour: Int, minute: Int, listener: TimePickerDialog.OnTimeSetListener) {
        TimePickerDialog(this,
                R.style.TimePickerDialogTheme,
                listener,
                hour,
                minute,
                true)
                .show()
    }

    private fun daysFlow() {
        val daysAdapter = recyclerView.adapter as DaysAdapter

        selectedDay.setOnClickListener {
            expandableLayout.toggle()
            daysAdapter.notifyItemChanged(0)
        }
        expandableLayout.setOnExpansionUpdateListener { _, state ->
            when (state) {
                ExpandableLayout.State.COLLAPSING -> todayLayout.setVisible(true)
                ExpandableLayout.State.EXPANDING -> todayLayout.setVisible(false)
            }
        }
        presenter.fillDays(getString(R.string.any_day))

        daysAdapter.itemClickBusObs
                .subscribe { presenter.selectDay(it) }
                .let { manage(it) }

        daysAdapter.collapseAction.toObservable()
                .subscribe { expandableLayout.toggle() }
                .let { manage(it) }
    }

    private fun distanceFlow() {
        distanceSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) =
                    presenter.calculateDistance(progress, fromUser)

        })
        distanceView.addTextChangedListener(distanceFormatter)
        distanceView.setSelection(distanceView.text.length)
        distanceView.setOnEditorActionListener { _, actionId, event ->
            if (event?.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
            }
            false
        }
    }

    override fun onDistanceChanged(distance: Int, fromUser: Boolean) {
        distanceView.removeTextChangedListener(distanceFormatter)
        distanceView.setText("$distance")
        distanceView.setSelection("$distance".length)
        distanceView.addTextChangedListener(distanceFormatter)
        if (!fromUser) {
            distanceSeekBar.progress = ((distance.toFloat() / MAX_DISTANCE.toFloat()) * 100).toInt()
        }
    }

    override fun onDaysReady(day: List<Day>) {
        val daysAdapter = recyclerView.adapter as DaysAdapter
        daysAdapter.data = day.toMutableList()
        daysAdapter.notifyDataSetChanged()
    }

    override fun onDaySelected(day: Day) {
        selectedDay.text = day.name.takeIf { it.isNotEmpty() } ?: getString(R.string.any_day)
        selectedDay.setTextColor(Color.BLACK)
        expandableLayout.collapse()
    }

    override fun isCanFilter(isCanFilter: Boolean) {
        showResults.isEnabled = isCanFilter
    }

    override fun onStartTimeChanged(format: String?) {
        startTime.text = format
    }

    override fun onEndTimeChanged(format: String?) {
        endTime.text = format
    }


    companion object {
        const val EXT_FILTER = "EXT_FILTER"
        fun key(): String = FiltersActivity::class.java.simpleName
    }
}