package com.oopsi.ui.filters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.models.Day
import com.artjoker.rx.RxBus
import com.oopsi.R
import com.oopsi.ui.base.AbstractRecyclerViewAdapter
import com.oopsi.utils.setVisible
import kotlinx.android.synthetic.main.item_recycler_days.view.*


class DaysAdapter(data: MutableList<Day>) : AbstractRecyclerViewAdapter<Day>(data) {

    val collapseAction = RxBus<Unit>()

    override fun createHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_days, parent, false)
        return ViewHolder(view)
    }

    override fun bindHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { itemClickBus.send(data[adapterPosition]) }
            itemView.collapseIcon.setOnClickListener {
                collapseAction.send(Unit)
                it.setVisible(false)
            }
        }

        fun bind(day: Day) {
            itemView.dayTitle.text = day.name
            itemView.collapseIcon.setVisible(adapterPosition == 0)
        }
    }
}