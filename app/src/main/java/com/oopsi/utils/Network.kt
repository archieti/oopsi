package com.oopsi.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object Network {

    fun isConnected(context: Context): Boolean {
        return checkConnection((context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo)
    }

    private fun checkConnection(networkInfo: NetworkInfo?): Boolean = networkInfo?.isConnected
            ?: false

}