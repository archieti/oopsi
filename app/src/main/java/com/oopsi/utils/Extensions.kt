package com.oopsi.utils

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.support.annotation.ArrayRes
import android.view.View
import com.artjoker.models.Comment
import com.oopsi.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.textColor
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace
import java.util.*

fun View.setVisible(isVisible: Boolean) {
    visibility = View.VISIBLE.takeIf { isVisible } ?: View.GONE
}

fun View.circleBackground(color: Int) {
    background = ShapeDrawable(OvalShape())
            .apply {
                intrinsicHeight = height
                intrinsicWidth = width
                paint.color = color
            }
}

fun Random.color(): Int = Color.argb(255, nextInt(256), nextInt(256), nextInt(256))

fun Comment.findParentPosition(comments: List<Comment>): Int {
    parentId!!
    comments.forEachIndexed { index, comment ->
        if (comment.id == parentId)
            return index
    }
    return -1
}

fun Int.fromPercents(): Float = if (this > 0) this / 100f else 0f

fun Context.getStringArray(@ArrayRes id: Int) = resources.getStringArray(id)

fun Command.screenKey() =
        when (this) {
            is BackTo -> screenKey
            is Replace -> screenKey
            is Forward -> screenKey
            else -> null
        }

inline fun <reified T> Command.transitionData() =
        when (this) {
            is Replace -> transitionData
            is Forward -> transitionData
            else -> null
        } as T


fun Context.showError(messageId: Int) = alert(
        getString(messageId),
        getString(R.string.default_error_message)
) { okButton {} }

fun Context.showError(message: String) = alert(
        message,
        getString(R.string.default_error_message)
) { okButton {} }

fun AlertDialog.nonCancelable(): AlertDialog {
    setCancelable(false)
    setCanceledOnTouchOutside(false)
    return this
}

fun AlertDialog.tintBtns(): AlertDialog {
    getButton(AlertDialog.BUTTON_POSITIVE)?.let { it.textColor = context.primaryColor }
    getButton(AlertDialog.BUTTON_NEGATIVE)?.let { it.textColor = context.primaryColor }
    return this
}
