package com.oopsi.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import io.reactivex.disposables.Disposables


class LocationHelper(
        private val activity: Activity,
        private val listener: Listener) {

    private val client by lazy { LocationServices.getFusedLocationProviderClient(activity) }

    private val locationCallback = LocationCallbackImpl()

    private var permissionRequest = Disposables.empty()

    private val request: LocationRequest =
            LocationRequest.create()
                    .setInterval(10000)
                    .setFastestInterval(5000)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

    fun tryRequestLocation() {

        val locationPermission = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION)

        if (locationPermission == PackageManager.PERMISSION_GRANTED) {

            requestLocationIfSettingsAllow()

        } else {

            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION)

        }
    }

    fun cancelLocationUpdates() {
        permissionRequest.dispose()
        client.removeLocationUpdates(locationCallback)
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int) =
            when (requestCode) {
                REQUEST_CHECK_SETTINGS -> {

                    if (resultCode == Activity.RESULT_OK) {
                        requestLocation()
                    } else {
                        listener.onLocatingFail()
                    }

                    true
                }

                else -> false
            }

    fun handleRequestPermissionsResult(
            requestCode: Int,
            grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION -> {

                if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
                    requestLocationIfSettingsAllow()
                } else {
                    listener.onLocatingFail()
                }
            }
        }
    }

    private fun requestLocationIfSettingsAllow() {

        listener.onLocationRequested()

        LocationSettingsRequest.Builder()
                .addLocationRequest(request)
                .build()
                .let(LocationServices.getSettingsClient(activity)::checkLocationSettings)
                .addOnCompleteListener {
                    try {
                        it.getResult(ApiException::class.java)
                        requestLocation()
                    } catch (e: ApiException) {
                        handleException(e)
                    }
                }
    }

    private fun handleException(e: ApiException) {
        when (e.statusCode) {

            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {

                try {

                    (e as ResolvableApiException)
                            .startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)

                } catch (ignored: Exception) {
                }
            }

            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                listener.onLocatingFail()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        client.requestLocationUpdates(request, locationCallback, null)
    }

    private inner class LocationCallbackImpl : LocationCallback() {

        override fun onLocationResult(result: LocationResult) {

            result.lastLocation?.let {
                listener.onLocationDetected(it)
                //cancelLocationUpdates()
            }
        }
    }

    interface Listener {

        fun onLocationRequested()

        fun onLocationDetected(location: Location)

        fun onLocatingFail()
    }

    companion object {
        const val REQUEST_CHECK_SETTINGS = 3012
        const val REQUEST_LOCATION_PERMISSION = 3012
    }
}
