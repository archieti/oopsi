package com.oopsi.utils;

import android.content.Context;

import com.oopsi.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nikita Skubak on 15.04.2018.
 */

public class DateUtils {

    public static String getReadableDate(long dateTimeStamp, Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        long delta = currentTimeMillis - dateTimeStamp;
        if (delta < 60 * 1000) { //less then minute
            //seconds
            String seconds = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(delta));
            return String.format(context.getString(R.string.seconds_format), seconds);
        } else if (delta < 60 * 60 * 1000) { //less then hour
            // minutes
            String minutes = String.valueOf(TimeUnit.MILLISECONDS.toMinutes(delta));
            return String.format(context.getString(R.string.minutes_format), minutes);
        } else if (delta < 24 * 60 * 60 * 1000) { //less then day
            //hours
            String hours = String.valueOf(TimeUnit.MILLISECONDS.toHours(delta));
            return String.format(context.getString(R.string.hours_format), hours);
        } else {
            SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
            return date.format(new Date(dateTimeStamp));
        }
    }


}
