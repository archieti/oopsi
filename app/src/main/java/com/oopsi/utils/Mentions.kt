package com.oopsi.utils

import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import com.oopsi.R


val Context.primaryColor get() = ContextCompat.getColor(this, R.color.colorPrimary)

/** Editable - critical for some cases like real time input to edit text**/
fun Editable.applySpanToMentions(context: Context) = findMentions()
        .map { it.range }
        .map { Pair(it.start, it.endInclusive + 1) }
        .toList()
        .let { applySpanToMentions(context, it) }

fun Editable.applySpanToMentions(context: Context, indexes: Iterable<Pair<Int, Int>>) = this.apply {
    indexes.forEach { setMentionSpan(context.primaryColor, it.first, it.second) }
}

/** Strings  **/
fun String.applySpanToMentions(context: Context) = findMentions()
        .map { it.range }
        .map { Pair(it.start, it.endInclusive + 1) }
        .toList()
        .let { applySpanToMentions(context, it) }

fun String.applySpanToMentions(context: Context, indexes: Iterable<Pair<Int, Int>>) = SpannableString(this)
        .apply { indexes.forEach { setMentionSpan(context.primaryColor, it.first, it.second) } }

fun String.applySpanToMention(context: Context): SpannableString = applySpanToMentions(context, 0, length)

fun String.applySpanToMentions(context: Context, start: Int, end: Int): SpannableString = SpannableString(this)
        .apply { setMentionSpan(context.primaryColor, start, end) }

val mentionRegex = Regex("@[A-Za-z0-9_-]*")
fun CharSequence.findMentions() = mentionRegex.findAll(this)

fun Spannable.setMentionSpan(color: Int, start: Int, end: Int) {
    setSpan(ForegroundColorSpan(color), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    setSpan(StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
}