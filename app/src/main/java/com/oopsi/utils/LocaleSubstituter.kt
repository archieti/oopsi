package com.oopsi.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.LocaleList
import com.artjoker.data.storage.LangStorageImpl
import java.util.*


object LocaleSubstituter {

    fun substitute(base: Context): Context {

        val langStorage = LangStorageImpl(base)
        val lang = langStorage.language
        val locale = Locale(lang.locale.language, lang.locale.country)

        return SetupLocaleAction(base).provideContext(locale)
    }
}


class SetupLocaleAction(private val context: Context) {

    fun provideContext(locale: Locale): Context {
        Locale.setDefault(locale)
        return context.createConfigurationContext(makeConfiguration(locale))
    }

    private fun makeConfiguration(locale: Locale): Configuration {
        val config = context.resources.configuration

        config.setLayoutDirection(locale)
        doSetLocale(config, locale)

        return config
    }

    private fun doSetLocale(config: Configuration, locale: Locale) {
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> config.locales = LocaleList(locale)
            else -> config.locale = locale
        }
    }
}
