package com.oopsi

import android.content.Context

import com.artjoker.repositories.Density

import javax.inject.Inject

class AndroidDensity @Inject constructor(context: Context) : Density {

    private val density = context.resources.displayMetrics.density

    override fun getDensity(): String = when (density) {
        1.0f -> "mdpi"
        1.5f -> "hdpi"
        2.0f -> "xhdpi"
        3.0f -> "xxhdpi"
        4.0f -> "xxxhdpi"
        else -> "xxxhdpi"
    }

}
