package com.oopsi.internal.di

import com.artjoker.data.internal.di.ApolloModule
import com.oopsi.Application
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            ImplementationsModule::class,
            ContributorsModule::class,
            CiceroneModule::class,
            AndroidInjectionModule::class,
            AndroidSupportInjectionModule::class,
            SchedulersModule::class,
            ApolloModule::class
        ]
)
interface AppComponent : AndroidInjector<Application> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<Application>()
}
