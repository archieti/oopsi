package com.oopsi.internal.di

import android.content.Context
import com.artjoker.core.BaseUseCase.SchedulerType.WORK
import com.artjoker.core.BaseUseCase.SchedulerType.WORK_RESULT
import com.artjoker.data.ocr.OpticalCharacterRecognition
import com.artjoker.data.ocr.OpticalCharacterRecognizer
import com.artjoker.data.ocr.preprocessing.BitmapPreprocessor
import com.artjoker.data.ocr.preprocessing.ImagePreprocessor
import com.artjoker.data.repositories.*
import com.artjoker.data.storage.HawkStorage
import com.artjoker.data.storage.LangStorageImpl
import com.artjoker.repositories.*
import com.oopsi.AndroidDensity
import com.oopsi.Application
import com.oopsi.ui.about.AboutOnBoardingActivity
import com.oopsi.ui.comments.add.AddCommentActivity
import com.oopsi.ui.comments.reply.ReplyCommentActivity
import com.oopsi.ui.content.PrivacyPolicyActivity
import com.oopsi.ui.content.TermsActivity
import com.oopsi.ui.devices.device.DeviceActivity
import com.oopsi.ui.devices.doc.SendUserManualActivity
import com.oopsi.ui.devices.favourites.DevicesFragment
import com.oopsi.ui.devices.gallery.GalleryActivity
import com.oopsi.ui.devices.history.DeviceHistoryActivity
import com.oopsi.ui.filters.FiltersActivity
import com.oopsi.ui.help.HelpActivity
import com.oopsi.ui.help.faq.FaqActivity
import com.oopsi.ui.languages.LanguagesActivity
import com.oopsi.ui.main.MainActivity
import com.oopsi.ui.onboarding.OnBoardingActivity
import com.oopsi.ui.scanner.camera.ScannerCameraActivity
import com.oopsi.ui.scanner.label.LabelPreviewActivity
import com.oopsi.ui.scanner.maybe.MaybeDevicesActivity
import com.oopsi.ui.scanner.preview.ScannerPreviewActivity
import com.oopsi.ui.scanner.search.SearchDeviceActivity
import com.oopsi.ui.scanner.search.brand.BrandSearchActivity
import com.oopsi.ui.services.all.ServicesFragment
import com.oopsi.ui.services.filtered.FilteredServicesActivity
import com.oopsi.ui.services.profile.ServiceDetailsActivity
import com.oopsi.ui.settings.SettingsActivity
import com.oopsi.ui.splash.SplashActivity
import com.oopsi.ui.stories.StoriesFragment
import com.oopsi.ui.stories.details.StoryDetailsActivity
import com.oopsi.ui.stories.preview.StoryImageActivity
import com.oopsi.ui.tips.TipsActivity
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton


@Module
abstract class ImplementationsModule {

    @Binds
    abstract fun context(application: Application): Context

    @Binds
    abstract fun scanningRepository(scannerRepository: ScannerRepository): ScanningRepository

    @Binds
    abstract fun devicesRepository(graphDevicesRepository: GraphDevicesRepository): DeviceRepository

    @Binds
    abstract fun brandsRepository(brandsRepository: GraphBrandsRepository): com.artjoker.repositories.BrandsRepository

    @Binds
    abstract fun ocr(textRecognizer: OpticalCharacterRecognizer): OpticalCharacterRecognition

    @Binds
    abstract fun servicesRepository(brandsRepository: GraphServicesRepository): ServicesRepository

    @Binds
    abstract fun storiesRepository(storiesRepository: GraphStoriesRepository): StoriesRepository

    @Binds
    abstract fun density(androidDensity: AndroidDensity): Density

    @Binds
    abstract fun documentRepository(documentRepository: GraphDocumentationRepository): DocumentationRepository

    @Binds
    abstract fun hawkStorage(localStorage: HawkStorage): LocalStorage

    @Binds
    abstract fun location(currentLocationRepository: CurrentLocationRepository): LocationRepository

    @Binds
    abstract fun preprocessing(bitmapPreprocessor: BitmapPreprocessor): ImagePreprocessor

    @Binds
    abstract fun langStorage(langStorage: LangStorageImpl): LangStorage
}

@Module
class CiceroneModule {

    private val cicerone: Cicerone<Router> = Cicerone.create()

    @Singleton
    @Provides
    fun provideRouter(): Router = cicerone.router


    @Singleton
    @Provides
    fun provideNavigationHolder(): NavigatorHolder = cicerone.navigatorHolder

    @Singleton
    @Provides
    fun provideCicerone() = cicerone
}


@Module
interface ContributorsModule {

    @ContributesAndroidInjector
    fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun contributeDeviceActivity(): DeviceActivity

    @ContributesAndroidInjector
    fun contributeScannerCameraActivity(): ScannerCameraActivity

    @ContributesAndroidInjector
    fun contributeScannerPreviewActivity(): ScannerPreviewActivity

    @ContributesAndroidInjector
    fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    fun contributeGalleryActivity(): GalleryActivity

    @ContributesAndroidInjector
    fun contributeUserManualActivity(): SendUserManualActivity

    @ContributesAndroidInjector
    fun contributeTipsActivity(): TipsActivity

    @ContributesAndroidInjector
    fun contributeSearchActivity(): BrandSearchActivity

    @ContributesAndroidInjector
    fun contributeOnBoardingActivity(): OnBoardingActivity

    @ContributesAndroidInjector
    fun contributeStoriesFragment(): StoriesFragment

    @ContributesAndroidInjector
    fun contributeServicesFragment(): ServicesFragment

    @ContributesAndroidInjector
    fun contributeDevicesFragment(): DevicesFragment

    @ContributesAndroidInjector
    fun contributeStoryDetailsActivity(): StoryDetailsActivity

    @ContributesAndroidInjector
    fun contributeDeviceServicesActivity(): FilteredServicesActivity

    @ContributesAndroidInjector
    fun contributeServiceDetailsActivity(): ServiceDetailsActivity

    @ContributesAndroidInjector
    fun contributeAddCommentActivity(): AddCommentActivity

    @ContributesAndroidInjector
    fun contributeReplyCommentActivity(): ReplyCommentActivity

    @ContributesAndroidInjector
    fun contributeFiltersActivity(): FiltersActivity

    @ContributesAndroidInjector
    fun contributeSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector
    fun contributeLanguagesActivity(): LanguagesActivity

    @ContributesAndroidInjector
    fun contributeHelpActivity(): HelpActivity

    @ContributesAndroidInjector
    fun contributeFaqActivity(): FaqActivity

    @ContributesAndroidInjector
    fun contributePreviewActivity(): StoryImageActivity

    @ContributesAndroidInjector
    fun contributeLabelPreviewActivity(): LabelPreviewActivity

    @ContributesAndroidInjector
    fun contributeDeviceHistoryActivity(): DeviceHistoryActivity

    @ContributesAndroidInjector
    fun contributeTermsActivity(): TermsActivity

    @ContributesAndroidInjector
    fun contributePrivacyActivity(): PrivacyPolicyActivity

    @ContributesAndroidInjector
    fun searchDeviceActivity(): SearchDeviceActivity

    @ContributesAndroidInjector
    fun maybeDevicesActivity(): MaybeDevicesActivity

    @ContributesAndroidInjector
    fun aboutOnBoardingActivity(): AboutOnBoardingActivity

}


@Module
class SchedulersModule {

    @Provides
    @Singleton
    internal fun appExecutionSchedulers() = mapOf(
            WORK to Schedulers.io(), WORK_RESULT to AndroidSchedulers.mainThread()
    )
}
