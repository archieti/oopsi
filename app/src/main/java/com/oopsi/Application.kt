package com.oopsi

import android.content.Context
import com.crashlytics.android.Crashlytics
import com.oopsi.internal.Logger
import com.oopsi.internal.di.DaggerAppComponent
import com.oopsi.ui.base.BaseActivity
import com.oopsi.utils.LocaleSubstituter
import com.orhanobut.hawk.Hawk
import com.tspoon.traceur.Traceur
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.fabric.sdk.android.Fabric


class Application : DaggerApplication() {
    private val activityStackState = mutableMapOf<Class<out BaseActivity>, Int>()

    override fun onCreate() {
        super.onCreate()
        Traceur.enableLogging()
        Fabric.with(this, Crashlytics())
        Hawk.init(this)
                .setLogInterceptor {
                    if (it.contains("Converter failed")) {
                        Logger.e(this, "Hawk issues: ${it}")
                        //throw IllegalStateException("Hawk issues: ${it}")
                    }
                }
                .build()
    }

    override fun applicationInjector(): AndroidInjector<Application> =
            DaggerAppComponent.builder().create(this)

    fun addToStackCount(activity: Class<out BaseActivity>) {
        val count = activityStackState[activity]
        activityStackState[activity] = 1.takeIf { count == null } ?: count!!.plus(1)
    }

    fun removeFromStackCount(activity: Class<out BaseActivity>) {
        val count = activityStackState[activity]
        activityStackState[activity] = 0.takeIf { count == null } ?: count!!.minus(1)
    }

    fun getCountOf(activity: Class<out BaseActivity>) = activityStackState[activity] ?: 0

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleSubstituter.substitute(newBase))
    }
}
