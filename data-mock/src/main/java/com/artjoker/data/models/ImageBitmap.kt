package com.artjoker.data.models

import android.graphics.Bitmap

import com.artjoker.core.models.Image

class ImageBitmap(val bitmap: Bitmap, override val url: String) : Image
