package com.artjoker.data.errors

import com.artjoker.rx.RxBus
import io.reactivex.Observable


/**
 * Created by dev on 30.01.18.
 */

object DefaultErrorHandler : HttpErrorHandler {

    private val errorRxBus = RxBus<Throwable>()

    override fun handleError(throwable: Throwable) {

    }

    override fun onErrorHandled(): Observable<Throwable> {
        return errorRxBus.toObservable()
    }
}
