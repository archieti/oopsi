package com.artjoker.data.repositories

import android.content.Context
import com.artjoker.models.Device
import com.artjoker.repositories.DocumentationRepository
import io.reactivex.Single
import javax.inject.Inject


class GraphDocumentationRepository @Inject constructor(var context: Context) : DocumentationRepository {

    override fun sendToEmail(device: Device, email: String): Single<Boolean> = Single.just(true)

    override fun downloadFor(device: Device): Single<List<String>> {
        return Single.just(mutableListOf())

    }
}
