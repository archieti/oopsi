package com.artjoker.data.repositories;

import com.artjoker.models.Brand;
import com.artjoker.models.Filter;
import com.artjoker.models.Location;
import com.artjoker.models.Service;
import com.artjoker.repositories.ServicesRepository;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GraphServicesRepository implements ServicesRepository {
    @Inject
    public GraphServicesRepository() {
        super();
    }
    private Service service;

    @Override
    public Single<List<Service>> getNearbyServices(Location filter) {
        return Single.just(mockNearbyServices());
    }

    private List<Service> mockNearbyServices() {
        ArrayList<Service> services = new ArrayList<>();
        services.add(new Service("Samsung certified centre", "Saint John street 67", "860m"));
        services.add(new Service("Sony service", "St.Mary Blvd 13 Apt.3", "2km"));
        services.add(new Service("Phillips service", "13 street Bld. 15 Apt. 4", "4km"));
        services.add(new Service("Kitched aid service", "Rotschield square 5, Bld.5", "5.2km"));
        return services;
    }



    @NotNull
    @Override
    public Single<List<Service>> findServicesByBrand(@NotNull Brand params, @NotNull Location location) {
        return Single.just(mockNearbyServices());
    }


    @NotNull
    @Override
    public Service getSelectedService() {
        return service;
    }

    @Override
    public void setSelectedService(@NotNull Service service) {
        this.service = service;
    }

    @NotNull
    @Override
    public Single<List<Service>> findServicesByFilters(@NotNull Filter params, @NotNull Location location) {
        return Single.just(mockNearbyServices());
    }

    @NotNull
    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public void setFilter(@NotNull Filter filter) {

    }
}
