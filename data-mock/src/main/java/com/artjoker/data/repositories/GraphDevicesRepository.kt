package com.artjoker.data.repositories

import com.artjoker.core.models.OcrDevice
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GraphDevicesRepository @Inject constructor() : DeviceRepository {

    override fun putToHistory(device: Device) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getHistory(): MutableList<Device> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override var device: Device = Device()

    override fun getFavouriteDevices(): List<Device> = listOf(Device(UUID.randomUUID().toString()))

    override fun toggleFavourite(device: Device): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isFavourite(device: Device): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeFromHistory(device: Device) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun searchDevices(model: String, brand: String): Single<List<Device>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findDevice(text: String): Single<OcrDevice> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDevicesBy(ocrStr: String): Single<List<Device>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}