package com.artjoker.data.repositories

import com.artjoker.models.Location
import com.artjoker.repositories.LocationRepository
import io.reactivex.Observable
import javax.inject.Inject

class CurrentLocationRepository @Inject
constructor() : LocationRepository {
    override fun putLocation(location: Location) {

    }

    override fun getLocation(): Observable<Location> = Observable.just(Location(30.0, 30.0))

}
