package com.artjoker.data.repositories

import com.artjoker.models.Brand
import com.artjoker.repositories.BrandsRepository
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class GraphBrandsRepository @Inject constructor() : BrandsRepository {
    override fun getBrands(): Observable<List<Brand>> {
        val brand = Brand(UUID.randomUUID().toString(), "OnePlus")
        brand.apply {
            imageUrl = "https://i0.wp.com/laowai.ru/wp-content/uploads/2015/07/oneplus-logo-big1.png?fit=810%2C207&ssl=1"
        }
        val brand1 = Brand(UUID.randomUUID().toString(), "Samsung")
        brand1.apply {

            imageUrl = "https://media.ao.com/uk/brandPages/samsung-tv/amends/img/gb/samsungblue-logo_2x.png"
        }
        val brand2 = Brand(UUID.randomUUID().toString(), "Google")
        brand2.apply {
            imageUrl = "https://cdn.vox-cdn.com/thumbor/Pkmq1nm3skO0-j693JTMd7RL0Zk=/0x0:2012x1341/1200x800/filters:focal(0x0:2012x1341)/cdn.vox-cdn.com/uploads/chorus_image/image/47070706/google2.0.0.jpg"
        }
        val brand3 = Brand(UUID.randomUUID().toString(), "Lenovo")
        brand3.apply {
            imageUrl = "http://www.pngmart.com/files/4/Lenovo-Logo-Transparent-PNG.png"
        }
        val brand4 = Brand(UUID.randomUUID().toString(), "HTC")
        brand4.apply {
            imageUrl = "https://i0.wp.com/www.keycomm.com.au/wp-content/uploads/2014/06/htc-logo-778px.png?ssl=1"
        }
        val brand5 = Brand(UUID.randomUUID().toString(), "Apple")
        brand5.apply {
            imageUrl = "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg"
        }
        val brand6 = Brand(UUID.randomUUID().toString(), "LG")
        brand6.apply {
            imageUrl = "http://www.hako.co.uk/wp-content/uploads/2015/10/LG-logo-high-res.jpg"
        }
        val brand7 = Brand(UUID.randomUUID().toString(), "Xiaomi")
        brand7.apply {
            imageUrl = "https://ubuykw.s3.amazonaws.com/ubuy/wysiwyg/xiaomi.png.png"
        }
        val list = listOf(brand, brand1, brand2, brand3, brand4, brand5, brand6, brand7)
        var resultList = ArrayList<Brand>()

        return Observable.just(resultList)
    }
}