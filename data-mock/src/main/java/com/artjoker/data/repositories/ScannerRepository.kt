package com.artjoker.data.repositories

import android.util.Log
import com.artjoker.core.models.Image
import com.artjoker.data.models.ImageBitmap
import com.artjoker.data.ocr.OpticalCharacterRecognition
import com.artjoker.repositories.ScanningRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScannerRepository @Inject
constructor(private val ocr: OpticalCharacterRecognition) : ScanningRepository {

    override fun saveToGallery(image: Image, id: String): String = ""

    override fun recognize(image: Image): Single<String> {
        val imageBitmap = image as ImageBitmap
        return ocr.recognize(imageBitmap.bitmap).map { toString(it) }
    }

    private var image: Image? = null

    override fun put(image: Image) {
        Log.e("---", "clear on put")
        clear()
        this.image = image
    }

    override fun clear() {

    }

    override fun getImage(): Image? {
        return image
    }

    private fun toString(it: List<String>): String {
        val stringBuilder = StringBuilder()
        it.forEach {
            stringBuilder.append(it)
            stringBuilder.append("\n")
        }

        return stringBuilder.toString()
    }

    override fun getFromGallery(url: String): Image? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
