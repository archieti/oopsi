package com.artjoker.data.ocr;

import android.graphics.Bitmap;

import java.util.List;

import io.reactivex.Single;

public interface OpticalCharacterRecognition {

    Single<List<String>> recognize(Bitmap bitmap);

}
