package com.artjoker.data.ocr.preprocessing.effects

import android.graphics.*

class NegativeEffect(private val sourceBitmap: Bitmap) : ImageEffect {

    override fun apply(): Bitmap = Bitmap.createBitmap(sourceBitmap.width, sourceBitmap.height, Bitmap.Config.ARGB_8888)
            .apply {
                val canvas = Canvas(this)
                val matrix = ColorMatrix()
                matrix.setSaturation(0f)
                val paint = Paint()
                paint.colorFilter = ColorMatrixColorFilter(matrix)
                canvas.drawBitmap(sourceBitmap, 0f, 0f, paint)
                paint.colorFilter = ColorMatrixColorFilter(NEGATIVE_COLOR_INVERSION)
                canvas.drawBitmap(this, 0f, 0f, paint)
            }

    private val NEGATIVE_COLOR_INVERSION = floatArrayOf(
            -1.0f, 0f, 0f, 0f, 255f, // red
            0f, -1.0f, 0f, 0f, 255f, // green
            0f, 0f, -1.0f, 0f, 255f, // blue
            0f, 0f, 0f, 1.0f, 0f  // alpha
    )
}