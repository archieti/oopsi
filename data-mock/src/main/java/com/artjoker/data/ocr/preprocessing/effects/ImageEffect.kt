package com.artjoker.data.ocr.preprocessing.effects

import android.graphics.Bitmap

interface ImageEffect {
    fun apply(): Bitmap
}