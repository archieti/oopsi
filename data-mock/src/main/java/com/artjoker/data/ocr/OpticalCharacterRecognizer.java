package com.artjoker.data.ocr;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public class OpticalCharacterRecognizer implements OpticalCharacterRecognition {
    @Inject
    public OpticalCharacterRecognizer(Context context) {
        super();
    }


    @Override
    public Single<List<String>> recognize(Bitmap bitmap) {
        List<String> strings = new ArrayList<>();
        strings.add("Samsung");
        Observable<List<String>> just = Observable.just(strings);
        return Single.fromObservable(just);
    }
}
