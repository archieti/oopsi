package com.artjoker.data.ocr.preprocessing.effects;

import android.graphics.Bitmap;
import android.graphics.Color;

import org.jetbrains.annotations.NotNull;

public class NoiseEffect implements ImageEffect {

    private Bitmap bitmap;

    public NoiseEffect(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    // Remove Noise
    @NotNull
    @Override
    public Bitmap apply() {
        for (int x = 0; x < bitmap.getWidth(); x++) {
            for (int y = 0; y < bitmap.getHeight(); y++) {
                int pixel = bitmap.getPixel(x, y);
                if (Color.red(pixel) < 162 && Color.green(pixel) < 162 && Color.blue(pixel) < 162) {
                    bitmap.setPixel(x, y, Color.BLACK);
                }
            }
        }
        for (int x = 0; x < bitmap.getWidth(); x++) {
            for (int y = 0; y < bitmap.getHeight(); y++) {
                int pixel = bitmap.getPixel(x, y);
                if (Color.red(pixel) > 162 && Color.green(pixel) > 162 && Color.blue(pixel) > 162) {
                    bitmap.setPixel(x, y, Color.WHITE);
                }
            }
        }
        return bitmap;
    }
}
