package com.artjoker.data.ocr.preprocessing

import android.graphics.Bitmap

interface ImagePreprocessor {
    fun negative(sourceBitmap: Bitmap): Bitmap
    fun monochrome(sourceBitmap: Bitmap): Bitmap
}