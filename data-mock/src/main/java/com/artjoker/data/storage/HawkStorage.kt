package com.artjoker.data.storage

import com.artjoker.models.*
import com.artjoker.repositories.LocalStorage
import com.artjoker.repositories.LocalStorage.Companion.FAVOURITES_KEY
import com.orhanobut.hawk.Hawk.get
import com.orhanobut.hawk.Hawk.put
import javax.inject.Inject


class HawkStorage @Inject constructor() : LocalStorage {
    override var settings: Settings by StorageProperty(Settings())
    override var filter: Filter by StorageProperty(Filter())
    override var reply: Comment by StorageProperty(Comment("", "", "", ""))
    override var selectedStory: Story by StorageProperty(Story(id = "", storyHtmlText = "", title = ""))
    override var selectedService: Service by StorageProperty(Service())
    override var device: Device by StorageProperty(Device())
    override var language: Language by StorageProperty(Language())

    override fun saveFavourites(devices: MutableSet<Device>) = put(FAVOURITES_KEY, devices)

    override fun getFavourites(): MutableSet<Device> = get(FAVOURITES_KEY, mutableSetOf())

    override fun saveHistory(history: MutableList<Device>) {
        put(LocalStorage.HISTORY_KEY, history)
    }

    override fun getHistory(): MutableList<Device> = get(LocalStorage.HISTORY_KEY, mutableListOf())
}