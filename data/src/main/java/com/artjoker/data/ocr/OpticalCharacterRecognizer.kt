package com.artjoker.data.ocr

import android.content.Context
import android.graphics.Bitmap
import android.util.SparseArray
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class OpticalCharacterRecognizer @Inject
constructor(private val context: Context) : OpticalCharacterRecognition {

    override fun recognize(bitmap: Bitmap): Single<List<String>> = Single.fromObservable(Observable.just(toFrame(bitmap))
            .map { ocr().detect(it) }
            .map { toList(it) }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()))


    private fun toList(textBlockSparseArray: SparseArray<TextBlock>): List<String> {
        val result = ArrayList<String>()
        for (i in 0 until textBlockSparseArray.size()) {
            val textBlock = textBlockSparseArray.get(textBlockSparseArray.keyAt(i))
            result.add(textBlock.value)
        }
        return result
    }

    private fun toFrame(bitmap: Bitmap): Frame = Frame.Builder()
            .setBitmap(bitmap)
            .build()

    private fun ocr(): TextRecognizer = TextRecognizer.Builder(context).build()

}
