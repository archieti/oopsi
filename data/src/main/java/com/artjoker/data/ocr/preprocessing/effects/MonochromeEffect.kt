package com.artjoker.data.ocr.preprocessing.effects

import android.graphics.*

class MonochromeEffect(private val sourceBitmap: Bitmap) : ImageEffect {
    override fun apply(): Bitmap = Bitmap.createBitmap(sourceBitmap.width, sourceBitmap.height, Bitmap.Config.ARGB_8888)
            .apply {
                val canvas = Canvas(this)
                val matrix = ColorMatrix()
                matrix.setSaturation(0f)
                val paint = Paint()
                paint.colorFilter = ColorMatrixColorFilter(matrix)
                canvas.drawBitmap(sourceBitmap, 0f, 0f, paint)
                paint.colorFilter = ColorMatrixColorFilter(createThresholdMatrix(128))
                canvas.drawBitmap(this, 0f, 0f, paint)
            }

    private fun createThresholdMatrix(threshold: Int) = ColorMatrix(
            floatArrayOf(
                    85f, 85f, 85f, 0f, -255f * threshold,
                    85f, 85f, 85f, 0f, -255f * threshold,
                    85f, 85f, 85f, 0f, -255f * threshold,
                    0f, 0f, 0f, 1f, 0f
            )
    )
}
