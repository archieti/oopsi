package com.artjoker.data.ocr.preprocessing.effects;

import android.graphics.Bitmap;
import android.graphics.Color;

import org.jetbrains.annotations.NotNull;

public class GreyScaleEffect implements ImageEffect {
    private Bitmap bitmap;

    public GreyScaleEffect(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    //So slow
    @NotNull
    @Override
    public Bitmap apply() {
        Bitmap bmap = bitmap.copy(bitmap.getConfig(), true);
        int c;
        for (int i = 0; i < bmap.getWidth(); i++) {
            for (int j = 0; j < bmap.getHeight(); j++) {
                c = bmap.getPixel(i, j);
                byte gray = (byte) (.299 * Color.red(c) + .587 * Color.green(c)
                        + .114 * Color.blue(c));

                bmap.setPixel(i, j, Color.argb(255, gray, gray, gray));
            }
        }
        return bmap;
    }
}
