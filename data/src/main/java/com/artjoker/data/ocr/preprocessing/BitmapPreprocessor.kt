package com.artjoker.data.ocr.preprocessing

import android.graphics.Bitmap
import com.artjoker.data.ocr.preprocessing.effects.MonochromeEffect
import com.artjoker.data.ocr.preprocessing.effects.NegativeEffect
import javax.inject.Inject

class BitmapPreprocessor @Inject constructor() : ImagePreprocessor {

    override fun negative(sourceBitmap: Bitmap): Bitmap = NegativeEffect(sourceBitmap).apply()

    override fun monochrome(sourceBitmap: Bitmap): Bitmap = MonochromeEffect(sourceBitmap).apply()

}
