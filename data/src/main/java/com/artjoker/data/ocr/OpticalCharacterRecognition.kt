package com.artjoker.data.ocr

import android.graphics.Bitmap

import io.reactivex.Single

interface OpticalCharacterRecognition {

    fun recognize(bitmap: Bitmap): Single<List<String>>

}
