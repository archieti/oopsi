package com.artjoker.data.http.graphql

/**
 * Created by dev on 24.01.18.
 */

enum class GraphqlClientTypes {
    AUTHENTICATION, DATA
}
