package com.artjoker.data.http.graphql

import com.apollographql.apollo.ApolloClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import java.util.concurrent.TimeUnit


class ApolloBuilder(private val url: String) {

    @JvmOverloads
    fun create(interceptor: Interceptor? = null): ApolloClient {
        val builder = OkHttpClient.Builder()
        interceptor?.let {
            builder.addNetworkInterceptor(interceptor)
        }

        val logging = okhttp3.logging.HttpLoggingInterceptor().apply {
            level = BODY
        }

        builder.addNetworkInterceptor(logging)
                .addInterceptor(logging)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        return ApolloClient.builder()
                .serverUrl(url)
                .okHttpClient(builder.build())
                .build()
    }

    companion object {
        const val TIMEOUT = 60L
    }
}
