package com.artjoker.data.internal.di

import android.artjoker.graphql_data.BuildConfig
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton


@Module
class ApolloModule {

    @Provides
    @Singleton
    @IntoMap
    @ApolloKeys(GraphqlClientTypes.DATA)
    fun provideApolloData() = ApolloBuilder(BuildConfig.GRAPH_QL_DATA_HOST)

    @Provides
    @Singleton
    @IntoMap
    @ApolloKeys(GraphqlClientTypes.AUTHENTICATION)
    fun provideApolloAuth() = ApolloBuilder("")
}
