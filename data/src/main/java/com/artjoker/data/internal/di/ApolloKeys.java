package com.artjoker.data.internal.di;

import com.artjoker.data.http.graphql.GraphqlClientTypes;

import dagger.MapKey;

@MapKey
public @interface ApolloKeys {
    GraphqlClientTypes value();
}
