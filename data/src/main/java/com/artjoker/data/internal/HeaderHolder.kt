package com.artjoker.data.internal

import com.orhanobut.hawk.Hawk

enum class HeaderHolder {

    Authorization;

    var header: String
        inline get() = Hawk.get<String>(name, "")
        inline set(value) {
            Hawk.put(name, value)
        }
}
