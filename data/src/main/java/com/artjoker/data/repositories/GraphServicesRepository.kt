package com.artjoker.data.repositories

import android.content.Context
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import com.artjoker.data.mappers.listOfServices
import com.artjoker.data.mappers.remoteDayIndex
import com.artjoker.data.repositories.base.AbstractRemoteRepository
import com.artjoker.models.*
import com.artjoker.repositories.Density
import com.artjoker.repositories.LocalStorage
import com.artjoker.repositories.ServicesRepository
import data.ServicesQuery
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GraphServicesRepository @Inject constructor(
        clients: Map<@JvmSuppressWildcards GraphqlClientTypes, @JvmSuppressWildcards ApolloBuilder>,
        density: Density,
        context: Context
) : AbstractRemoteRepository(
        clients[GraphqlClientTypes.DATA],
        context
), ServicesRepository {

    init {
        applyDensity(density)
    }

    // override var brandOfServices by LocalStorageProperty(LocalStorage::brandOfServices)

    override var selectedService by LocalStorageProperty(LocalStorage::selectedService)

    override var filter: Filter by LocalStorageProperty(LocalStorage::filter)

    override fun getNearbyServices(location: Location): Single<List<Service>> = Single.fromObservable(
            ServicesQuery.builder()
                    .lat(location.lat)
                    .lng(location.lng)
                    .build()
                    .let { query(it) }
                    .map { listOfServices.invoke(it.data()) }
    )


    override fun findServicesByBrand(params: Brand, location: Location): Single<List<Service>> = Single.fromObservable(
            ServicesQuery.builder()
                    .brand_id(params.id)
                    .lat(location.lat)
                    .lng(location.lng)
                    .build()
                    .let { query(it) }
                    .map { listOfServices.invoke(it.data()) }
    )

    override fun findServicesByFilters(params: Filter, location: Location): Single<List<Service>> {

        val builder = ServicesQuery.builder().lat(location.lat).lng(location.lng)

        params.radius?.value?.let {
            when (it) {
                0 -> RadiusFilter.MAX_DISTANCE
                else -> it
            }.times(1000).let(builder::radius)
        }

        params.brand?.value?.let {
            builder.brand_id(it.id)
        }

        params.workingDay?.value?.let {
            if (it.dayIndex() >= 0) {
                builder.work_day(remoteDayIndex(it))
            }
        }

        params.startTime?.value?.let {
            builder.work_from(getGTM0Millis(it))
        }

        params.endTime?.value?.let {
            builder.work_to(getGTM0Millis(it))
        }

        return Single.fromObservable(
                query(builder.build())
                        .map { listOfServices(it.data()) }
        )
    }


    private fun getGTM0Millis(date: Date): Int {
        val d = Date(date.time - date.timezoneOffset * 60 * 1000)
        return TimeUnit.MILLISECONDS.toSeconds(d.time).toInt()
    }
}
