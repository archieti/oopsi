package com.artjoker.data.repositories.base

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.TYPE_WIFI
import com.artjoker.repositories.LocalStorage
import javax.inject.Inject
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty

abstract class AbstractRepository {

    @Inject
    lateinit var storage: LocalStorage

    protected inner class LocalStorageProperty<T>(
            private val property: KMutableProperty1<LocalStorage, T>
    ) {

        operator fun getValue(thisRef: Any, p: KProperty<*>): T =
                property.get(storage)

        operator fun setValue(thisRef: Any, p: KProperty<*>, value: T) {
            property.set(storage, value)
        }
    }
}

@Suppress("DEPRECATION")
@SuppressLint("MissingPermission")
fun Context.isWifiConnected(): Boolean = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        .activeNetworkInfo?.type == TYPE_WIFI

