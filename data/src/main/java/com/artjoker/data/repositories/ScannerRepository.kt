package com.artjoker.data.repositories

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import com.artjoker.core.models.Image
import com.artjoker.data.models.ImageBitmap
import com.artjoker.data.ocr.OpticalCharacterRecognition
import com.artjoker.data.ocr.preprocessing.ImagePreprocessor
import com.artjoker.repositories.ScanningRepository
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import java.io.FileNotFoundException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ScannerRepository @Inject constructor(
        private val ocr: OpticalCharacterRecognition,
        private val imagePreprocessor: ImagePreprocessor,
        private val context: Context
) : ScanningRepository {

    private var image: Image? = null

    override fun put(image: Image) {
        clear()
        this.image = image
    }

    override fun clear() {
        this.image?.let {
            if (it is ImageBitmap) {
                it.bitmap.recycle()
                image = null
            }
        }
    }

    override fun getImage(): Image? {
        return image
    }

    override fun recognize(image: Image): Single<String> =
            Single.just(image as ImageBitmap)
                    .map { it.bitmap }
                    .flatMap {
                        Single.zip(
                                rawRecognition(it), //use raw image for high pixaleted images
                                preprocessedRecognition(it), //apply 2 monochrome filters for greyscaled images
                                BiFunction<String, String, String> { recognizedData, recognizedMonochromeData -> "$recognizedData \n $recognizedMonochromeData" }
                        )
                    }

    private fun preprocessedRecognition(it: Bitmap) =
            Single.just(monochrome(it))
                    .flatMap {
                        Single.zip(
                                ocr.recognize(it).map { toString(it) },
                                ocr.recognize(negative(it)).map { toString(it) },
                                BiFunction<String, String, String> { monochrome, inversed -> "$monochrome \n $inversed" }
                        )
                    }


    private fun rawRecognition(it: Bitmap) = ocr.recognize(it).map { toString(it) }

    private fun monochrome(sourceBitmap: Bitmap): Bitmap = imagePreprocessor.monochrome(sourceBitmap)

    private fun negative(sourceBitmap: Bitmap): Bitmap = imagePreprocessor.negative(sourceBitmap)

    private fun toString(it: List<String>): String {
        val stringBuilder = StringBuilder()
        it.forEach {
            stringBuilder.append(it)
            stringBuilder.append("\n")
        }
        return stringBuilder.toString()
    }

    override fun getFromGallery(url: String): Image? {
        var bitmap: Bitmap?
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, Uri.parse(url))
        } catch (e: FileNotFoundException) {
            val options = BitmapFactory.Options().apply {
                inPreferredConfig = Bitmap.Config.ARGB_8888
            }
            bitmap = BitmapFactory.decodeFile(url, options)
            if (bitmap == null) {
                return null
            }
        }
        return ImageBitmap(bitmap!!, url)
    }

    override fun saveToGallery(image: Image, id: String): String {
        val bitmap = image as ImageBitmap
        return saveImage(bitmap.bitmap, id)
    }

    private fun saveImage(finalBitmap: Bitmap, name: String): String {

        val fileName = "Image-$name.jpg"
        val url = MediaStore.Images.Media.insertImage(context.contentResolver, finalBitmap, fileName, "")
        val contentValues = ContentValues()
        contentValues.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
        context.contentResolver.update(Uri.parse(url), contentValues, "", arrayOf())

        return url
    }
}
