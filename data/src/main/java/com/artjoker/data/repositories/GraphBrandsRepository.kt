package com.artjoker.data.repositories

import android.content.Context
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import com.artjoker.data.mappers.brand
import com.artjoker.data.repositories.base.AbstractRemoteRepository
import com.artjoker.models.Brand
import com.artjoker.repositories.BrandsRepository
import com.artjoker.repositories.Density
import data.BrandsQuery
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GraphBrandsRepository @Inject constructor(
        clients: Map<@JvmSuppressWildcards GraphqlClientTypes, @JvmSuppressWildcards ApolloBuilder>,
        density: Density,
        context: Context
) : AbstractRemoteRepository(
        clients[GraphqlClientTypes.DATA],
        context
), BrandsRepository {
    init {
        applyDensity(density)
    }

    override fun getBrands(): Observable<List<Brand>> = BrandsQuery.builder()
            .build()
            .let { query(it, HttpCachePolicy.CACHE_FIRST) }
            .map { brand.asListMapper().invoke(it.data()?.Brands()) }
}