package com.artjoker.data.repositories

import android.content.Context
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import com.artjoker.data.mappers.listOfComments
import com.artjoker.data.mappers.listOfStories
import com.artjoker.data.repositories.base.AbstractRemoteRepository
import com.artjoker.models.Comment
import com.artjoker.models.Story
import com.artjoker.repositories.Density
import com.artjoker.repositories.LocalStorage
import com.artjoker.repositories.StoriesRepository
import data.AddCommentMutation
import data.CommentsOfStoryQuery
import data.ReplyMutation
import data.StoriesQuery
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GraphStoriesRepository @Inject constructor(
        clients: Map<@JvmSuppressWildcards GraphqlClientTypes, @JvmSuppressWildcards ApolloBuilder>,
        density: Density,
        context: Context
) : AbstractRemoteRepository(
        clients[GraphqlClientTypes.DATA],
        context
), StoriesRepository {


    override var reply: Comment by LocalStorageProperty(LocalStorage::reply)

    override var selectedStory: Story by LocalStorageProperty(LocalStorage::selectedStory)

    init {
        applyDensity(density)
    }

    override fun retrieveStories(): Single<List<Story>> = Single.fromObservable(
            StoriesQuery.builder()
                    .build()
                    .let { query(it) }
                    .map { listOfStories.invoke(it.data()) }
    )


    override fun loadComments(storyID: String): Single<List<Comment>> = Single.fromObservable(
            CommentsOfStoryQuery.builder()
                    .story_id(storyID)
                    .build()
                    .let { query(it) }
                    .map { listOfComments(it.data()) }
                    .map { it.map { it.copy(storyId = storyID) } }
    )


    override fun add(comment: Comment): Completable = Completable.fromObservable(
            AddCommentMutation.builder()
                    .author(comment.authorName)
                    .story_id(comment.storyId ?: "")
                    .text(comment.text)
                    .build()
                    .let { mutation(it) }
    )

    override fun reply(new: Comment, parent: Comment): Completable = Completable.fromObservable(
            ReplyMutation.builder()
                    .author(new.authorName)
                    .text(new.text)
                    .story_id(new.storyId ?: "")
                    .parentCommentId(parent.id)
                    .build()
                    .let { mutation(it) }
    )
}
