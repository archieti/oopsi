package com.artjoker.data.repositories.base;

import android.content.Context;

import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Mutation;
import com.apollographql.apollo.api.Query;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.api.cache.http.HttpCachePolicy;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.artjoker.data.errors.DefaultErrorHandler;
import com.artjoker.data.errors.HttpErrorHandler;
import com.artjoker.data.errors.RxErrorHandlers;
import com.artjoker.data.http.graphql.ApolloBuilder;
import com.artjoker.models.Settings;
import com.artjoker.repositories.Density;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.reactivex.Observable;
import okhttp3.Interceptor;
import okhttp3.Request;

import static com.artjoker.data.repositories.base.AbstractRepositoryKt.isWifiConnected;


public abstract class AbstractRemoteRepository extends AbstractRepository {

    private ApolloBuilder apollo;
    private Context context;
    private HttpErrorHandler errorHandler = DefaultErrorHandler.INSTANCE;
    private ApolloClient client;
    private Map<String, String> headers = new HashMap<>();

    public AbstractRemoteRepository(
            ApolloBuilder client,
            @Nullable HttpErrorHandler errorHandler,
            Context context
    ) {
        apollo = client;
        this.context = context;
        if (errorHandler != null)
            this.errorHandler = errorHandler;
    }

    public AbstractRemoteRepository(ApolloBuilder client, Context context) {
        this(client, null, context);
    }

    public void applyDensity(Density density) {
        headers.put("Image-Density", density.getDensity());
    }

    private Interceptor interceptor(Map<String, String> headers) {
        return chain -> {
            Request.Builder builder = chain.request().newBuilder();
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder.header(entry.getKey(), entry.getValue());
            }
            return chain.proceed(builder.build());
        };
    }

    protected <D extends Mutation.Data, T, V extends Mutation.Variables> Observable<Response<T>> mutation(
            @Nonnull Mutation<D, T, V> mutation
    ) {
        return Rx2Apollo.from(apollo.create(interceptor(headers)).mutate(mutation))
                .compose(RxErrorHandlers.applyHttpErrors(errorHandler));
    }

    protected <D extends Query.Data, T, V extends Query.Variables> Observable<Response<T>> query(
            @Nonnull Query<D, T, V> query
    ) {
        return query(query, HttpCachePolicy.NETWORK_ONLY);
    }

    protected <D extends Query.Data, T, V extends Query.Variables> Observable<Response<T>> query(
            @Nonnull Query<D, T, V> query,
            @Nonnull HttpCachePolicy.Policy cachePolicy
    ) {
        if (client == null) {
            client = this.apollo.create(interceptor(headers));
        }
        return query(client, query, cachePolicy);
    }


    private <D extends Query.Data, T, V extends Query.Variables> Observable<Response<T>> query(
            @Nonnull ApolloClient apollo,
            @Nonnull Query<D, T, V> query,
            @Nonnull HttpCachePolicy.Policy cachePolicy
    ) {
        if (isOnlyWifiAllowed() && !isWifiConnected(context)) {
            return Observable.error(new Throwable("No connection. Only wifi allowed"));
        } else
            return Rx2Apollo.from(apollo.query(query).httpCachePolicy(cachePolicy))
                    .compose(RxErrorHandlers.applyHttpErrors(errorHandler));
    }

    private boolean isOnlyWifiAllowed() {
        return storage.getSettings().getNetworkState().ordinal() == Settings.NetworkState.WIFI_ONLY.ordinal();
    }
}
