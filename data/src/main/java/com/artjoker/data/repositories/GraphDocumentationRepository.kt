package com.artjoker.data.repositories

import android.content.Context
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import com.artjoker.data.repositories.base.AbstractRemoteRepository
import com.artjoker.models.Device
import com.artjoker.repositories.DocumentationRepository
import com.esafirm.rxdownloader.RxDownloader
import data.DocToEmailMutation
import io.reactivex.Observable

import io.reactivex.Single
import javax.inject.Inject


class GraphDocumentationRepository @Inject constructor(
        var context: Context,
        clients: Map<@JvmSuppressWildcards GraphqlClientTypes, @JvmSuppressWildcards ApolloBuilder>
) : AbstractRemoteRepository(
        clients[GraphqlClientTypes.DATA],
        context
), DocumentationRepository {

    override fun downloadFor(device: Device): Single<List<String>> = Observable.just(device)
            .map { it.files }
            .flatMap { Observable.fromIterable(it) }
            .flatMap { RxDownloader(context).download(it.url, it.name, true) }
            .toList()


    override fun sendToEmail(device: Device, email: String): Single<Boolean> = Single.fromObservable(
            DocToEmailMutation.builder()
                    .email(email)
                    .id(device.id)
                    .build()
                    .let { mutation(it) }
                    .map { it.data()?.SendDeviceFileOnEmail() }
    )

}
