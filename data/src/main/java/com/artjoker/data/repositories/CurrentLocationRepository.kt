package com.artjoker.data.repositories

import com.artjoker.models.Location
import com.artjoker.repositories.LocationRepository
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrentLocationRepository @Inject constructor() : LocationRepository {

    private val lastLocation = BehaviorSubject.create<Location>()

    override fun putLocation(location: Location) {
        lastLocation.onNext(location)
    }

    override fun getLocation(): Observable<Location> = lastLocation
}
