package com.artjoker.data.repositories

import android.content.Context
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.artjoker.core.models.OcrDevice
import com.artjoker.data.http.graphql.ApolloBuilder
import com.artjoker.data.http.graphql.GraphqlClientTypes
import com.artjoker.data.mappers.additionalDevicesMapper
import com.artjoker.data.mappers.deviceDetailsOcr
import com.artjoker.data.mappers.devices
import com.artjoker.data.repositories.base.AbstractRemoteRepository
import com.artjoker.events.AppEvents
import com.artjoker.interactors.devices.DevicesNotFoundException
import com.artjoker.models.Device
import com.artjoker.repositories.Density
import com.artjoker.repositories.DeviceRepository
import com.artjoker.repositories.LocalStorage
import data.AdditionalSearchQuery
import data.DevicesQuery
import data.OcrSearchQuery
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GraphDevicesRepository @Inject constructor(
        clients: Map<@JvmSuppressWildcards GraphqlClientTypes, @JvmSuppressWildcards ApolloBuilder>,
        density: Density,
        context: Context,
        val appEvents: AppEvents
) : AbstractRemoteRepository(clients[GraphqlClientTypes.DATA], context), DeviceRepository {

    init {
        applyDensity(density)
    }

    override var device by LocalStorageProperty(LocalStorage::device)

    override fun searchDevices(model: String, brand: String): Single<List<Device>> {
        val devicesQuery = DevicesQuery.builder()
                .model_name(model)
                .brand_name(brand)
                .build()

        return Single.fromObservable(query(devicesQuery, HttpCachePolicy.CACHE_FIRST))
                .flatMap {
                    when (it.data()) {
                        null -> Single.error { DevicesNotFoundException() }
                        else -> Single.just(it.data()!!.Devices().orEmpty())
                    }
                }
                .map { devices.asListMapper().invoke(it) }
    }

    override fun findDevice(text: String): Single<OcrDevice> =
            Single.fromObservable(
                    OcrSearchQuery.builder()
                            .text(text)
                            .build()
                            .let { query(it) }
            ).flatMap {
                when {
                    it.data() == null || it.data()?.OCRSearch() == null -> Single.error(Throwable("Device not found"))
                    else -> Single.just(it.data()?.OCRSearch())
                }
            }.map { OcrDevice(deviceDetailsOcr(it), text) }


    override fun getFavouriteDevices(): List<Device> = storage.getFavourites().toList()

    override fun isFavourite(device: Device) = storage.getFavourites().find { it -> it.id == device.id } != null

    override fun toggleFavourite(device: Device): Boolean {
        val favourite = isFavourite(device)
        when {
            favourite -> removeFromFavourites(device)
            else -> addToFavourites(device)
        }
        val newFavourite = !favourite
        appEvents.notifyListeners { it.onFavouritesChanged(device, newFavourite) }
        return newFavourite
    }

    override fun putToHistory(device: Device) {
        val newHistory = storage.getHistory()
        newHistory.forEachIndexed { pos, element ->
            if (element.id == device.id) {
                newHistory.removeAt(pos)
                newHistory.add(newHistory.size, device)
                storage.saveHistory(newHistory)
                return
            }
        }

        newHistory.add(device)
        storage.saveHistory(newHistory)
    }

    override fun removeFromHistory(device: Device) {
        val history = storage.getHistory().apply {
            val element = find { it.id == device.id }
            remove(element)
        }
        storage.saveHistory(history)
    }

    override fun getHistory(): MutableList<Device> = storage.getHistory()

    override fun getDevicesBy(ocrStr: String): Single<List<Device>> {
        val additionalSearchQuery = AdditionalSearchQuery.builder()
                .text(ocrStr)
                .build()

        return Single.fromObservable(query(additionalSearchQuery))
                .map { it.data()?.AdditionalSearch() }
                .map { additionalDevicesMapper.asListMapper().invoke(it) }
    }

    private fun addToFavourites(device: Device) {
        storage.getFavourites()
                .apply { add(device) }
                .let { storage.saveFavourites(it) }
    }

    private fun removeFromFavourites(device: Device) {
        storage.getFavourites()
                .apply { remove(find { it.id == device.id }) }
                .let { storage.saveFavourites(it) }
    }
}
