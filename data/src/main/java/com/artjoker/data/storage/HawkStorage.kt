package com.artjoker.data.storage

import android.content.Context
import com.artjoker.data.defaultLocale
import com.artjoker.models.*
import com.artjoker.repositories.LangStorage
import com.artjoker.repositories.LocalStorage
import com.artjoker.repositories.LocalStorage.Companion.FAVOURITES_KEY
import com.artjoker.repositories.LocalStorage.Companion.HISTORY_KEY
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk.get
import com.orhanobut.hawk.Hawk.put
import java.util.*
import javax.inject.Inject


class HawkStorage @Inject constructor() : LocalStorage {

    override var settings: Settings by StorageProperty(Settings())
    override var filter: Filter by StorageProperty(Filter())
    override var reply: Comment by StorageProperty(Comment("", "", "", ""))
    override var selectedStory: Story by StorageProperty(Story(id = "", storyHtmlText = "", title = ""))
    override var selectedService: Service by StorageProperty(Service())
    override var device: Device by StorageProperty(Device())

    override fun saveFavourites(devices: MutableSet<Device>) = put(FAVOURITES_KEY, devices)

    override fun getFavourites(): MutableSet<Device> = get(FAVOURITES_KEY, mutableSetOf())

    override fun saveHistory(history: MutableList<Device>) {
        put(HISTORY_KEY, history)
    }

    override fun getHistory(): MutableList<Device> = get(HISTORY_KEY, mutableListOf())
}

class LangStorageImpl @Inject constructor(context: Context) : LangStorage {

    private val name = "LangStorageImpl"
    private val key = "LANG_KEY"

    private val preferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    private val gson = Gson()

    override var language: Language
        get() {
            val defaultLocale = defaultLocale()
            val defLang = Language(locale = Locale(defaultLocale.language, defaultLocale.country))
            val defLangJson = gson.toJson(defLang)
            return gson.fromJson(preferences.getString(key, defLangJson), Language::class.java)
        }
        set(value) {
            val json = gson.toJson(value)
            preferences.edit().putString(key, json).apply()
        }
}