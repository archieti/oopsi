package com.artjoker.data.storage

import android.support.annotation.CallSuper
import com.orhanobut.hawk.Hawk
import kotlin.reflect.KProperty

open class StorageProperty<T : Any>(
        private val defaultValue: T? = null,
        private val onPropertyChanged: (T) -> Unit = {}
) {


    open operator fun getValue(thisRef: Any, property: KProperty<*>): T =
            Hawk.get<T>(property.name, defaultValue)

    @CallSuper
    open operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        Hawk.put(property.name, value)
        onPropertyChanged(value)
    }
}
