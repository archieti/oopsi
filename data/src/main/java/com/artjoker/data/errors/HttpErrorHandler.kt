package com.artjoker.data.errors

import io.reactivex.Observable

/**
 * Created by dev on 30.01.18.
 */

interface HttpErrorHandler {

    fun handleError(throwable: Throwable)

    fun onErrorHandled(): Observable<Throwable>
}
