package com.artjoker.data.errors

import com.artjoker.NoInternetException
import com.artjoker.TimeoutException
import com.artjoker.rx.RxBus
import io.reactivex.Observable
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by dev on 30.01.18.
 */

object DefaultErrorHandler : HttpErrorHandler {

    private val errorRxBus = RxBus<Throwable>()

    override fun handleError(throwable: Throwable) {
        when (throwable.cause) {
            is UnknownHostException -> errorRxBus.send(NoInternetException())
            is SocketTimeoutException -> errorRxBus.send(TimeoutException())
            else -> errorRxBus.send(throwable)
        }
    }

    override fun onErrorHandled(): Observable<Throwable> {
        return errorRxBus.toObservable()
    }
}
