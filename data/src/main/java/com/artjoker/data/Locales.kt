package com.artjoker.data

import java.util.*

enum class Locales(val language: String, val country: String) {
    ENGLISH("en", "GB"),
    FRENCH("fr", "FR"),
    LATVIAN("lv", "LV"),
    RUSSIAN("ru", "RU");
}

fun defaultLocale() = Locales.values().find { it.language == Locale.getDefault().language }
        ?: Locales.ENGLISH
