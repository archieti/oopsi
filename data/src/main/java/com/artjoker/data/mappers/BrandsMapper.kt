package com.artjoker.data.mappers

import com.artjoker.models.Brand
import com.smb.core.mappers.Transformer
import data.BrandsQuery
import data.fragment.BrandInfo


val brand = Transformer.build<BrandsQuery.Brand, Brand> {
    brandFragment(fragments().brandInfo())
}

val brandFragment = Transformer.build<BrandInfo, Brand> {
    Brand(id(), name()).apply {
        imageUrl = logo()
    }
}