package com.artjoker.data.mappers

import com.artjoker.models.Brand
import com.artjoker.models.Device
import com.artjoker.models.DeviceType
import com.artjoker.models.RemoteFile
import com.smb.core.mappers.Transformer
import data.AdditionalSearchQuery
import data.DevicesQuery
import data.OcrSearchQuery


val deviceDetailsOcr = Transformer.build<OcrSearchQuery.OCRSearch, Device> {

    val deviceDetails = fragments().deviceDetails()
    val deviceTypeInfo = type()?.fragments()?.deviceTypeInfo()
    val brandInfo = brand()?.fragments()?.brandInfo()
    val thumbsList = mutableListOf<String>()
    val originalList = mutableListOf<String>()
    val docs = mutableListOf<RemoteFile>()
    galleries()?.forEach {
        val imageInfo = it.fragments().imageInfo()
        imageInfo.thumbnail()?.let(thumbsList::add)
        imageInfo.original()?.let(originalList::add)
    }
    manuals()?.forEach {
        val fileInfo = it.fragments().fileInfo()
        fileInfo.url().let {
            docs.add(RemoteFile(it, fileInfo.name())) //if crash - bug on backend
        }

    }
    Device(
            id = deviceDetails.id(),
            model = deviceDetails.model_name(),
            brand = brandInfo?.let(brandFragment) ?: Brand(),
            type = deviceTypeInfo?.let(deviceTypeFragment) ?: DeviceType(),
            version = deviceDetails.version() ?: "",
            backgroundImage = brandInfo?.cover() ?: "",
            thumbs = thumbsList,
            images = originalList,
            files = docs
    )

}

val devices = Transformer.build<DevicesQuery.Device, Device> {
    val deviceDetails = fragments().deviceDetails()
    val deviceTypeInfo = type()?.fragments()?.deviceTypeInfo()
    val brandInfo = brand()?.fragments()?.brandInfo()
    val thumbsList = mutableListOf<String>()
    val originalList = mutableListOf<String>()
    val docs = mutableListOf<RemoteFile>()
    galleries()?.forEach {
        val imageInfo = it.fragments().imageInfo()
        imageInfo.thumbnail()?.let(thumbsList::add)
        imageInfo.original()?.let(originalList::add)

    }
    manuals()?.forEach {
        val fileInfo = it.fragments().fileInfo()
        fileInfo.url().let {
            docs.add(RemoteFile(it, fileInfo.name())) //if crash - bug on backend
        }

    }
    Device(
            id = deviceDetails.id(),
            model = deviceDetails.model_name(),
            brand = brandInfo?.let(brandFragment) ?: Brand(),
            type = deviceTypeInfo?.let(deviceTypeFragment) ?: DeviceType(),
            version = deviceDetails.version() ?: "",
            backgroundImage = brandInfo?.cover() ?: "",
            thumbs = thumbsList,
            images = originalList,
            files = docs
    )
}


val additionalDevicesMapper = Transformer.build<AdditionalSearchQuery.AdditionalSearch, Device> {
    val deviceDetails = fragments().deviceDetails()
    val deviceTypeInfo = type()?.fragments()?.deviceTypeInfo()
    val brandInfo = brand()?.fragments()?.brandInfo()
    val thumbsList = mutableListOf<String>()
    val originalList = mutableListOf<String>()
    val docs = mutableListOf<RemoteFile>()
    galleries()?.forEach {
        val imageInfo = it.fragments().imageInfo()
        imageInfo.thumbnail()?.let(thumbsList::add)
        imageInfo.original()?.let(originalList::add)

    }
    manuals()?.forEach {
        val fileInfo = it.fragments().fileInfo()
        fileInfo.url().let {
            docs.add(RemoteFile(it, fileInfo.name())) //if crash - bug on backend
        }

    }
    Device(
            id = deviceDetails.id(),
            model = deviceDetails.model_name(),
            brand = brandInfo?.let(brandFragment) ?: Brand(),
            type = deviceTypeInfo?.let(deviceTypeFragment) ?: DeviceType(),
            version = deviceDetails.version() ?: "",
            backgroundImage = brandInfo?.cover() ?: "",
            thumbs = thumbsList,
            images = originalList,
            files = docs
    )
}
