package com.artjoker.data.mappers

import com.artjoker.models.Comment
import com.artjoker.models.Story
import com.smb.core.mappers.Transformer
import data.CommentsOfStoryQuery
import data.StoriesQuery
import java.util.concurrent.TimeUnit

val story = Transformer.build<StoriesQuery.Story, Story> {
    val storyInfo = fragments().storyInfo()
    Story(
            id = storyInfo.id(),
            title = storyInfo.title(),
            backgroundImageUrl = storyInfo.cover(),
            storyHtmlText = storyInfo.text(),
            commentsCount = storyInfo.commentsCount()?.toInt() ?: 0
    )
}

val listOfStories = Transformer.build<StoriesQuery.Data?, List<Story>> { story.asListMapper().invoke(this?.Stories()) }

val comment = Transformer.build<CommentsOfStoryQuery.Comment, Comment> {
    val commentInfo = fragments().commentInfo()
    Comment(
            commentInfo.id(),
            commentInfo.author(),
            "",
            commentInfo.text(),
            TimeUnit.SECONDS.toMillis(commentInfo.timestamp().toLong()),
            parentId = commentInfo.parent_id()
    )
}

val listOfComments = Transformer.build<CommentsOfStoryQuery.Data?, List<Comment>> { comment.asListMapper().invoke(this?.Comments()) }
