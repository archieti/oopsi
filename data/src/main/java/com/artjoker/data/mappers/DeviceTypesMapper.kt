package com.artjoker.data.mappers

import com.artjoker.models.DeviceType
import com.smb.core.mappers.Transformer
import data.DeviceTypesQuery
import data.fragment.DeviceTypeInfo

val deviceType = Transformer.build<DeviceTypesQuery.Type, DeviceType> {
    deviceTypeFragment.invoke(fragments().deviceTypeInfo())
}

val deviceTypeFragment = Transformer.build<DeviceTypeInfo, DeviceType> {
    DeviceType().apply {
        id = id()
        name = name()
    }
}