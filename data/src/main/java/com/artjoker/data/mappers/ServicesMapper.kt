package com.artjoker.data.mappers


import com.artjoker.models.Contact
import com.artjoker.models.Location
import com.artjoker.models.Schedule
import com.artjoker.models.Service
import com.artjoker.utils.DayOfWeek
import com.smb.core.mappers.Transformer
import data.ServicesQuery


val listOfServices = Transformer.build<ServicesQuery.Data?, List<Service>> {
    val services = service.asListMapper().invoke(this?.Services())
    services
}

val service = Transformer.build<ServicesQuery.Service, Service> {

    val serviceInfo = fragments().serviceInfo()
    val serviceModel = Service(serviceInfo.name(), serviceInfo.address(), serviceInfo.distance()
            ?: "")
    serviceModel.isAuthorized = serviceInfo.status()
    serviceModel.location = Location(serviceInfo.lat(), serviceInfo.lng())
    serviceModel.contact = contact.invoke(contact())
    serviceModel.schedule = schedule.asListMapper().invoke(schedules())
    serviceModel
}

val contact = Transformer.build<ServicesQuery.Contact?, Contact?> {
    when {
        this == null -> null
        else -> Contact(fragments().contactInfo().phone() ?: "", fragments().contactInfo().email() ?: "")
    }
}

val schedule = Transformer.build<ServicesQuery.Schedule, Schedule> {
    val schedulesInfo = fragments().schedulesInfo()
    Schedule(
            getDayOfWeek(schedulesInfo.day()),
            schedulesInfo.work_hours().orEmpty(),
            schedulesInfo.is_active
    )
}

fun getDayOfWeek(day: Int): DayOfWeek = DayOfWeek.values()[day]


val remoteDayIndex = Transformer.build<DayOfWeek, Int> {
    when(this) {
        DayOfWeek.ALL -> -1
        else -> ordinal
    }
}
