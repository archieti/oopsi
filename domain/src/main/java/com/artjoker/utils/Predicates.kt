package com.artjoker.utils

infix fun <T> T?.or(default: T): T = this ?: default
infix fun <T> T?.or(compute: () -> T): T = this ?: compute()
infix fun <T> Boolean.thenDo(param: T): T? = if (this) param else null
infix fun <T> Boolean.thenDo(param: () -> T): T? = if (this) param() else null

