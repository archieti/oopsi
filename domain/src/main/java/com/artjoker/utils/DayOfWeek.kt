package com.artjoker.utils


import com.artjoker.models.Day
import java.text.DateFormatSymbols
import java.util.*

enum class DayOfWeek(private val day: Int) {
    SUNDAY(Calendar.SUNDAY),
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY),
    ALL(-1);

    fun dayIndex(): Int = day

    fun toDay(defaultValue: String = "") =
            Day(
                    this,
                    when {
                        dayIndex() > 0 -> DateFormatSymbols.getInstance().weekdays[dayIndex()]
                        else -> defaultValue
                    }
            )


    companion object {

        fun dayOfWeek(): DayOfWeek {
            val instance = Calendar.getInstance()
            val day = instance.get(Calendar.DAY_OF_WEEK)
            return DayOfWeek.values().find { dayOfWeek -> dayOfWeek.dayIndex() == day }!!
        }
    }
}
