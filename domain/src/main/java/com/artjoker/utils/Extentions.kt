package com.artjoker.utils

import java.text.SimpleDateFormat
import java.util.*

fun Calendar.fromTime(hourOfDay: Int, minute: Int): Date {
    set(Calendar.HOUR_OF_DAY, hourOfDay)
    set(Calendar.MINUTE, minute)
    return time
}

fun Date.toTime() = SimpleDateFormat("HH:mm", Locale.getDefault()).format(this).toString()

fun <T> List<T>.takeFirstIfExist(count: Int): List<T> = subList(0, (count).takeIf { size > (count - 1) }
        ?: size)