package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.CompletableUseCase
import com.artjoker.models.Comment
import com.artjoker.repositories.StoriesRepository
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject

class AddCommentUseCase @Inject constructor(
        private val storiesRepository: StoriesRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>)
    : CompletableUseCase.ParametrizedUseCase<Comment>() {

    override fun buildCompletable(params: Comment): Completable = storiesRepository.add(params)
}