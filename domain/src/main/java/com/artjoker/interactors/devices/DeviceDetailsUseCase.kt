package com.artjoker.interactors.devices

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeviceDetailsUseCase @Inject constructor(
        private val deviceRepository: DeviceRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : SingleUseCase.SimpleUseCase<Device>() {

    override fun buildSingle(params: Nothing?): Single<Device> = Single.just(deviceRepository.device)
}
