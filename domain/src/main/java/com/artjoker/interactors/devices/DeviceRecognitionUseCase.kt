package com.artjoker.interactors.devices

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.core.models.Image
import com.artjoker.core.models.OcrDevice
import com.artjoker.repositories.DeviceRepository
import com.artjoker.repositories.ScanningRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class DeviceRecognitionUseCase @Inject constructor(
        private val scanningRepository: ScanningRepository,
        private val deviceRepository: DeviceRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : SingleUseCase.ParametrizedUseCase<Image, OcrDevice>() {

    override fun buildSingle(params: Image): Single<OcrDevice> =
            scanningRepository.recognize(params)
                    .flatMap { text ->
                        when {
                            text.isEmpty() -> Single.error(RecognitionFailedException())
                            else -> deviceRepository.findDevice(text)
                                    .map {
                                        val deviceWithLabel = it.device.copy(labelUrl = params.url)
                                        OcrDevice(deviceWithLabel, it.ocrText)
                                    }
                        }
                    }
                    .doOnSuccess {
                        deviceRepository.device = it.device
                    }

    class RecognitionFailedException : Exception("No data is recognized")
}
