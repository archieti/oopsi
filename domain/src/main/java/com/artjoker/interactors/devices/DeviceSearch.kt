package com.artjoker.interactors.devices

import com.artjoker.core.BaseUseCase
import com.artjoker.core.ObservableInteractor
import com.artjoker.core.UseCase
import com.artjoker.core.models.SearchDeviceData
import com.artjoker.models.Device
import com.artjoker.repositories.DeviceRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class DeviceSearchInteractor @Inject constructor(
        private val deviceRepository: DeviceRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : ObservableInteractor.ParametrizedUseCase<SearchDeviceData, List<Device>>() {

    override fun buildObservable(params: SearchDeviceData): Observable<List<Device>> =
            deviceRepository.searchDevices(params.model, params.brand)
                    .toObservable()
}


class DevicesNotFoundException : Throwable()


class MaybeDevicesInteractor @Inject constructor(
        private val deviceRepository: DeviceRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : UseCase.ParametrizedUseCase<String, MaybeDevicesInteractor.Result>() {

    override fun buildObservable(params: String): Observable<Result> =

            Observable.interval(10, TimeUnit.SECONDS)
                    .take(1)
                    .map<Result> { Result.TooLongRequest }
                    .mergeWith(
                            deviceRepository.getDevicesBy(params)
                                    .toObservable()
                                    .map(Result::Success)
                    )
                    .takeUntil { it is Result.Success }


    sealed class Result {
        object TooLongRequest : Result()
        class Success(val devices: List<Device>) : Result()
    }
}