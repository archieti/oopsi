package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Story
import com.artjoker.repositories.StoriesRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class StoryDetailsUseCase @Inject
constructor(private val storiesRepository: StoriesRepository,
            override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>)
    : SingleUseCase.SimpleUseCase<Story>() {

    override fun buildSingle(params: Nothing?): Single<Story> = Single.just(storiesRepository.selectedStory)

}
