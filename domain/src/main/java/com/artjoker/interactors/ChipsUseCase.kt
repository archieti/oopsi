package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Chip
import com.artjoker.models.FilterParam
import com.artjoker.repositories.ServicesRepository
import com.artjoker.utils.DayOfWeek
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class ChipsUseCase @Inject constructor(
        val servicesRepository: ServicesRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : SingleUseCase.SimpleUseCase<MutableList<Chip>>() {

    override fun buildSingle(params: Nothing?): Single<MutableList<Chip>> {

        val filter = servicesRepository.filter
        val chips = mutableListOf<Chip>()

        filter.radius.addIfNotNull(chips, 0)
        filter.workingDay.addIfNotNull(chips, DayOfWeek.ALL)
        filter.startTime.addIfNotNull(chips)
        filter.endTime.addIfNotNull(chips)

        return Single.just(chips)
    }

    private fun <T> FilterParam<T>?.addIfNotNull(chips: MutableList<Chip>, unlessValue: Any? = null) {
        this?.let(::Chip)?.let {
            if (unlessValue == null || this.value != unlessValue) {
                chips.add(it)
            }
        }
    }
}
