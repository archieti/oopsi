package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Device
import com.artjoker.repositories.DocumentationRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class SendDocumentationUseCase
@Inject constructor(override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>,
                    private val docRepository: DocumentationRepository) :
        SingleUseCase.ParametrizedUseCase<SendDocumentationUseCase.Params, Boolean>() {

    override fun buildSingle(params: Params): Single<Boolean> = docRepository.sendToEmail(params.device, params.email)

    data class Params(val device: Device, val email: String)
}
