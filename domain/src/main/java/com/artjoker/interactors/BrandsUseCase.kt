package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Brand
import com.artjoker.repositories.BrandsRepository
import com.artjoker.utils.takeFirstIfExist
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class BrandsUseCase @Inject
constructor(private val brandsRepository: BrandsRepository,
            override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>)
    : SingleUseCase.ParametrizedUseCase<String, List<Brand>>() {

    override fun buildSingle(params: String): Single<List<Brand>> = Single.fromObservable(
            Observable.just(params)
                    .flatMap(
                            { _ -> brandsRepository.getBrands() },
                            { query, brands -> find(query, brands) }
                    )
    )

    private fun find(queryChars: String, brands: List<Brand>): List<Brand> = brands
            .takeFirstIfExist(10)
            .takeIf { queryChars.isEmpty() } ?: queryChars.let { doQuery(brands, queryChars) }

    private fun doQuery(brands: List<Brand>, query: String): ArrayList<Brand> {
        val result = ArrayList<Brand>()
        brands.forEach {
            val lowerCaseQuery = query.toLowerCase()
            val candidate = it.name.toLowerCase()
            if (candidate.startsWith(lowerCaseQuery)) {
                result.add(it)
            }
        }
        return result
    }
}
