package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Service
import com.artjoker.repositories.LocationRepository
import com.artjoker.repositories.ServicesRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class NearbyServicesUseCase @Inject constructor(
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>,
        private val servicesRepository: ServicesRepository,
        private val locationRepository: LocationRepository
) : SingleUseCase.SimpleUseCase<List<Service>>() {

    override fun buildSingle(params: Nothing?): Single<List<Service>> =
            locationRepository.getLocation()
                    .firstOrError()
                    .flatMap(servicesRepository::getNearbyServices)


}
