package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Comment
import com.artjoker.repositories.StoriesRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class CommentsUseCase @Inject constructor(
        private val storiesRepository: StoriesRepository,
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>
) : SingleUseCase.SimpleUseCase<List<Comment>>() {

    override fun buildSingle(params: Nothing?): Single<List<Comment>> =
            storiesRepository.loadComments(storiesRepository.selectedStory.id)
}
