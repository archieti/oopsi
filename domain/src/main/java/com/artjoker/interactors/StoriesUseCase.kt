package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Story
import com.artjoker.repositories.StoriesRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StoriesUseCase @Inject
constructor(private val storiesRepository: StoriesRepository,
            override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>)
    : SingleUseCase.SimpleUseCase<List<Story>>() {

    override fun buildSingle(params: Nothing?): Single<List<Story>> = storiesRepository.retrieveStories()

}
