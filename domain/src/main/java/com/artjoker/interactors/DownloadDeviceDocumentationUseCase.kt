package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.CompletableUseCase
import com.artjoker.models.Device
import com.artjoker.repositories.DocumentationRepository
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject

class DownloadDeviceDocumentationUseCase @Inject
constructor(override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>,
            private val documentationRepository: DocumentationRepository)
    : CompletableUseCase.ParametrizedUseCase<Device>() {

    override fun buildCompletable(params: Device): Completable = Completable.fromSingle(documentationRepository.downloadFor(params))

}
