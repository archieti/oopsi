package com.artjoker.interactors

import com.artjoker.core.BaseUseCase
import com.artjoker.core.SingleUseCase
import com.artjoker.models.Filter
import com.artjoker.models.Service
import com.artjoker.repositories.LocationRepository
import com.artjoker.repositories.ServicesRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

class FilterServicesUseCase @Inject constructor(
        override val schedulers: Map<@JvmSuppressWildcards BaseUseCase.SchedulerType, @JvmSuppressWildcards Scheduler>,
        private val servicesRepository: ServicesRepository,
        private val locationRepository: LocationRepository
) : SingleUseCase.ParametrizedUseCase<Filter, List<Service>>() {

    override fun buildSingle(params: Filter): Single<List<Service>> =
            locationRepository.getLocation()
                    .firstOrError()
                    .flatMap {
                        servicesRepository.findServicesByFilters(params, it)
                    }
}
