package com.artjoker.core

import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver


sealed class BaseUseCase {

    abstract val schedulers: Map<SchedulerType, Scheduler>
    protected var disposable: Disposable = Disposables.disposed()

    val disposed get() = disposable.isDisposed

    fun cancel() {
        disposable.dispose()
    }

    enum class SchedulerType {
        WORK,
        WORK_RESULT
    }
}

sealed class UseCase<in P, R> : BaseUseCase() {

    protected abstract fun buildObservable(params: P): Observable<R>

    protected open fun mainThreadActions(o: Observable<R>): Observable<R> = o

    protected fun doExecute(params: P, o: DisposableObserver<R>) {
        cancel()
        disposable = o
        buildObservable(params)
                .subscribeOn(schedulers[SchedulerType.WORK])
                .observeOn(schedulers[SchedulerType.WORK_RESULT])
                .compose(this::mainThreadActions)
                .subscribe(o)
    }

    abstract class ParametrizedUseCase<in P : Any, R> : UseCase<P, R>() {

        fun execute(params: P, o: DisposableObserver<R>) {
            doExecute(params, o)
        }
    }

    abstract class SimpleUseCase<R> : UseCase<Nothing?, R>() {


        fun execute(o: DisposableObserver<R>) {
            doExecute(null, o)
        }
    }
}

sealed class ObservableInteractor<in P, R> : BaseUseCase() {

    protected abstract fun buildObservable(params: P): Observable<R>

    protected fun doExecute(params: P): Observable<R> =
        buildObservable(params)
                .subscribeOn(schedulers[SchedulerType.WORK])
                .observeOn(schedulers[SchedulerType.WORK_RESULT])


    abstract class ParametrizedUseCase<in P : Any, R> : ObservableInteractor<P, R>() {

        fun execute(params: P) = doExecute(params)
    }
}


sealed class SingleUseCase<in P, R> : BaseUseCase() {

    protected abstract fun buildSingle(params: P): Single<R>

    protected open fun mainThreadActions(single: Single<R>) = single

    protected fun doExecute(params: P, o: DisposableSingleObserver<R>) {
        cancel()
        disposable = o
        buildSingle(params)
                .subscribeOn(schedulers[SchedulerType.WORK])
                .observeOn(schedulers[SchedulerType.WORK_RESULT])
                .compose(this::mainThreadActions)
                .subscribe(o)
    }

    abstract class ParametrizedUseCase<in P : Any, R> : SingleUseCase<P, R>() {

        fun execute(params: P, o: DisposableSingleObserver<R>) {
            doExecute(params, o)
        }
    }

    abstract class SimpleUseCase<R> : SingleUseCase<Nothing?, R>() {

        fun execute(o: DisposableSingleObserver<R>) {
            doExecute(null, o)
        }
    }

    open class SimpleObserver<T> : DisposableSingleObserver<T>() {

        override fun onSuccess(t: T) {}

        override fun onError(e: Throwable) {}
    }
}

sealed class MaybeUseCase<in P, R> : BaseUseCase() {

    protected abstract fun buildMaybe(params: P): Maybe<R>

    protected fun doExecute(params: P, o: DisposableMaybeObserver<R>) {
        cancel()
        disposable = o
        buildMaybe(params)
                .subscribeOn(schedulers[SchedulerType.WORK])
                .observeOn(schedulers[SchedulerType.WORK_RESULT])
                .subscribe(o)
    }

    abstract class ParametrizedUseCase<in P : Any, R> : MaybeUseCase<P, R>() {

        fun execute(params: P, o: DisposableMaybeObserver<R>) {
            doExecute(params, o)
        }
    }

    abstract class SimpleUseCase<R> : MaybeUseCase<Nothing?, R>() {

        fun execute(o: DisposableMaybeObserver<R>) {
            doExecute(null, o)
        }
    }
}

sealed class CompletableUseCase<in P> : BaseUseCase() {

    protected abstract fun buildCompletable(params: P): Completable

    protected open fun mainThreadActions(completable: Completable, params: P): Completable =
            completable

    protected fun doExecute(params: P, o: DisposableCompletableObserver) {
        cancel()
        disposable = o
        buildCompletable(params)
                .subscribeOn(schedulers[SchedulerType.WORK])
                .observeOn(schedulers[SchedulerType.WORK_RESULT])
                .compose { mainThreadActions(it, params) }
                .subscribe(o)
    }

    abstract class ParametrizedUseCase<in P : Any> : CompletableUseCase<P>() {

        fun execute(params: P, o: DisposableCompletableObserver = SimpleObserver()) {
            doExecute(params, o)
        }
    }

    abstract class SimpleUseCase : CompletableUseCase<Nothing?>() {

        fun execute(o: DisposableCompletableObserver = SimpleObserver()) {
            doExecute(null, o)
        }
    }

    class SimpleObserver : DisposableCompletableObserver() {

        override fun onError(e: Throwable) {}

        override fun onComplete() {}
    }
}
