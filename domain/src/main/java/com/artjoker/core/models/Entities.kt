package com.artjoker.core.models

import com.artjoker.models.Device
import java.io.Serializable


class SearchDeviceData(val model: String, val brand: String) : Serializable

class OcrDevice(val device: Device, val ocrText: String) : Serializable
