package com.artjoker.core.models

interface Image {
    val url: String
}
