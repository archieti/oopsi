package com.artjoker.models

data class Comment(
        val id: String = "",
        val authorName: String,
        val authorAvatarUrl: String = "",
        val text: String,
        val timestamp: Long = 0,
        val storyId: String? = null,
        val parentId: String? = null
)