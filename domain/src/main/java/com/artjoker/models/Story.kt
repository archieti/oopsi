package com.artjoker.models

data class Story(val id: String,
                 val title: String,
                 var commentsCount: Int = 0,
                 val backgroundImageUrl: String = "",
                 val storyHtmlText: String) {

    override fun equals(other: Any?): Boolean = if (other is Story) id == other.id else false

    override fun hashCode(): Int = id.hashCode()

}



