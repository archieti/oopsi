package com.artjoker.models

import com.artjoker.utils.DayOfWeek
import java.io.Serializable
import java.util.*


data class Filter(
        val brand: BrandFilter? = null,
        val radius: RadiusFilter? = null,
        val workingDay: WorkingDayFilter? = null,
        val startTime: StartTimeFilter? = null,
        val endTime: EndTimeFilter? = null
) : Serializable {
    val anyActiveFilter
        get() = brand != null
                || (radius != null && radius.value != 0)
                || workingDay != null
                || startTime != null
                || endTime != null
}


sealed class FilterParam<T>(val value: T): Serializable


class BrandFilter(brand: Brand) : FilterParam<Brand>(brand), Serializable


class RadiusFilter(radius: Int) : FilterParam<Int>(radius), Serializable {

    companion object {
        const val MAX_DISTANCE = 50
    }
}


class WorkingDayFilter(dayOfWeek: DayOfWeek) : FilterParam<DayOfWeek>(dayOfWeek), Serializable


class StartTimeFilter(date: Date) : FilterParam<Date>(date), Serializable


class EndTimeFilter(date: Date) : FilterParam<Date>(date), Serializable
