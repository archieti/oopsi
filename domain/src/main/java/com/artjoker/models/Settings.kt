package com.artjoker.models

import com.artjoker.models.Settings.NetworkState.ALL

data class Settings(val networkState: NetworkState = ALL) {
    enum class NetworkState {
        ALL,
        WIFI_ONLY
    }
}
