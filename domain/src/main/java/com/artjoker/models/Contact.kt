package com.artjoker.models

data class Contact(val phone: String, val email: String)