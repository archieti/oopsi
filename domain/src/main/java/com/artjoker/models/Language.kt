package com.artjoker.models

import java.util.*

data class Language(val title: String = "", val locale: Locale = Locale(""))