package com.artjoker.models

import com.artjoker.utils.DayOfWeek

data class Day(val dayOfWeek: DayOfWeek, private val _name: String = "") {
    val name get() = _name.capitalize()
}