package com.artjoker.models


import com.artjoker.utils.DayOfWeek.Companion.dayOfWeek

data class Service(val name: String = "", val address: String = "", val distance: String = "") {
    var brand: List<Brand> = listOf()
    var contact: Contact? = null
    var isAuthorized: Boolean = false
    var location: Location = Location(0.0, 0.0)
    var schedule: List<Schedule> = listOf()


    fun todaySchedule(): Schedule = dayOfWeek()
            .let { dayOfWeek ->
                schedule.find { it.dayOfWeek.ordinal == dayOfWeek.ordinal }
            }!!


    fun scheduleExceptToday(): List<Schedule> = dayOfWeek()
            .let { dayOfWeek ->
                schedule.filter { it.dayOfWeek.ordinal != dayOfWeek.ordinal }
            }

}
