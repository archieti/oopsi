package com.artjoker.models

import java.io.Serializable

data class Brand(val id: String = "", val name: String = "") : Serializable {
    var imageUrl: String? = null
}