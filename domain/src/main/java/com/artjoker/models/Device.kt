package com.artjoker.models

data class Device constructor(
        val id: String = "",
        val model: String = "",
        val brand: Brand = Brand(),
        val type: DeviceType = DeviceType(),
        val version: String = "",
        val userManualUrl: String = "",
        val images: List<String> = mutableListOf(),
        val thumbs: List<String> = mutableListOf(),
        val files: List<RemoteFile> = mutableListOf(),
        val backgroundImage: String = "",
        val labelUrl: String = ""
)

