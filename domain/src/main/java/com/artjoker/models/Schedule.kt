package com.artjoker.models

import com.artjoker.utils.DayOfWeek


data class Schedule constructor(
        val dayOfWeek: DayOfWeek,
        val workHours: List<String>,
        val isWorking: Boolean
)
