package com.artjoker.models

data class Location(val lat: Double, val lng: Double)
