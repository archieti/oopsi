package com.artjoker.models

data class RemoteFile(val url: String, val name: String)
