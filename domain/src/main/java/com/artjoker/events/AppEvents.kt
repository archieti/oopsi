package com.artjoker.events

import com.artjoker.models.Comment
import com.artjoker.models.Device
import com.artjoker.models.Filter
import com.artjoker.models.Story
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppEvents @Inject constructor() {

    private val listeners: MutableList<Listener> = mutableListOf()

    fun addListener(listener: Listener) {
        listeners.add(listener)
    }

    fun removeListener(listener: Listener) {
        listeners.remove(listener)
    }

    fun notifyListeners(notification: (Listener) -> Unit) {
        listeners.forEach(notification)
    }

    interface Listener {

        fun onNewComment(story: Story, comments: List<Comment>) {}
        fun onCommentsCountChanged(story: Story, commentsCount: Int) {}
        fun onFiltersChanged(filters: Filter) {}
        fun onLocaleChanges() {}
        fun onFavouritesChanged(device: Device, fav: Boolean) {}
    }
}