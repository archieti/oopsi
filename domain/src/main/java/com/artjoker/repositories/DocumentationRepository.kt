package com.artjoker.repositories

import com.artjoker.models.Device
import io.reactivex.Single

interface DocumentationRepository {
    fun downloadFor(device: Device): Single<List<String>>
    fun sendToEmail(device: Device, email: String): Single<Boolean>
}
