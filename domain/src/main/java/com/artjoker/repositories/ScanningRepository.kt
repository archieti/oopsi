package com.artjoker.repositories

import com.artjoker.core.models.Image

import io.reactivex.Single
import java.util.*

interface ScanningRepository {

    fun getImage(): Image?

    fun put(image: Image)

    fun recognize(image: Image): Single<String>

    fun clear()

    fun saveToGallery(image: Image, id: String): String

    fun getFromGallery(url: String): Image?
}

fun ScanningRepository.saveToGallery(image: Image): String =
        saveToGallery(image, "OOpsi-${UUID.randomUUID()}")
