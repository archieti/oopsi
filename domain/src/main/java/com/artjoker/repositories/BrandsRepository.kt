package com.artjoker.repositories

import com.artjoker.models.Brand
import io.reactivex.Observable

interface BrandsRepository {
    fun getBrands(): Observable<List<Brand>>
}