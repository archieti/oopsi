package com.artjoker.repositories

interface Density {
    fun getDensity(): String
}
