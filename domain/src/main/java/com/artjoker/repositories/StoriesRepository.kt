package com.artjoker.repositories

import com.artjoker.models.Comment
import com.artjoker.models.Story
import io.reactivex.Completable
import io.reactivex.Single

interface StoriesRepository {
    fun retrieveStories(): Single<List<Story>>
    fun loadComments(storyID: String): Single<List<Comment>>
    fun add(comment: Comment): Completable
    fun reply(new: Comment, parent: Comment): Completable
    var selectedStory: Story
    var reply: Comment

}