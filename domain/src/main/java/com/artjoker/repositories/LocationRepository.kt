package com.artjoker.repositories

import com.artjoker.models.Location
import io.reactivex.Observable

interface LocationRepository {

    fun getLocation(): Observable<Location>

    fun putLocation(location: Location)
}
