package com.artjoker.repositories

import com.artjoker.core.models.OcrDevice
import com.artjoker.models.Device
import io.reactivex.Single

interface DeviceRepository { //TODO -> Interface segregation
    var device: Device
    fun searchDevices(model: String, brand: String): Single<List<Device>>
    fun findDevice(text: String): Single<OcrDevice>
    fun getFavouriteDevices(): List<Device>
    fun isFavourite(device: Device): Boolean
    fun toggleFavourite(device: Device): Boolean
    fun putToHistory(device: Device)
    fun removeFromHistory(device: Device)
    fun getHistory(): MutableList<Device>

    fun getDevicesBy(ocrStr: String): Single<List<Device>>
}
