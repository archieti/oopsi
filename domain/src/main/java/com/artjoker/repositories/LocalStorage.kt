package com.artjoker.repositories

import com.artjoker.models.*

interface LocalStorage {

    var device: Device

    var selectedService: Service

    var selectedStory: Story

    var reply: Comment

    var filter: Filter

    var settings: Settings

    fun saveFavourites(devices: MutableSet<Device>): Boolean

    fun getFavourites(): MutableSet<Device>

    fun saveHistory(history: MutableList<Device>)

    fun getHistory(): MutableList<Device>

    companion object {
        const val FAVOURITES_KEY = "favourites"
        const val HISTORY_KEY = "history"
    }
}

interface LangStorage {

    var language: Language
}
