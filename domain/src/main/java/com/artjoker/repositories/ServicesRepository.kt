package com.artjoker.repositories

import com.artjoker.models.Brand
import com.artjoker.models.Filter
import com.artjoker.models.Location
import com.artjoker.models.Service
import io.reactivex.Single

interface ServicesRepository {
    fun getNearbyServices(location: Location): Single<List<Service>>
    fun findServicesByBrand(params: Brand, location: Location): Single<List<Service>>
    fun findServicesByFilters(params: Filter, location: Location): Single<List<Service>>
    var selectedService: Service
    var filter: Filter
}
